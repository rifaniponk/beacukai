select * from user;
select * from karyawan;
select * from userRole;
select * from pelanggan;
select * from pelangganv;
select * from joborderstatus order by urutan ASC;	
select * from layanan;
select * from joborder order by ctime DESC;
select * from barang;
select * from ci;
select * from createjoborder;
select * from biaya;
select * from buktibayar;
select * from dokumen;
select * from pib;
select * from invoice;
select * from pembayaran;
select * from pemasukanv;
select * from pengeluaranv;
select * from tabledesc;
select * from notif;
select * from laporanJO;

create or replace view laporanJO AS
select j.id AS Pesanan_ID, p.nama AS Nama_Pelanggan, b.nama AS Nama_Barang, j.tglMulai AS Tanggal_Mulai, j.tglSelesai AS Tanggal_Selesai, GetStatus(j.tglMulai,j.status) AS Status, j.noPengajuan AS No_Pengajuan_PIB, j.grandtotal AS Total_Biaya
from sub_laporanJO AS j, 
	pelanggan p, barang b
where j.pelanggan_user_id=p.user_id AND j.barang_id=b.id;

create or replace view sub_laporanJO AS
select joborder.id, joborder.tglMulai, joborder.tglSelesai, joborder.status, joborder.pelanggan_user_id, joborder.barang_id, pib.noPengajuan, joborder.invoice_no, invoice.grandtotal 
from joborder LEFT JOIN (pib, invoice) on (joborder.pib_id=pib.id AND joborder.invoice_no=invoice.no);

DROP FUNCTION IF EXISTS GetStatus;
DELIMITER $$
CREATE FUNCTION GetStatus(tglMulai datetime, stat char(10))
  RETURNS CHAR(20) DETERMINISTIC
BEGIN
	IF stat="ljoc1" THEN
		RETURN "Selesai";
	END IF;
	IF NOW() < DATE(tglMulai + INTERVAL 1 MONTH) THEN
		RETURN "Belum Selesai";
	else
		RETURN "Expired";
	END IF;
END;
$$
DELIMITER ;

create or replace view pemasukanv AS
select MONTH(tgl) AS bulan, year(tgl) as tahun, sum(jasaImpor) as jasaImpor, sum(jasaPPJK) as jasaPPJK, sum(biayaDok) as biayaDok, sum(biayaLain) as biayaLain, 
sum(total1) as total1, sum(ppnTotal1) as ppnTotal1, sum(pdri) as pdri, sum(beaMasuk) as beaMasuk, sum(biayaBeacukai) as biayaBeacukai, sum(total2) as total2, sum(grandtotal) as grandtotal
from invoice group by MONTH(tgl), Year(tgl);

create or replace view pengeluaranv AS
select MONTH(tgl) AS bulan, year(tgl) as tahun, sum(ppnTotal1) as ppnTotal1, sum(pdri) as pdri, sum(beaMasuk) as beaMasuk, sum(biayaBeacukai) as biayaBeacukai, sum(total2) as total2
from invoice group by MONTH(tgl), Year(tgl);

create or replace view pelangganv AS
select `id`,`username`,`password`,a.`nama` AS namauser,a.`role` AS roleUser,`last_login` ,
`user_id`,`ktp`,b.`nama` AS nama,b.`role` AS rolePelanggan,`email`,`alamat`,`npwp`,`noapi`,`npik`,`telp`,`web`,`siupAtc`,`npwpAtc`,`ktpAtc`,`isVerified`
from user a left join pelanggan b on a.id=b.user_id;

create or replace view createJobOrder AS
select a.id,a.barang_id, a.layanan_id, a.bl_id, a.ci_id, a.si_id, a.pl, a.plAtc, a.sj, a.sjAtc, a.sk, a.skAtc, 
b.noSI, b.tglTiba, b.tglBrgkt, b.atc as si_atc, c.noCI, c.tgl as ci_tgl, c.atc as ci_atc, d.nomor, d.tgl as bl_tgl, 
d.atc, e.nama, e.des, e.jml, e.bruto, e.netto, e.volume 
from joborder a, si b, ci c, bl d, barang e 
where a.barang_id=e.id AND a.bl_id=d.id AND a.ci_id=c.id AND a.si_id=b.id;
