-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2014 at 10:06 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `beacukai`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(145) NOT NULL,
  `des` text,
  `jml` int(11) NOT NULL,
  `bruto` int(11) NOT NULL,
  `netto` int(11) NOT NULL,
  `volume` int(11) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama`, `des`, `jml`, `bruto`, `netto`, `volume`, `ctime`) VALUES
(3, 'senpak kuda', 'sempak kuda merk alibaba', 200, 200, 100, 300, '2014-03-25 14:31:09'),
(4, 'kolor babeh', 'sempak kuda merk alibaba', 32, 45, 45, 45, '2014-03-25 14:39:10'),
(5, 'senpak kuda', 'cuma senpak', 2, 200, 80, 234, '2014-05-04 10:04:57'),
(6, 'dasdas', 'dasdasd', 23, 78, 78, 21000, '2014-05-25 07:22:46'),
(8, 'senpak kuda', 'senpak kuda yang bau', 12, 12, 12, 12312, '2014-05-29 04:26:55'),
(9, 'antik', '12atyaweta', 15, 20, 20, 30, '2014-05-31 07:33:07'),
(11, 'asd', 'asd', 123, 123, 123, 123, '2014-05-31 07:36:24'),
(12, 'batu', 'glnakgl', 30, 500, 500, 50, '2014-06-01 05:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE IF NOT EXISTS `biaya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cif` float DEFAULT NULL,
  `fob` float DEFAULT NULL,
  `asuransi` float DEFAULT NULL,
  `bm` float DEFAULT NULL,
  `cukai` float DEFAULT NULL,
  `ppn` float DEFAULT NULL,
  `ppnbm` float DEFAULT NULL,
  `pph` float DEFAULT NULL,
  `kurs` float DEFAULT NULL,
  `freight` float DEFAULT NULL,
  `nilaiPabean` float DEFAULT NULL,
  `nilaiImpor` float DEFAULT NULL,
  `tglTiba` date DEFAULT NULL,
  `docAtc` varchar(245) DEFAULT NULL,
  `bm_rp` float DEFAULT NULL,
  `cukai_rp` float DEFAULT NULL,
  `pdri` float DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`id`, `cif`, `fob`, `asuransi`, `bm`, `cukai`, `ppn`, `ppnbm`, `pph`, `kurs`, `freight`, `nilaiPabean`, `nilaiImpor`, `tglTiba`, `docAtc`, `bm_rp`, `cukai_rp`, `pdri`, `ctime`) VALUES
(1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 222, 22211, '2014-04-09', NULL, NULL, NULL, NULL, NULL),
(2, 1, 2, 200, 200, 200, 20, 20, 20, 2, 2, 44004, 44004200000, '2014-05-04', 'uploaded/usep/2014-05-04 17.18.25-Jellyfish.jpg', NULL, NULL, NULL, NULL),
(3, 100, 100, 100, 10, 10, 10, 10, 10, 2000, 50, 50000000, 50000000, '2014-05-22', 'uploaded/usep/2014-05-29 14.31.25-installer_prefs.json', NULL, NULL, NULL, NULL),
(4, NULL, 40, 40, 10, 10, 10, NULL, 15, 3000, 20, 300000, 360000, '2014-05-31', 'uploaded/usep/2014-05-31 14.55.11-Desert.jpg', 30000, 30000, 90000, '2014-05-31 07:55:11'),
(5, NULL, 50, 30, 10, NULL, 10, NULL, 7.5, 40000, 20, 4000000, 4400000, '2014-06-02', 'uploaded/usep/2014-06-01 12.43.38-Desert.jpg', 400000, 0, 748000, '2014-06-01 05:43:38');

-- --------------------------------------------------------

--
-- Table structure for table `biayappjk`
--

CREATE TABLE IF NOT EXISTS `biayappjk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenisImpor` int(1) NOT NULL COMMENT '1=qq;2=fs',
  `nilaiInvoice` float NOT NULL,
  `jmlKontainer` int(11) NOT NULL,
  `jalur` int(1) NOT NULL COMMENT '1=hijau;2=hijau+;3=merah',
  `jmlDok` int(11) NOT NULL,
  `jmlLampiranPIB` int(11) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `biayappjk`
--

INSERT INTO `biayappjk` (`id`, `jenisImpor`, `nilaiInvoice`, `jmlKontainer`, `jalur`, `jmlDok`, `jmlLampiranPIB`, `ctime`) VALUES
(1, 1, 213123, 12, 1, 32, 45, '2014-06-01 08:02:41'),
(2, 1, 123, 12, 1, 12, 32, '2014-06-01 08:04:27'),
(3, 2, 10000000, 3, 1, 2, 3, '2014-06-01 08:34:34');

-- --------------------------------------------------------

--
-- Table structure for table `bl`
--

CREATE TABLE IF NOT EXISTS `bl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(30) NOT NULL,
  `tgl` date NOT NULL,
  `atc` varchar(245) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nomor_UNIQUE` (`nomor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `bl`
--

INSERT INTO `bl` (`id`, `nomor`, `tgl`, `atc`, `ctime`) VALUES
(1, '425345345', '2014-02-03', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0045_kecil.jpg', '2014-03-25 14:31:09'),
(2, '68678678', '2014-02-03', 'uploaded/dadang/2014-04-09 17.11.22-cop surat.docx', '2014-03-25 14:39:10'),
(3, '123123', '2014-02-02', 'uploaded/yunero/2014-05-04 17.10.38-Lighthouse.jpg', '2014-05-04 10:04:57'),
(4, 'asdasdasd', '0000-00-00', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.png', '2014-05-25 07:22:46'),
(6, '4444444', '2014-05-22', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.png', '2014-05-29 04:26:55'),
(7, '12361326', '2014-05-31', 'uploaded/koko/2014-05-31 14.33.07-Jellyfish.jpg', '2014-05-31 07:33:07'),
(8, 'asd', '2014-05-23', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.png', '2014-05-31 07:36:24'),
(9, '12616', '2014-06-02', 'uploaded/tes/2014-06-01 12.39.47-Tulips.jpg', '2014-06-01 05:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `buktibayar`
--

CREATE TABLE IF NOT EXISTS `buktibayar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(15) DEFAULT NULL,
  `judul` varchar(145) NOT NULL,
  `atc` varchar(245) NOT NULL,
  `JobOrder_id` int(11) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_buktiBayar_JobOrder1_idx` (`JobOrder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `buktibayar`
--

INSERT INTO `buktibayar` (`id`, `jenis`, `judul`, `atc`, `JobOrder_id`, `ctime`) VALUES
(1, NULL, '123', 'uploaded/mkeu/2014-04-13 11.13.18-bpk deden display.xlsx', 1, '2014-04-13 04:13:18'),
(2, NULL, 'asdasdadasd', 'uploaded/mkeu/2014-04-13 11.17.44-BERITA ACARA SERAH TERIMA BARANG.docx', 1, '2014-04-13 04:17:44'),
(3, NULL, 'asdasdadasd', 'uploaded/mkeu/2014-04-13 11.18.37-BERITA ACARA SERAH TERIMA BARANG.docx', 1, '2014-04-13 04:18:37'),
(4, 'Notul', 'Notul', 'uploaded/dadang/2014-04-20 10.59.34-installer_prefs.json', 1, '2014-04-20 03:59:34'),
(5, 'Notul', ' ASDA SDASD ASD Aa sdas dasd', 'uploaded/dadang/2014-04-21 21.14.33-DSCF0459 _crop _small.jpg', 1, '2014-04-21 14:14:33'),
(6, 'PIB', 'pemabyaran senpak', 'uploaded/mkeu/2014-05-04 17.20.38-Hydrangeas.jpg', 3, '2014-05-04 10:20:38'),
(7, 'PIB', 'ee', 'uploaded/mkeu/2014-05-29 14.39.48-Arthemes Logo.png', 5, '2014-05-29 07:39:48'),
(8, 'PIB', 'asd', 'uploaded/mkeu/2014-05-29 14.43.10-Arthemes Logo.png', 5, '2014-05-29 07:43:10'),
(9, 'PIB', '65e67e5', 'uploaded/mkeu/2014-05-31 15.27.59-Penguins.jpg', 6, '2014-05-31 08:27:59'),
(10, 'Notul', 'asdasd', 'uploaded/koko/2014-05-31 22.07.43-Data', 6, '2014-05-31 15:07:43'),
(11, 'PIB', '156161', 'uploaded/mkeu/2014-06-01 12.45.24-Hydrangeas.jpg', 8, '2014-06-01 05:45:24'),
(12, 'Notul', '15125', 'uploaded/tes/2014-06-01 13.10.43-Penguins.jpg', 8, '2014-06-01 06:10:43'),
(13, 'Notul', '56467', 'uploaded/tes/2014-06-01 15.11.27-Penguins.jpg', 8, '2014-06-01 08:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `ci`
--

CREATE TABLE IF NOT EXISTS `ci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noCI` varchar(45) NOT NULL,
  `tgl` date NOT NULL,
  `atc` varchar(245) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `noCI_UNIQUE` (`noCI`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ci`
--

INSERT INTO `ci` (`id`, `noCI`, `tgl`, `atc`, `ctime`) VALUES
(1, '234234234', '2014-02-06', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0074.JPG', '2014-03-25 14:31:09'),
(2, '678678', '2014-02-06', 'uploaded/dadang/2014-04-09 17.11.22-BERITA ACARA SERAH TERIMA BARANG.docx', '2014-03-25 14:39:10'),
(3, '234234', '2014-02-02', 'uploaded/yunero/2014-05-04 17.10.38-Koala.jpg', '2014-05-04 10:04:57'),
(4, 'asdasdas', '0000-00-00', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.png', '2014-05-25 07:22:46'),
(5, '4444444444', '2014-05-14', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.png', '2014-05-29 04:26:55'),
(6, '12351235', '2014-05-30', 'uploaded/koko/2014-05-31 14.33.07-Desert.jpg', '2014-05-31 07:33:07'),
(7, 'asdas', '2014-05-07', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.png', '2014-05-31 07:36:24'),
(8, '615123', '2014-06-02', 'uploaded/tes/2014-06-01 12.39.47-Desert.jpg', '2014-06-01 05:39:47');

-- --------------------------------------------------------

--
-- Stand-in structure for view `createjoborder`
--
CREATE TABLE IF NOT EXISTS `createjoborder` (
`id` int(11)
,`barang_id` int(11)
,`layanan_id` int(11)
,`bl_id` int(11)
,`ci_id` int(11)
,`si_id` int(11)
,`pl` varchar(45)
,`plAtc` varchar(245)
,`sj` varchar(45)
,`sjAtc` varchar(245)
,`sk` varchar(45)
,`skAtc` varchar(245)
,`noSI` varchar(30)
,`tglTiba` date
,`tglBrgkt` date
,`si_atc` varchar(245)
,`noCI` varchar(45)
,`ci_tgl` date
,`ci_atc` varchar(245)
,`nomor` varchar(30)
,`bl_tgl` date
,`atc` varchar(245)
,`nama` varchar(145)
,`des` text
,`jml` int(11)
,`bruto` int(11)
,`netto` int(11)
,`volume` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `dokumen`
--

CREATE TABLE IF NOT EXISTS `dokumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(15) DEFAULT NULL COMMENT 'Dokumen Pembukuan\nDeklarasi Nilai Pabean',
  `judul` varchar(145) NOT NULL,
  `atc` varchar(245) NOT NULL,
  `JobOrder_id` int(11) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_buktiBayar_JobOrder1_idx` (`JobOrder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dokumen`
--

INSERT INTO `dokumen` (`id`, `jenis`, `judul`, `atc`, `JobOrder_id`, `ctime`) VALUES
(1, 'asdasd', 'asdasd', 'uploaded/dadang/2014-04-20 13.12.50-installation_status.xml', 1, '2014-04-20 06:12:50'),
(2, 'asdfvcxcvxcv', 'qewqweqewqew', 'uploaded/dadang/2014-04-20 13.18.26-installer_prefs.json', 1, '2014-04-20 06:18:26'),
(3, 'brombrom', 'adasdasd', 'uploaded/koko/2014-06-01 08.32.55-metadata', 6, '2014-06-01 01:32:55'),
(4, 'dga', '141r', 'uploaded/tes/2014-06-01 15.11.51-Tulips.jpg', 8, '2014-06-01 08:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `no` varchar(50) NOT NULL,
  `tgl` date NOT NULL,
  `jasaImpor` float NOT NULL,
  `jasaPPJK` float NOT NULL,
  `biayaDok` float NOT NULL,
  `biayaLain` float DEFAULT NULL,
  `total1` float NOT NULL,
  `ppnTotal1` float NOT NULL,
  `pdri` float NOT NULL,
  `beaMasuk` float NOT NULL,
  `biayaBeacukai` float DEFAULT NULL,
  `total2` float NOT NULL,
  `grandtotal` float NOT NULL,
  `pembayaranInvoice_id` int(11) DEFAULT NULL,
  `mk_id` int(11) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`no`,`beaMasuk`),
  KEY `fk_invoice_pembayaranInvoice1_idx` (`pembayaranInvoice_id`),
  KEY `fk_invoice_user1_idx` (`mk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`no`, `tgl`, `jasaImpor`, `jasaPPJK`, `biayaDok`, `biayaLain`, `total1`, `ppnTotal1`, `pdri`, `beaMasuk`, `biayaBeacukai`, `total2`, `grandtotal`, `pembayaranInvoice_id`, `mk_id`, `ctime`) VALUES
('6/TSA/2014-05-31', '2014-06-02', 1000000, 2800000, 17100000, 3700000, 24600000, 246000, 90000, 30000, 12000, 132000, 24978000, NULL, 11, '2014-06-02 13:07:04'),
('8/TSA/2014-06-01', '2014-06-01', 2000000, 1000000, 2100000, 500000, 5600000, 0, 748000, 400000, 2000000, 3148000, 8748000, 8, 11, '2014-06-01 09:18:56');

--
-- Triggers `invoice`
--
DROP TRIGGER IF EXISTS `delete_fk_invoice_jo`;
DELIMITER //
CREATE TRIGGER `delete_fk_invoice_jo` BEFORE DELETE ON `invoice`
 FOR EACH ROW BEGIN
update joborder
set invoice_no=null
where invoice_no=OLD.no;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `joborder`
--

CREATE TABLE IF NOT EXISTS `joborder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tglMulai` date DEFAULT NULL,
  `pelanggan_user_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `layanan_id` int(11) NOT NULL,
  `bl_id` int(11) NOT NULL,
  `ci_id` int(11) NOT NULL,
  `si_id` int(11) NOT NULL,
  `pl` varchar(45) NOT NULL,
  `plAtc` varchar(245) NOT NULL,
  `sj` varchar(45) NOT NULL,
  `sjAtc` varchar(245) NOT NULL,
  `sk` varchar(45) NOT NULL,
  `skAtc` varchar(245) NOT NULL,
  `status` varchar(5) NOT NULL,
  `pelayaran_user_id` int(11) DEFAULT NULL,
  `tglPelayaran` date DEFAULT NULL,
  `biaya_id` int(11) DEFAULT NULL,
  `manifes` int(1) DEFAULT '0',
  `pib_id` int(11) DEFAULT NULL,
  `jalurAtc` varchar(245) DEFAULT NULL COMMENT 'm=merah;h=hijau;hn=hijau notul',
  `sp2` int(1) DEFAULT '0' COMMENT 'sp2 udah sampai atau blm',
  `notulAngka` int(11) DEFAULT NULL,
  `notulAtc` varchar(245) DEFAULT NULL,
  `sppbAtc` varchar(245) DEFAULT NULL,
  `tglSelesai` date DEFAULT NULL,
  `invoice_kode` varchar(30) DEFAULT NULL,
  `docAtc` varchar(245) DEFAULT NULL,
  `biayaPPJK_id` int(11) DEFAULT NULL,
  `invoice_no` varchar(50) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_onGoingJobOrder_layanan1_idx` (`layanan_id`),
  KEY `fk_onGoingJobOrder_pelayaran1_idx` (`pelayaran_user_id`),
  KEY `fk_onGoingJobOrder_biaya1_idx` (`biaya_id`),
  KEY `fk_onGoingJobOrder_bl1_idx` (`bl_id`),
  KEY `fk_onGoingJobOrder_ci1_idx` (`ci_id`),
  KEY `fk_onGoingJobOrder_si1_idx` (`si_id`),
  KEY `fk_onGoingJobOrder_barang1_idx` (`barang_id`),
  KEY `fk_onGoingJobOrder_pelanggan1_idx` (`pelanggan_user_id`),
  KEY `fk_JobOrder_JobOrderStatus1_idx` (`status`),
  KEY `fk_JobOrder_pib1_idx` (`pib_id`),
  KEY `fk_JobOrder_biayaPPJK1_idx` (`biayaPPJK_id`),
  KEY `fk_JobOrder_invoice1_idx` (`invoice_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `joborder`
--

INSERT INTO `joborder` (`id`, `tglMulai`, `pelanggan_user_id`, `barang_id`, `layanan_id`, `bl_id`, `ci_id`, `si_id`, `pl`, `plAtc`, `sj`, `sjAtc`, `sk`, `skAtc`, `status`, `pelayaran_user_id`, `tglPelayaran`, `biaya_id`, `manifes`, `pib_id`, `jalurAtc`, `sp2`, `notulAngka`, `notulAtc`, `sppbAtc`, `tglSelesai`, `invoice_kode`, `docAtc`, `biayaPPJK_id`, `invoice_no`, `ctime`) VALUES
(1, '2014-03-25', 15, 3, 1, 1, 1, 1, '78576456456', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0051.JPG', '234234234', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0066.JPG', '235345234234', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0070.JPG', 'sppb', NULL, '2014-03-13', 1, 0, NULL, NULL, 0, 2147483647, 'uploaded/usep/2014-04-21 21.06.44-DSCF0459 _crop _small.jpg', 'uploaded/usep/2014-06-01 12.54.14-Penguins.jpg', NULL, NULL, NULL, NULL, NULL, '2014-03-25 14:31:09'),
(2, '2014-04-09', 15, 4, 1, 2, 2, 2, '678678', 'uploaded/dadang/2014-04-09 17.11.22-BERITA ACARA SERAH TERIMA BARANG.docx', '68678678', 'uploaded/dadang/2014-04-09 17.11.22-BERITA ACARA SERAH TERIMA BARANG.docx', '678678678', 'uploaded/dadang/2014-04-09 17.11.22-PEKERJAAN PENGADAAN INTERIOR DAN FURNITURE.docx', 'cdpi3', NULL, '2014-03-21', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-03-25 14:39:10'),
(3, '2014-05-04', 17, 5, 1, 3, 3, 3, '123123', 'uploaded/yunero/2014-05-04 17.10.38-Lighthouse.jpg', '123123', 'uploaded/yunero/2014-05-04 17.10.38-Jellyfish.jpg', '123123123', 'uploaded/yunero/2014-05-04 17.10.38-Tulips.jpg', 'jm61', NULL, '2014-05-14', 2, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-04 10:04:57'),
(4, '2014-05-25', 17, 6, 1, 4, 4, 4, 'asdasdasd', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.png', 'asdasdas', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.png', 'dasdasdasd', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.jpg', 'jm', NULL, '2014-05-27', NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-25 07:22:46'),
(5, '2014-05-29', 18, 8, 1, 6, 5, 6, '44444444', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.jpg', '44444444444', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.png', '4444444444', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.png', 'jh1', NULL, '2014-05-22', 2, 0, NULL, 'uploaded/usep/2014-05-29 15.47.18-installer_prefs.json', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-29 04:26:55'),
(6, '2014-05-31', 21, 9, 1, 7, 6, 7, '123513261', 'uploaded/koko/2014-05-31 14.33.07-Tulips.jpg', '132513261', 'uploaded/koko/2014-05-31 14.33.07-Desert.jpg', '5126123', 'uploaded/koko/2014-05-31 14.33.07-Chrysanthemum.jpg', 'inv', NULL, '2014-05-31', 4, 0, NULL, 'uploaded/usep/2014-05-31 15.33.52-Jellyfish.jpg', 0, 112312312, 'uploaded/usep/2014-05-31 22.03.03-version', 'uploaded/usep/2014-06-01 08.35.17-Data', NULL, NULL, NULL, 2, '6/TSA/2014-05-31', '2014-05-31 07:33:07'),
(7, '2014-05-31', 17, 11, 1, 8, 7, 9, 'asdasd', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.png', 'asdasd', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.png', 'asdasd', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.jpg', 'init', NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-05-31 07:36:25'),
(8, '2014-06-01', 22, 12, 1, 9, 8, 10, '61351', 'uploaded/tes/2014-06-01 12.39.47-Jellyfish.jpg', '6136123', 'uploaded/tes/2014-06-01 12.39.47-Hydrangeas.jpg', '16123124', 'uploaded/tes/2014-06-01 12.39.47-Lighthouse.jpg', 'inv', NULL, '2014-06-02', 5, 0, NULL, 'uploaded/usep/2014-06-01 13.15.39-Jellyfish.jpg', 0, 1500000, 'uploaded/usep/2014-06-01 13.25.23-Jellyfish.jpg', 'uploaded/usep/2014-06-01 15.12.29-Desert.jpg', NULL, NULL, NULL, 3, '8/TSA/2014-06-01', '2014-06-01 05:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `joborderstatus`
--

CREATE TABLE IF NOT EXISTS `joborderstatus` (
  `kode` varchar(5) NOT NULL,
  `status` varchar(245) NOT NULL,
  `urutan` float NOT NULL DEFAULT '0',
  `ket` text,
  `ric` varchar(5) NOT NULL COMMENT 'role yg bertanggung jawab',
  PRIMARY KEY (`kode`),
  KEY `fk_JobOrderStatus_userRole1_idx` (`ric`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `joborderstatus`
--

INSERT INTO `joborderstatus` (`kode`, `status`, `urutan`, `ket`, `ric`) VALUES
('bjtb', 'Biaya kepabeanan telah dibayar', 14, NULL, 'PEL'),
('bjtb1', 'Pembayaran kepabeanan telah diterima', 15, NULL, 'MK'),
('bukba', 'Bukti bayar telah diupload', 6, NULL, 'MK'),
('byop', 'Biaya telah dihitung', 4, NULL, 'OP'),
('cdpi0', 'Cek dokumen pendukung import (rejected)', 2, NULL, 'OP'),
('cdpi1', 'Cek dokumen pendukung import (approved)', 2, NULL, 'OP'),
('cdpi3', 'Cek dokumen pendukung import (revised)', 3, NULL, 'OP'),
('hti', 'Biaya PPJK telah diinput', 12, NULL, 'MK'),
('init', 'initial', 0, NULL, 'OP'),
('inv', 'Invoice telah dikirim', 13, NULL, 'MK'),
('jh1', 'Jalur Hijau', 9.1, NULL, 'PEL'),
('jhn1', 'Jalur Hijau+ - Notul telah dibayar', 9.21, NULL, 'PEL'),
('jhn2', 'Jalur Hijau+ - Dokumen telah diupload', 9.22, NULL, 'PEL'),
('jhn31', 'Jalur Hijau+ - Dokumen telah dicek (accepted)', 9.231, NULL, 'OP'),
('jhn32', 'Jalur Hijau+ - Dokumen telah dicek (rejected)', 9.232, NULL, 'OP'),
('jhn33', 'Jalur Hijau+ - Dokumen telah dicek (revised)', 9.233, NULL, 'PEL'),
('jhp', 'Jalur Hijau +', 9, NULL, 'MO'),
('jm', 'Jalur Merah', 9, NULL, 'MO'),
('jm1', 'Jalur Merah - Pajak telah dihitung ulang', 9.31, NULL, 'OP'),
('jm21', 'Jalur Merah - Pajak telah dicek (accepted)', 9.321, NULL, 'MO'),
('jm22', 'Jalur Merah - Pajak telah dicek (rejected)', 9.322, NULL, 'MO'),
('jm23', 'Jalur Merah - Pajak telah dicek (revised)', 9.333, NULL, 'OP'),
('jm3', 'Jalur Merah - notul sudah diinput', 9.4, NULL, 'OP'),
('jm4', 'Jalur Merah - notul sudah dibayar', 9.5, NULL, 'PEL'),
('jm5', 'Jalur Merah - Dokumen sudah diupload', 9.6, NULL, 'PEL'),
('jm61', 'Jalur Merah - Dokumen sudah dicek (accepted)', 9.71, NULL, 'OP'),
('jm62', 'Jalur Merah - Dokumen sudah dicek (rejected)', 9.72, NULL, 'OP'),
('jm63', 'Jalur Merah - Dokumen sudah dicek (revised)', 9.73, NULL, 'OP'),
('ljoc', 'Laporan Job Order complete telah dibuat', 16, NULL, 'DIR'),
('ljoc1', 'Laporan Job Order Complete telah diotorisasi', 17, NULL, 'DIR'),
('man', 'Manifes telah diambil', 5, NULL, 'OP'),
('pibis', 'PIB telah diisi', 7, NULL, 'MO'),
('pibot', 'PIB telah diotorisasi', 8, NULL, 'MO'),
('sp2', 'SP2 telah diotorisasi', 11, NULL, 'OP'),
('sppb', 'SPPB telah dicetak', 10, NULL, 'OP'),
('tPLY', 'Tgl Pelayaran telah di-set', 1, NULL, 'PEL');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`user_id`, `nama`, `alamat`, `telp`, `email`, `ctime`) VALUES
(10, 'Manager Administrasi', 'jalan buntung', '354364354', 'adm@adm', '2014-02-27 12:26:09'),
(11, 'MK', 'jalan buntung', '354364354', 'adm@adm', '2014-02-28 07:03:55'),
(12, 'MO', 'jalan buntung', '354364354', 'adm@adm', '2014-02-28 07:03:55'),
(13, 'sdf', 'jalan buntung', '354364354354364354', 'adm@adm', '2014-02-28 07:03:55'),
(14, 'sdf', 'jalan buntung', '354364354', 'adm@adm', '2014-02-28 07:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `layanan`
--

CREATE TABLE IF NOT EXISTS `layanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(145) NOT NULL,
  `biaya` int(11) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `layanan`
--

INSERT INTO `layanan` (`id`, `nama`, `biaya`, `ctime`) VALUES
(1, 'import', 510000, '2014-03-25 14:27:27');

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE IF NOT EXISTS `notif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dari` int(11) NOT NULL,
  `roleDari` varchar(5) NOT NULL,
  `ke` int(11) NOT NULL,
  `roleKe` varchar(5) NOT NULL,
  `tabel` varchar(45) NOT NULL,
  `field` varchar(45) NOT NULL,
  `isRead` int(1) DEFAULT '0',
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_notif_user1_idx` (`dari`),
  KEY `fk_notif_user2_idx` (`ke`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pelabuhan_bongkar`
--

CREATE TABLE IF NOT EXISTS `pelabuhan_bongkar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaPOD` varchar(145) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pelabuhan_muat`
--

CREATE TABLE IF NOT EXISTS `pelabuhan_muat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaPOL` varchar(145) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE IF NOT EXISTS `pelanggan` (
  `user_id` int(11) NOT NULL,
  `ktp` varchar(45) NOT NULL,
  `nama` varchar(145) NOT NULL,
  `role` int(1) NOT NULL DEFAULT '1' COMMENT '1=importir perusahaan; 2=importir perorangan;',
  `email` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `npwp` varchar(45) DEFAULT NULL,
  `noapi` varchar(45) DEFAULT NULL,
  `npik` varchar(45) DEFAULT NULL,
  `telp` varchar(45) DEFAULT NULL,
  `web` varchar(25) DEFAULT NULL,
  `siupAtc` varchar(245) DEFAULT NULL,
  `npwpAtc` varchar(245) DEFAULT NULL,
  `ktpAtc` varchar(245) DEFAULT NULL,
  `isVerified` int(1) DEFAULT '0',
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  KEY `fk_pelanggan_user_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`user_id`, `ktp`, `nama`, `role`, `email`, `alamat`, `npwp`, `noapi`, `npik`, `telp`, `web`, `siupAtc`, `npwpAtc`, `ktpAtc`, `isVerified`, `ctime`) VALUES
(9, '32654614631684611617', 'PT Kavex Sejahtera', 1, 'ptkavex@gmail.com', '234243adsasdasd', '', '', '', '234234', '', 'uploaded/ptkavex/2014-02-26 15.09.07-2x3.jpg', 'uploaded/ptkavex/2014-02-26 15.09.07-3x4 resize.jpg', 'uploaded/ptkavex/2014-02-26 15.09.07-3x4 resize2.jpg', 1, '2014-02-26 08:09:07'),
(15, '14233412432432423', 'dadang sumarja', 2, 'dadang@gmail.com', 'Jalan kemuning', '423243432', '2543243423', '423423423', '08993453456', 'www.dadang.com', 'uploaded/dadang/2014-03-23 14.44.58-Chrysanthemum.jpg', 'uploaded/dadang/2014-03-23 14.44.58-Desert.jpg', 'uploaded/dadang/2014-03-23 14.44.58-Penguins.jpg', 0, '2014-03-23 07:44:58'),
(17, '234432234423423', 'yunero', 1, 'yunero@gmail.com', 'jalan pajajaran', '23432423', '3234234234', '234234234', '5423543543', '234234', 'uploaded/yunero/2014-05-04 16.46.08-Desert.jpg', 'uploaded/yunero/2014-05-04 16.46.08-Hydrangeas.jpg', 'uploaded/yunero/2014-05-04 16.46.08-Tulips.jpg', 0, '2014-05-04 09:46:08'),
(18, '123123', 'tata', 1, 'tata@gmail.com', '23123123123', '12123123', '123123', '123123123', '1231', 'asdasdasd', 'uploaded/tata/2014-05-29 11.00.15-Arthemes Logo.jpg', 'uploaded/tata/2014-05-29 11.00.15-Arthemes Logo.png', 'uploaded/tata/2014-05-29 11.00.15-Arthemes Logo.jpg', 0, '2014-05-29 04:00:15'),
(19, '123123', 'tata', 1, 'tataa@gmail.com', '23123123123', '12123123', '123123', '123123123', '1231', 'asdasdasd', 'uploaded/tata12/2014-05-29 11.07.16-Arthemes Logo.jpg', 'uploaded/tata12/2014-05-29 11.07.16-Arthemes Logo.png', 'uploaded/tata12/2014-05-29 11.07.16-Arthemes Logo.jpg', 0, '2014-05-29 04:07:16'),
(20, '123123', 'tata', 1, 'aaaaa@gmail.com', '23123123123', '12123123', '123123', '123123123', '1231', 'asdasdasd', 'uploaded/aaaaa/2014-05-29 11.07.50-Arthemes Logo.png', 'uploaded/aaaaa/2014-05-29 11.07.50-Arthemes Logo.png', 'uploaded/aaaaa/2014-05-29 11.07.50-Arthemes Logo.jpg', 0, '2014-05-29 04:07:50'),
(21, '123141', 'koko', 1, 'koko@gmail.com', 'koko', '125132612', '125126', '16261', '15125', 'koko', 'uploaded/koko/2014-05-31 14.31.15-Penguins.jpg', 'uploaded/koko/2014-05-31 14.31.15-Tulips.jpg', 'uploaded/koko/2014-05-31 14.31.15-Hydrangeas.jpg', 0, '2014-05-31 07:31:15'),
(22, '321', 'tes', 1, 'tes@gmail.com', 'tes', '512', '5151', '1125', '412', 'tes', 'uploaded/tes/2014-06-01 12.37.25-Penguins.jpg', 'uploaded/tes/2014-06-01 12.37.25-Hydrangeas.jpg', 'uploaded/tes/2014-06-01 12.37.25-Tulips.jpg', 0, '2014-06-01 05:37:25');

-- --------------------------------------------------------

--
-- Stand-in structure for view `pelangganv`
--
CREATE TABLE IF NOT EXISTS `pelangganv` (
`id` int(11)
,`username` varchar(10)
,`password` varchar(245)
,`namauser` varchar(245)
,`roleUser` varchar(5)
,`last_login` timestamp
,`user_id` int(11)
,`ktp` varchar(45)
,`nama` varchar(145)
,`rolePelanggan` int(1)
,`email` varchar(45)
,`alamat` text
,`npwp` varchar(45)
,`noapi` varchar(45)
,`npik` varchar(45)
,`telp` varchar(45)
,`web` varchar(25)
,`siupAtc` varchar(245)
,`npwpAtc` varchar(245)
,`ktpAtc` varchar(245)
,`isVerified` int(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `pelayaran`
--

CREATE TABLE IF NOT EXISTS `pelayaran` (
  `user_id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `web` varchar(45) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaraninvoice`
--

CREATE TABLE IF NOT EXISTS `pembayaraninvoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jml` int(11) NOT NULL,
  `tglBayar` date NOT NULL,
  `ket` text,
  `atc` varchar(245) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pembayaraninvoice`
--

INSERT INTO `pembayaraninvoice` (`id`, `jml`, `tglBayar`, `ket`, `atc`, `ctime`) VALUES
(1, 5000000, '2014-06-10', 'asdasdasdas', 'uploaded/koko/2014-06-01 17.54.11-Data', '2014-06-01 10:54:11'),
(2, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.06.27-Data', '2014-06-01 11:06:27'),
(3, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.06.55-Data', '2014-06-01 11:06:55'),
(4, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.07.11-Data', '2014-06-01 11:07:11'),
(5, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.08.15-Data', '2014-06-01 11:08:15'),
(6, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.09.08-Data', '2014-06-01 11:09:08'),
(7, 123123, '2014-06-04', 'asdasd', 'uploaded/koko/2014-06-01 18.09.22-Data', '2014-06-01 11:09:22'),
(8, 6, '2014-06-04', '', 'uploaded/tes/2014-06-01 18.58.09-Penguins.jpg', '2014-06-01 11:58:09');

-- --------------------------------------------------------

--
-- Table structure for table `pib`
--

CREATE TABLE IF NOT EXISTS `pib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noPengajuan` varchar(45) NOT NULL,
  `tglPengajuan` date NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `noPengajuan_UNIQUE` (`noPengajuan`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pib`
--

INSERT INTO `pib` (`id`, `noPengajuan`, `tglPengajuan`, `ctime`) VALUES
(4, '18248', '2014-05-31', '2014-05-31 08:32:31'),
(5, '161231', '2014-06-03', '2014-06-01 05:46:19');

-- --------------------------------------------------------

--
-- Table structure for table `si`
--

CREATE TABLE IF NOT EXISTS `si` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noSI` varchar(30) NOT NULL,
  `tglTiba` date NOT NULL,
  `tglBrgkt` date NOT NULL,
  `atc` varchar(245) NOT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nosi_UNIQUE` (`noSI`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `si`
--

INSERT INTO `si` (`id`, `noSI`, `tglTiba`, `tglBrgkt`, `atc`, `ctime`) VALUES
(1, '34234234', '2014-02-02', '2014-02-02', 'uploaded/dadang/2014-03-25 21.31.09-DSCF0045.JPG', '2014-03-25 14:31:09'),
(2, '3643634', '2014-02-02', '2014-02-02', 'uploaded/dadang/2014-04-09 17.11.22-BERITA ACARA SERAH TERIMA BARANG.docx', '2014-03-25 14:39:10'),
(3, '213312323', '2014-02-02', '2014-02-02', 'uploaded/yunero/2014-05-04 17.10.38-Lighthouse.jpg', '2014-05-04 10:04:57'),
(4, 'asdasdasd', '2014-02-02', '2014-02-02', 'uploaded/yunero/2014-05-25 14.40.10-Arthemes Logo.png', '2014-05-25 07:22:46'),
(6, '44444444', '2014-05-14', '2014-05-08', 'uploaded/tata/2014-05-29 11.26.55-Arthemes Logo.jpg', '2014-05-29 04:26:55'),
(7, '1211', '2014-05-31', '2014-05-31', 'uploaded/koko/2014-05-31 14.33.07-Tulips.jpg', '2014-05-31 07:33:07'),
(9, 'aasdas', '2014-05-06', '2014-05-08', 'uploaded/yunero/2014-05-31 14.36.24-Arthemes Logo.jpg', '2014-05-31 07:36:24'),
(10, '1613', '2014-06-03', '2014-06-01', 'uploaded/tes/2014-06-01 12.39.47-Penguins.jpg', '2014-06-01 05:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `tarif_pengiriman`
--

CREATE TABLE IF NOT EXISTS `tarif_pengiriman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pelabuhan_muat_id` int(11) NOT NULL,
  `pelabuhanBongkar_id` int(11) NOT NULL,
  `biaya` int(11) NOT NULL,
  `ukContainer` varchar(45) DEFAULT NULL,
  `tipeContainer` varchar(45) DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tarif_pengiriman_pelabuhan_muat1_idx` (`pelabuhan_muat_id`),
  KEY `fk_tarif_pengiriman_pelabuhanBongkar1_idx` (`pelabuhanBongkar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(245) NOT NULL,
  `nama` varchar(245) NOT NULL,
  `role` varchar(5) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `ctime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_user_userRole1_idx` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `role`, `last_login`, `ctime`) VALUES
(9, 'ptkavex', '$2a$12$ZdQ1fYyzqnHq.S1zUuiUOu66Ss//s0wvOUsQ3XRiHCPDGAolqH0Su', 'PT Kavex Sejahtera', 'PEL', '2014-05-29 04:45:28', '2014-02-26 08:09:07'),
(10, 'madmin', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Manager Administrasi', 'MA', '2014-06-01 12:05:04', '2014-02-27 12:24:19'),
(11, 'mkeu', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Manager Keuangan', 'MK', '2014-06-02 14:58:29', '2014-02-28 07:01:40'),
(12, 'moper', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Manager Operasional', 'MO', '2014-06-01 12:06:09', '2014-02-28 07:01:40'),
(13, 'direktur', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Direktur', 'DIR', NULL, '2014-02-28 07:01:40'),
(14, 'pelayaran', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Pelayaran', 'PLY', '2014-06-01 05:40:29', '2014-02-28 07:01:40'),
(15, 'dadang', '$2a$12$WDUd6mbogGapdeI1gZ8jquyPQrZ8nDPV3YdaKPJHofe/LV8kJ6SLe', 'dadang sumarja', 'PEL', '2014-04-21 14:09:39', '2014-03-23 07:44:58'),
(16, 'usep', '$2a$12$3NTqJvLyesnIFrJxD7KVjO63pD7ifDtZKoKPDgJ6eVmd2/eKj/ZL6', 'Usep Operator', 'OP', '2014-06-01 12:06:30', '2014-03-31 07:21:13'),
(17, 'yunero', '$2a$12$Ts6jjimP3L47Vf6V5CKYJuH7G.gcu5I6bx3W0yH8r0k2EJg8JsPoy', 'yunero', 'PEL', '2014-06-01 10:39:36', '2014-05-04 09:46:08'),
(18, 'tata', '$2a$12$SlnEWBxlRsbKV6NGrKRhiOd8la7EN2isngCXAaKyRFDZAM9hUIpM2', 'tata', 'PEL', '2014-05-29 04:08:09', '2014-05-29 04:00:15'),
(19, 'tata12', '$2a$12$bPAihf7aQYZs7W/l97s5ZOVPd4x.7CNyhS4GLKbezq3zckC33r/7m', 'tata', 'PEL', NULL, '2014-05-29 04:07:16'),
(20, 'aaaaa', '$2a$12$0yY.D3rO7.P1oe9VXEKwZOEfpK1y9senkCt.Za.a47rcWkmXd2G7q', 'tata', 'PEL', NULL, '2014-05-29 04:07:50'),
(21, 'koko', '$2a$12$NpbXozfMJcGg2YRiYRxdCemeLjBe0ss9DdgMMlG5sKM84Y3M8a2gO', 'koko', 'PEL', '2014-06-02 12:33:12', '2014-05-31 07:31:15'),
(22, 'tes', '$2a$12$.VrUWKIPiRhkUsHIgMMk9OxHDqg2OaZm0h0vSYOvvmP7MDnYGPKM6', 'tes', 'PEL', '2014-06-01 12:29:46', '2014-06-01 05:37:25');

--
-- Triggers `user`
--
DROP TRIGGER IF EXISTS `delete_all_user`;
DELIMITER //
CREATE TRIGGER `delete_all_user` BEFORE DELETE ON `user`
 FOR EACH ROW begin
delete from pelanggan where user_id=old.id;
delete from pelayaran where user_id=old.id;
delete from karyawan where user_id=old.id;
delete from notif where dari=old.id OR ke=old.id;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE IF NOT EXISTS `userrole` (
  `kode` varchar(5) NOT NULL,
  `role` varchar(45) NOT NULL,
  `ket` text,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`kode`, `role`, `ket`) VALUES
('DIR', 'Direktur', NULL),
('MA', 'Manager Administrasi', NULL),
('MK', 'Manager Keuangan', NULL),
('MO', 'Manager Operasional', NULL),
('OP', 'Operator', NULL),
('PEL', 'Pelanggan', NULL),
('PLY', 'Pelayaran', NULL);

-- --------------------------------------------------------

--
-- Structure for view `createjoborder`
--
DROP TABLE IF EXISTS `createjoborder`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `createjoborder` AS select `a`.`id` AS `id`,`a`.`barang_id` AS `barang_id`,`a`.`layanan_id` AS `layanan_id`,`a`.`bl_id` AS `bl_id`,`a`.`ci_id` AS `ci_id`,`a`.`si_id` AS `si_id`,`a`.`pl` AS `pl`,`a`.`plAtc` AS `plAtc`,`a`.`sj` AS `sj`,`a`.`sjAtc` AS `sjAtc`,`a`.`sk` AS `sk`,`a`.`skAtc` AS `skAtc`,`b`.`noSI` AS `noSI`,`b`.`tglTiba` AS `tglTiba`,`b`.`tglBrgkt` AS `tglBrgkt`,`b`.`atc` AS `si_atc`,`c`.`noCI` AS `noCI`,`c`.`tgl` AS `ci_tgl`,`c`.`atc` AS `ci_atc`,`d`.`nomor` AS `nomor`,`d`.`tgl` AS `bl_tgl`,`d`.`atc` AS `atc`,`e`.`nama` AS `nama`,`e`.`des` AS `des`,`e`.`jml` AS `jml`,`e`.`bruto` AS `bruto`,`e`.`netto` AS `netto`,`e`.`volume` AS `volume` from ((((`joborder` `a` join `si` `b`) join `ci` `c`) join `bl` `d`) join `barang` `e`) where ((`a`.`barang_id` = `e`.`id`) and (`a`.`bl_id` = `d`.`id`) and (`a`.`ci_id` = `c`.`id`) and (`a`.`si_id` = `b`.`id`));

-- --------------------------------------------------------

--
-- Structure for view `pelangganv`
--
DROP TABLE IF EXISTS `pelangganv`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pelangganv` AS select `a`.`id` AS `id`,`a`.`username` AS `username`,`a`.`password` AS `password`,`a`.`nama` AS `namauser`,`a`.`role` AS `roleUser`,`a`.`last_login` AS `last_login`,`b`.`user_id` AS `user_id`,`b`.`ktp` AS `ktp`,`b`.`nama` AS `nama`,`b`.`role` AS `rolePelanggan`,`b`.`email` AS `email`,`b`.`alamat` AS `alamat`,`b`.`npwp` AS `npwp`,`b`.`noapi` AS `noapi`,`b`.`npik` AS `npik`,`b`.`telp` AS `telp`,`b`.`web` AS `web`,`b`.`siupAtc` AS `siupAtc`,`b`.`npwpAtc` AS `npwpAtc`,`b`.`ktpAtc` AS `ktpAtc`,`b`.`isVerified` AS `isVerified` from (`user` `a` left join `pelanggan` `b` on((`a`.`id` = `b`.`user_id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buktibayar`
--
ALTER TABLE `buktibayar`
  ADD CONSTRAINT `fk_buktiBayar_JobOrder1` FOREIGN KEY (`JobOrder_id`) REFERENCES `joborder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dokumen`
--
ALTER TABLE `dokumen`
  ADD CONSTRAINT `fk_buktiBayar_JobOrder10` FOREIGN KEY (`JobOrder_id`) REFERENCES `joborder` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_user1` FOREIGN KEY (`mk_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pembayaranInvoice1` FOREIGN KEY (`pembayaranInvoice_id`) REFERENCES `pembayaraninvoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `joborder`
--
ALTER TABLE `joborder`
  ADD CONSTRAINT `fk_JobOrder_biayaPPJK1` FOREIGN KEY (`biayaPPJK_id`) REFERENCES `biayappjk` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_JobOrder_invoice1` FOREIGN KEY (`invoice_no`) REFERENCES `invoice` (`no`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_JobOrder_JobOrderStatus1` FOREIGN KEY (`status`) REFERENCES `joborderstatus` (`kode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_JobOrder_pib1` FOREIGN KEY (`pib_id`) REFERENCES `pib` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_barang1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_biaya1` FOREIGN KEY (`biaya_id`) REFERENCES `biaya` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_bl1` FOREIGN KEY (`bl_id`) REFERENCES `bl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_ci1` FOREIGN KEY (`ci_id`) REFERENCES `ci` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_layanan1` FOREIGN KEY (`layanan_id`) REFERENCES `layanan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_pelanggan1` FOREIGN KEY (`pelanggan_user_id`) REFERENCES `pelanggan` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_pelayaran1` FOREIGN KEY (`pelayaran_user_id`) REFERENCES `pelayaran` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_onGoingJobOrder_si1` FOREIGN KEY (`si_id`) REFERENCES `si` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `joborderstatus`
--
ALTER TABLE `joborderstatus`
  ADD CONSTRAINT `fk_JobOrderStatus_userRole1` FOREIGN KEY (`ric`) REFERENCES `userrole` (`kode`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `fk_pelayaran_user10` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif`
--
ALTER TABLE `notif`
  ADD CONSTRAINT `fk_notif_user1` FOREIGN KEY (`dari`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notif_user2` FOREIGN KEY (`ke`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `fk_pelanggan_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pelayaran`
--
ALTER TABLE `pelayaran`
  ADD CONSTRAINT `fk_pelayaran_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tarif_pengiriman`
--
ALTER TABLE `tarif_pengiriman`
  ADD CONSTRAINT `fk_tarif_pengiriman_pelabuhanBongkar1` FOREIGN KEY (`pelabuhanBongkar_id`) REFERENCES `pelabuhan_bongkar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tarif_pengiriman_pelabuhan_muat1` FOREIGN KEY (`pelabuhan_muat_id`) REFERENCES `pelabuhan_muat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_userRole1` FOREIGN KEY (`role`) REFERENCES `userrole` (`kode`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
