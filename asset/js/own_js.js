$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
}
function toglogin(){
    $('.loginbox').toggle('blind',300);
    // return false;
}
function callmodal(name){
	$(name).modal('show');
	// alert(name);
}

 $(document).ready(function(){
 	$("#LoginForm").submit(function(){
 		$("#loadingdiv").fadeIn(200);
 		var data=$("#LoginForm").serialize();
 		$.ajax({
 			url:base_url+'main/login',
 			data:data,
 			type:"POST",
 			success:function(data){
 				$("#loadingdiv").fadeOut(200);
 				if (data=='gagal'){
 					$(".logingagal").fadeIn(200);
 					$(".loginberhasil").hide();
 				}else{
 					$(".loginberhasil").fadeIn(200);
 					$(".logingagal").hide();
 					location.href=base_url+'joborder/admin';
 				}
 				
 			}
 		})
 		return false;
 	})

 	$('.loginbox').hide();
 	// $(".select2").select2();
 	// $('.timepicker').timepicker();

	// === Sidebar navigation === //
	
	$('.submenu > a').click(function(e)
	{
		e.preventDefault();
		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');
		var submenus = $('#sidebar li.submenu ul');
		var submenus_parents = $('#sidebar li.submenu');
		if(li.hasClass('open'))
		{
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				// submenu.slideUp();
			} else {
				submenu.fadeOut(250);
			}
			li.removeClass('open');
		} else 
		{
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				submenus.slideUp();			
				submenu.slideDown();
			} else {
				submenus.fadeOut(250);			
				submenu.fadeIn(250);
			}
			submenus_parents.removeClass('open');		
			li.addClass('open');	
		}
	});
	
	var ul = $('#sidebar > ul');
	
	$('#sidebar > a').click(function(e)
	{
		e.preventDefault();
		var sidebar = $('#sidebar');
		if(sidebar.hasClass('open'))
		{
			sidebar.removeClass('open');
			ul.slideUp(250);
		} else 
		{
			sidebar.addClass('open');
			ul.slideDown(250);
		}
	});
})