'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var ser= angular.module('myApp.services', []);
ser.run(function($http) {
	//for API auth
  	$http.defaults.headers.common.AUTH = 'toshioyoiibgt';
});