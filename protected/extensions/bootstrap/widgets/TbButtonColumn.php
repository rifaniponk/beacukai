<?php
/**
 * TbButtonColumn class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright  Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.8
 */

Yii::import('zii.widgets.grid.CButtonColumn');

/**
 * Bootstrap button column widget.
 * Used to set buttons to use Glyphicons instead of the defaults images.
 */
class TbButtonColumn extends CButtonColumn
{
	/**
	 * @var string the view button icon (defaults to 'eye-open').
	 */
	public $viewButtonIcon = 'eye-open';
	/**
	 * @var string the update button icon (defaults to 'pencil').
	 */
	public $updateButtonIcon = 'pencil';
	/**
	 * @var string the delete button icon (defaults to 'trash').
	 */
	public $deleteButtonIcon = 'trash';

	public $assignButtonIcon = 'list-alt';

	public $setButtonIcon = 'check';
	public $addButtonIcon= 'plus-sign';
	public $moneyButtonIcon= 'money';
	public $printButtonIcon = 'print';
	public $btnClass='btn mini';

	// public $emptyText= '';
	// public $imagePathExpression='';
	// public $usePlaceHoldIt='';
	// public $usePlaceKitten='';

	/**
	 * Initializes the default buttons (view, update and delete).
	 */
	protected function initDefaultButtons()
	{
		parent::initDefaultButtons();

		if ($this->viewButtonIcon !== false && !isset($this->buttons['view']['icon']))
			$this->buttons['view']['icon'] = $this->viewButtonIcon;
		if ($this->assignButtonIcon !== false && !isset($this->buttons['assign']['icon']))
			$this->buttons['assign']['icon'] = $this->assignButtonIcon;
		if ($this->updateButtonIcon !== false && !isset($this->buttons['update']['icon']))
			$this->buttons['update']['icon'] = $this->updateButtonIcon;
		if ($this->deleteButtonIcon !== false && !isset($this->buttons['delete']['icon']))
			$this->buttons['delete']['icon'] = $this->deleteButtonIcon;
		if ($this->setButtonIcon !== false && !isset($this->buttons['set']['icon']))
			$this->buttons['set']['icon'] = $this->setButtonIcon;
		if ($this->addButtonIcon !== false && !isset($this->buttons['add']['icon']))
			$this->buttons['add']['icon'] = $this->addButtonIcon;
		if ($this->moneyButtonIcon !== false && !isset($this->buttons['money']['icon']))
			$this->buttons['money']['icon'] = $this->moneyButtonIcon;
		if ($this->printButtonIcon !== false && !isset($this->buttons['print']['icon']))
			$this->buttons['print']['icon'] = $this->printButtonIcon;
	}

	/**
	 * Renders a link button.
	 * @param string $id the ID of the button
	 * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data object associated with the row
	 */
	protected function renderButton($id, $button, $row, $data)
	{
		if (isset($button['visible']) && !$this->evaluateExpression($button['visible'], array('row'=>$row, 'data'=>$data)))
			return;

		$label = isset($button['label']) ? $button['label'] : $id;
		$url = isset($button['url']) ? $this->evaluateExpression($button['url'], array('data'=>$data, 'row'=>$row)) : '#';
		$options = isset($button['options']) ? $button['options'] : array();

		if (!isset($options['title']))
			$options['title'] = $label;

		if (!isset($options['rel']))
			$options['rel'] = 'tooltip';

		if (isset($button['icon']))
		{
			if (strpos($button['icon'], 'icon') === false)
				$button['icon'] = 'icon-'.implode(' icon-', explode(' ', $button['icon']));

			echo CHtml::link('<i class="'.$button['icon'].'"></i>', $url, $options);
		}
		else if (isset($button['imageUrl']) && is_string($button['imageUrl']))
			echo CHtml::link(CHtml::image($button['imageUrl'], $label), $url, $options);
		else{
			if (!isset($button['btnClass'])){
				$button['btnClass']=$this->btnClass;
			}
			$options['class']=$button['btnClass'];
			echo CHtml::link($label, $url, $options);
		}
	}

	public function renderDataCellContent($row,$data)
	{
	    $tr=array();
	    ob_start();
	    foreach($this->buttons as $id=>$button)
	    {
	        $this->renderButton($id,$button,$row,$data);
	        $tr['{'.$id.'}']=ob_get_contents();
	        ob_clean();
	    }
	    ob_end_clean();
	    echo strtr($this->template,$tr);
	}

}

