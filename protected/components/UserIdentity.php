<?php
class UserIdentity extends CUserIdentity{
	private $_id;
	private $_username;
    
	public function authenticate()
	{	
		$user=User::model()->findByAttributes(array('username'=>strtolower($this->username)));
		if($user===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if(!Yii::app()->hash->pass->compare($this->password,$user->password)){
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id=$user->id;
            $this->_username=$user->username;
            $this->errorCode=self::ERROR_NONE;
            //update last login
            $user->last_login=date('Y-m-d H:i:s');
            $user->save();
            
        }
        return !$this->errorCode;
	}

	public function getId()
    {
        return $this->_id;
    }
    public function getUsername()
    {
        return $this->_username;
    }
    public function getRol(){
    	return $this->_roles;
    }
}