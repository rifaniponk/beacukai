<?php
class WebUser extends CWebUser{

 protected $_model;
    function getRole(){
      $user = $this->loadUser();
        if ($user){
            if ($user->role=='MA')
                return "Manager Administrasi";
            if ($user->role=='MK')
                return "Manager Keuangan";
            if ($user->role=='MO')
                return "Manager Operasional";
            if ($user->role=='OP')
                return "Operator";
            if ($user->role=='PEL')
                return "Pelanggan";
            if ($user->role=='PLY')
                return "Pelayaran";
            if ($user->role=='DIR')
                return "Direktur";
        }
        return false;
    }
    function getRoleCode(){
      $user = $this->loadUser();
        if ($user){
            return $user->role;
        }
        return false;
    }
    function isMA(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='MA')
           return true;
        }
        return false;
    }
    function isMK(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='MK')
           return true;
        }
        return false;
    }
    function isMO(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='MO')
           return true;
        }
        return false;
    }
    function isOP(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='OP')
           return true;
        }
        return false;
    }
    function isPEL(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='PEL')
           return true;
        }
        return false;
    }
    function isPLY(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='PLY')
           return true;
        }
        return false;
    }
    function isDIR(){
        $user = $this->loadUser();
        if ($user){
          if ($user->role=='DIR')
           return true;
        }
        return false;
    }
    public function getUserModel(){
      $this->loadUser();
      return $this->_model;
    }
    // Load user model.
    protected function loadUser()
    {
        if ( $this->_model === null ) {
                $this->_model = User::model()->findByPk( $this->id );
        }
        return $this->_model;
    }

}