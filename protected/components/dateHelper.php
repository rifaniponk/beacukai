<?php
class dateHelper{
	static public function tgl_indo($tgl,$returnday=false,$singkat=true,$returnTime=false) {
		//2011-01-01
		if (dateHelper::is_timestamp($tgl)){
			$tanggal = substr($tgl,8,2);
			$bulan	 = dateHelper::get_bulan(substr($tgl, 5, 2),$singkat);
			$tahun 	 = substr($tgl, 0, 4);
			if ($returnday){
				$day = date('D', $tgl);
				return dateHelper::get_hari($day).', '.$tanggal.' '.$bulan.' '.$tahun;
			}
			$formattedDate = $tanggal.' '.$bulan.' '.$tahun;
		}else{
			$tanggal = substr($tgl,8,2);
			$bulan	 = dateHelper::get_bulan(substr($tgl, 5, 2),$singkat);
			$tahun 	 = substr($tgl, 0, 4);
			if ($returnday){
				$day = date('D', strtotime($tgl));
				return dateHelper::get_hari($day).', '.$tanggal.' '.$bulan.' '.$tahun;
			}
			$formattedDate = $tanggal.' '.$bulan.' '.$tahun;
		}

		if ($returnTime){
			$formattedDate .=" [".substr($tgl, -8, 5)."]";
		}
		return $formattedDate;
	}

	static public function is_timestamp($date){
		$e=explode(' ', $date);
		if (count($e)>=2){
			return true;
		}else{
			return false;
		}
	}
	static public function get_hari($hari){
		switch ($hari) {
			case 'Sun': return "Minggu";break;
			case 'Mon': return "Senin";break;
			case 'Tue': return "Selasa";break;
			case 'Wed': return "Rabu";break;
			case 'Thu': return "Kamis";break;
			case 'Fri': return "Jumat";break;
			case 'Sat': return "sabtu";break;
			default:
				return "-";
				break;
		}
	}
	static public function get_bulan($bln,$singkat=false) {
		if ($singkat){
			switch ($bln) {
				case 1:
					return "Jan";
					break;
				case 2:
					return "Feb";
					break;
				case 3:
					return "Mar";
					break;
				case 4:
					return "Apr";
					break;
				case 5:
					return "Mei";
					break;
				case 6:
					return "Jun";
					break;
				case 7:
					return "Jul";
					break;
				case 8:
					return "Agus";
					break;
				case 9:
					return "Sep";
					break;
				case 10:
					return "Okt";
					break;
				case 11:
					return "Nov";
					break;
				case 12:
					return "Des";
					break;
			}	
		}else{
			switch ($bln) {
				case 1:
					return "Januari";
					break;
				case 2:
					return "Februari";
					break;
				case 3:
					return "Maret";
					break;
				case 4:
					return "April";
					break;
				case 5:
					return "Mei";
					break;
				case 6:
					return "Juni";
					break;
				case 7:
					return "Juli";
					break;
				case 8:
					return "Agustus";
					break;
				case 9:
					return "September";
					break;
				case 10:
					return "Oktober";
					break;
				case 11:
					return "November";
					break;
				case 12:
					return "Desember";
					break;
			}	
		}
	}
	static public function getArrayBulan(){
		return array(
				1=>"Jan",
				2=>"Feb",
				3=>"Mar",
				4=>"Apr",
				5=>"Mei",
				6=>'Jun',
				7=>'Jul',
				8=>'Agus',
				9=>'Sept',
				10=>'Okt',
				11=>'Nov',
				12=>'Des'
				);
	}

	static public function getArrayTahun($from,$to){
		$result=[];
		for ($i=$from; $i <=$to ; $i++) { 
			$result[$i]=$i;
		}
		return $result;
	}

	static public function convertMonthSeq($s){
		if(strlen($s)==1){
			return '0'.$s;
		}else{
			return $s;
		}
	}
}