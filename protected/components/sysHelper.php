<?php
class sysHelper{
	public static function VerifiedControl($isVerified,$pelanggan_id)
	{
		$changed=1;
		if ($isVerified==1){
			$changed=0;
		}
		$modeldrop[]=array('name'=>'Approved','id'=>1);
		$modeldrop[]=array('name'=>'UnApproved','id'=>0);
		$list = CHtml::listData($modeldrop, 'id', 'name');
		echo CHtml::dropDownList("name",$isVerified, $list, array('class'=>'input-medium', 'ng-model'=>'isverfied'.$pelanggan_id.'', 'ng-init'=>'isverfied'.$pelanggan_id.'='.$isVerified, 'ng-change'=>'changeData('.$pelanggan_id.','.$changed.')'));

	}
	public static function generateLinkDoc($user_id){
		$u=Pelanggan::model()->findByPk($user_id);
		if (!isset($u)){
			return false;
		}
		$siup=CHtml::link('SIUP',Yii::app()->createUrl($u->siupAtc));
		$npwp=CHtml::link('NPWP',Yii::app()->createUrl($u->npwpAtc));
		$ktp=CHtml::link('KTP',Yii::app()->createUrl($u->ktpAtc));
		return $siup.' '.$npwp.' '.$ktp;
	}
	public static function getUsername($id){
		$u=User::model()->findByPk($id);
		if (!isset($u->username)){
			return false;
		}
		return $u->username;
	}

	public static function rp($harga){
		return "Rp ".number_format($harga,0,",",".");
	}
	public static function getuser($user_id){
		if ($user_id==''){
			return null;
		}else{
			$us=User::model()->findByPk($user_id);
			if (isset($us->nama)){
				return $us->nama;
			}else{
				return "Tidak dikenal";
			}
		}
	}
	

	static public function reverseDateFormat($date,$delimiter='-'){
    	$d=explode('-', $date);
    	return $d[2].$delimiter.$d[1].$delimiter.$d[0];
    }

    

 	static public function Loadthemes(){
		$filename="themes.txt";
		$file = fopen($filename, "r");
		$f=fread($file,filesize($filename));
		fclose($file);
		echo $f;
	}

	static public function isManifesDiambil($a){
		if ($a==1){
			return "Manifes telah diambil";
		}else{
			return "Manifes belum diambil";
		}
	}

	static public function isSP2Otorisasi($a){
		if ($a==1){
			return "SP2 telah diotorisasi";
		}else{
			return "SP2 belum diotorisasi";
		}
	}

	static public function getJalur($a){
		if ($a==1){
			return "Jalur Hijau";
		}elseif($a==2){
			return "Jalur Hijau dengan notul";
		}elseif($a==3){
			return "Jalur Merah";
		}else{
			return "Jalur belum dipilih";
		}
	}

	static public function getPemasukan($field,$bulan,$tahun){
		$c=new CDbCriteria;
		$c->select=$field;
		$c->condition='bulan='.$bulan." AND tahun=".$tahun;
		$count=Pemasukanv::model()->count($c);
		if($count<=0){
			return 0;
		}else{
			$p=Pemasukanv::model()->find($c);
			return $p->$field;
		}
	}

	static public function getTotalPemasukan($bulan,$tahun){
		$c=new CDbCriteria;
		$c->condition='bulan='.$bulan." AND tahun=".$tahun;
		$count=Pemasukanv::model()->count($c);
		if($count<=0){
			return 0;
		}else{
			$p=Pemasukanv::model()->find($c);
			$total=$p->jasaImpor + $p->jasaPPJK + $p->biayaDok + $p->biayaLain + $p->ppnTotal1 + $p->pdri + $p->beaMasuk + $p->biayaBeacukai;
			return $total;
		}
	}
	static public function getPengeluaran($field,$bulan,$tahun){
		$c=new CDbCriteria;
		$c->select=$field;
		$c->condition='bulan='.$bulan." AND tahun=".$tahun;
		$count=Pengeluaranv::model()->count($c);
		if($count<=0){
			return 0;
		}else{
			$p=Pengeluaranv::model()->find($c);
			return $p->$field;
		}
	}

	static public function getTotalPengeluaran($bulan,$tahun){
		$c=new CDbCriteria;
		$c->condition='bulan='.$bulan." AND tahun=".$tahun;
		$count=Pengeluaranv::model()->count($c);
		if($count<=0){
			return 0;
		}else{
			$p=Pengeluaranv::model()->find($c);
			$total=$p->ppnTotal1 + $p->pdri + $p->beaMasuk + $p->biayaBeacukai;
			return $total;
		}
	}

	static public function getTotalProfit($bulan,$tahun){
		$c=new CDbCriteria;
		$c->condition='bulan='.$bulan." AND tahun=".$tahun;
		$count=Pengeluaranv::model()->count($c);
		if($count<=0){
			$totalKeluar=0;
		}else{
			$p=Pengeluaranv::model()->find($c);
			$totalKeluar=$p->ppnTotal1 + $p->pdri + $p->beaMasuk + $p->biayaBeacukai;
		}
		$count=Pemasukanv::model()->count($c);
		if($count<=0){
			$totalMasuk=0;
		}else{
			$p=Pemasukanv::model()->find($c);
			$totalMasuk=$p->jasaImpor + $p->jasaPPJK + $p->biayaDok + $p->biayaLain + $p->ppnTotal1 + $p->pdri + $p->beaMasuk + $p->biayaBeacukai;
		}
		return $totalMasuk-$totalKeluar;
	}

	static public function getStringNotif($s){
		switch ($s) {
			case 'init':
				$msg="telah membuat job order baru";
				break;
			case 'cdpi0':
				$msg="telah mengecek dan meminta anda untuk merevisi dokumen";
				break;
			case 'cdpi1':
				$msg="telah mengecek dan menyetujui dokumen yang anda buat";
				break;
			case 'cdpi3':
				$msg="telah merevisi dokumen pesanannya";
				break;
			case 'tPLY':
				$msg="telah memasukkan tanggal pelayaran";
				break;
			case 'man':
				$msg="telah mengambil manifes pesanan, silakan melakukan pembayaran PIB";
				break;
			case 'bukba':
				$msg="telah melakukan pembayaran PIB, silakan melakukan pengisian PIB";
				break;
			case 'pibis':
				$msg="telah melakukan pengisian PIB, silakan melakukan otorisasi PIB";
				break;
			case 'pibot':
				$msg="telah melakukan otorisasi PIB, silakan memasukkan jalur pesanan";
				break;
			case 'jh1':
				$msg="telah memasukkan pesanan anda pada jalur Hijau";
				break;
			case 'jhp':
				$msg="telah memasukkan pesanan anda pada jalur Hijau (dengan notul), Silakan membayar notul";
				break;
			case 'jhn33':
				$msg="telah merevisi dokumen pesanannya, silakan cek kembali";
				break;
			case 'jhn2':
				$msg="telah mengupload dokumen pesanannya, silakan cek";
				break;
			case 'jhn31':
				$msg="telah mengecek dan memvalidasi dokumen anda";
				break;
			case 'jhn32':
				$msg="telah mengecek dan menolak dokumen anda, silakan revisi dokumen anda";
				break;
			case 'jm':
				$msg="telah memasukkan pesanan anda pada jalur Merah";
				break;
			case 'jm3':
				$msg="telah memasukkan notul pada pesanan anda, silakan membayarnya";
				break;
			case 'jm5':
				$msg="telah mengupload dokumen pesanannya, silakan cek";
				break;
			case 'jm61':
				$msg="telah mengecek dan memvalidasi dokumen anda";
				break;
			case 'jm62':
				$msg="telah mengecek dan menolak dokumen anda, silakan revisi dokumen anda";
				break;
			case 'jm63':
				$msg="telah merevisi dokumen pesanannya, silakan cek kembali";
				break;
			case 'sp2':
				$msg="telah mengotorisasi SP2, silakan input biaya PPJK";
				break;
			case 'inv':
				$msg="telah mengirimkan invoice, silakan melakukan pembayaran";
				break;
			case 'bjtb':
				$msg="telah melakukan pembayaran biaya PPJK";
				break;
			case 'ljoc':
				$msg="telah melakukan job order complete, silakan otorisasi";
				break;
			case 'ljoc1':
				$msg="telah mengotorisasi job order complete";
				break;
			default:
				$msg="";
				break;
		}
		if(Yii::app()->user->getRoleCode()=='PLY'){
			switch ($s) {
				case 'cdpi1':
					$msg="telah menyetujui pesanan dan membutuhkan pelayaran untuk memasukkan tanggal pelayaran";
					break;
				default:
					$msg="";
					break;
			}
		}
		return $msg;
	}
	
}