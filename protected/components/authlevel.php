<?php
class authlevel{
      const AdminDJK = 1;
      const EksekutifDJK = 2;
      const PengawasDJK = 3;
      const OfficerDJK = 4;
      const LSK=5;
      const TT=6;
      // For CGridView, CListView Purposes
      public static function getLabel( $level ){
        if($level == self::DIR)
             return 'Direktur';
           if($level == self::MA)
             return 'Manager Administrasi';
          if($level == self::MK)
             return 'Manager Keuangan';
          if($level == self::MO)
             return 'Manager Operasional';
           if($level == self::OP)
             return 'Operator';
           if($level == self::PEL)
             return 'Pelanggan';
           if($level == self::PLY)
             return 'Pelayaran';
          return false;
      }
      // for dropdown lists purposes
      public static function getLevelList(){
          return array(
                 self::DIR=>'Direktur',
                 self::MK=>'Manager Keuangan',
                 self::MO=>'Manager Operasional',
                 self::OP=>'Operator',
                 self::PEL=>'Pelanggan',
                 self::PLY=>'Pelayaran',
          );
    }
}