<?php
$this->breadcrumbs=array(
    'Laporan Job Order',
);
?>
<?php if (!$printMode) {?>
    <form class="form-inline" method="get" action="<?php echo Yii::app()->createUrl('laporan/joborder')?>">
      <?php
        echo CHtml::dropDownList("month1",(int)$month1, dateHelper::getArrayBulan(),array('class'=>'input-small'));
        echo CHtml::dropDownList("year1",$year1, dateHelper::getArrayTahun(date('Y')-2,date('Y')),array('class'=>'input-small'));
        echo " s/d ";
        echo CHtml::dropDownList("month2",(int)$month2, dateHelper::getArrayBulan(),array('class'=>'input-small'));
        echo CHtml::dropDownList("year2",$year2, dateHelper::getArrayTahun(date('Y')-2,date('Y')),array('class'=>'input-small'));

      ?>
      <button type="submit" class="btn green">Submit</button>
    </form>
<?php } ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array( 
    'id'=>'laporan-jo-grid', 
    'dataProvider'=>$model->search(), 
    // 'filter'=>$model, 
    'columns'=>array( 
        array('name'=>'Pesanan_ID', 'sortable'=>false),
        array('name'=>'Nama_Pelanggan', 'sortable'=>false),
        array('name'=>'Nama_Barang', 'sortable'=>false),
        array('name'=>'Tanggal_Mulai', 'value'=>'dateHelper::tgl_indo($data->Tanggal_Mulai)', 'sortable'=>false),
        array('name'=>'Tanggal_Selesai', 'value'=>'dateHelper::tgl_indo($data->Tanggal_Selesai)', 'sortable'=>false),
        array('name'=>'Status', 'sortable'=>false),
        array('name'=>'No_Pengajuan_PIB', 'sortable'=>false),
        array('name'=>'Total_Biaya', 'value'=>'$data->Total_Biaya==null ? "":sysHelper::rp($data->Total_Biaya)', 'sortable'=>false),
        
    ), 
)); ?> 
<?php if (!$printMode) {?>
<center>
    <?php echo CHtml::link("Print",Yii::app()->createUrl('laporan/joborder',array('print'=>true,'month1'=>$month1, 'month2'=>$month2, 'year1'=>$year1, 'year2'=>$year2 )),array('class'=>'btn green','target'=>'_blank'))?>
</center>
<?php } ?>