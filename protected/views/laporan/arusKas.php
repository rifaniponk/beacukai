<?php
$this->breadcrumbs=array(
    'Arus Kas',
);
$bulan=dateHelper::getArrayBulan();
$itemMasuk=array(
	"jasaImpor"=>"Jasa Impor",
	"jasaPPJK"=>"Jasa PPJK",
	"biayaDok"=>"Biaya Dokumen",
	"biayaLain"=>"Biaya Lain",
	);
$itemKeluar=array(
	"ppnTotal1"=>"PPN",
	"pdri"=>"Pajak dalam rangka impor",
	"beaMasuk"=>"Bea Masuk",
	"biayaBeacukai"=>"Biaya Beacukai",
	);
?>
<center><h1>Arus Kas</h1></center>
<table class="table table-bordered">
	<thead>
		<tr>
			<th></th>
			<?php foreach ($bulan as $ib => $nb) { ?>
				<th><?php echo $nb?></th>
			<?php }?>
		</tr>
	</thead>
	<tbody>

		<!-- PEMASUKAN -->
		<tr>
			<td colspan="13" bgcolor="#F2F2F2"><b>Pemasukan</b></td>
		</tr>
		<?php foreach ($itemMasuk as $key => $value) { ?>
			<tr>
				<td><?php echo $value?></td>
				<?php foreach ($bulan as $ib => $nb) { ?>
					<td><?php echo sysHelper::getPemasukan($key,$ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getPemasukan($key,$ib,Date('Y'))) : "-" ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		<?php foreach ($itemKeluar as $key => $value) { ?>
			<tr>
				<td><?php echo $value?></td>
				<?php foreach ($bulan as $ib => $nb) { ?>
					<td><?php echo sysHelper::getPengeluaran($key,$ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getPengeluaran($key,$ib,Date('Y'))) : "-" ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		<tr bgcolor="">
			<td><b>Total</b></td>
			<?php foreach ($bulan as $ib => $nb) { ?>
				<td><b><?php echo sysHelper::getTotalPemasukan($ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getTotalPemasukan($ib,Date('Y'))) : "-" ?></b></td>
			<?php }?>
		</tr>


		<!-- PENGELUARAN -->
		<tr>
			<td colspan="13" bgcolor="#F2F2F2"><b>Pengeluaran</b></td>
		</tr>
		<?php foreach ($itemKeluar as $key => $value) { ?>
			<tr>
				<td><?php echo $value?></td>
				<?php foreach ($bulan as $ib => $nb) { ?>
					<td><?php echo sysHelper::getPengeluaran($key,$ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getPengeluaran($key,$ib,Date('Y'))) : "-" ?></td>
				<?php }?>
			</tr>
		<?php } ?>
		<tr bgcolor="">
			<td><b>Total</b></td>
			<?php foreach ($bulan as $ib => $nb) { ?>
				<td><b><?php echo sysHelper::getTotalPengeluaran($ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getTotalPengeluaran($ib,Date('Y'))) : "-" ?></b></td>
			<?php }?>
		</tr>


		<!-- PROFIT -->
		<tr>
			<td colspan="13" bgcolor="#F2F2F2"><b>Profit (pemasukan - pengeluaran)</b></td>
		</tr>
		<tr bgcolor="">
			<td><b>TOTAL PROFIT</b></td>
			<?php foreach ($bulan as $ib => $nb) { ?>
				<td><b><?php echo sysHelper::getTotalProfit($ib,Date('Y'))!=0 ? sysHelper::rp(sysHelper::getTotalProfit($ib,Date('Y'))) : "-" ?></b></td>
			<?php }?>
		</tr>
	</tbody>
</table>
<center>
	<?php echo CHtml::link("Print",Yii::app()->createUrl('laporan/printArusKas'),array('class'=>'btn green','target'=>'_blank'))?>
</center>