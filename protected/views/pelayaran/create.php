<?php
$this->breadcrumbs=array(
	'Pelayarans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pelayaran','url'=>array('index')),
	array('label'=>'Manage Pelayaran','url'=>array('admin')),
);
?>

<h1>Create Pelayaran</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>