<?php
$this->breadcrumbs=array(
	'Pelayarans'=>array('index'),
	$model->user_id=>array('view','id'=>$model->user_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pelayaran','url'=>array('index')),
	array('label'=>'Create Pelayaran','url'=>array('create')),
	array('label'=>'View Pelayaran','url'=>array('view','id'=>$model->user_id)),
	array('label'=>'Manage Pelayaran','url'=>array('admin')),
);
?>

<h1>Update Pelayaran <?php echo $model->user_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>