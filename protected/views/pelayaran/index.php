<?php
$this->breadcrumbs=array(
	'Pelayarans',
);

$this->menu=array(
	array('label'=>'Create Pelayaran','url'=>array('create')),
	array('label'=>'Manage Pelayaran','url'=>array('admin')),
);
?>

<h1>Pelayarans</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
