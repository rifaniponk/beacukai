<?php
$this->breadcrumbs=array(
	'Pelayarans'=>array('index'),
	$model->user_id,
);

$this->menu=array(
	array('label'=>'List Pelayaran','url'=>array('index')),
	array('label'=>'Create Pelayaran','url'=>array('create')),
	array('label'=>'Update Pelayaran','url'=>array('update','id'=>$model->user_id)),
	array('label'=>'Delete Pelayaran','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pelayaran','url'=>array('admin')),
);
?>

<h1>View Pelayaran #<?php echo $model->user_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'nama',
		'alamat',
		'telp',
		'email',
		'web',
		'ctime',
	),
)); ?>
