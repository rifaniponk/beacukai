<script type="text/javascript">
    $(document).ready(function(){
        // $("#Invoice_tgl").datepicker({
        //     dateFormat:"yy-mm-dd",
        //     changeMonth: true,
        //     // numberOfMonths: 2,
        //     changeYear:true,
        // })

        $("#Invoice_biayaBeacukai").keyup(function(){
        	var total1=Math.floor($("#Invoice_total1").val());
        	var ppn=Math.floor($("#Invoice_ppnTotal1").val());

        	var bea=Math.floor($("#Invoice_biayaBeacukai").val());
        	var bm=Math.floor($("#Invoice_beaMasuk").val());
        	var pdri=Math.floor($("#Invoice_pdri").val());

        	var total2=bea+bm+pdri;
        	var grandtotal=total2+total1+ppn;
        	$("#Invoice_total2").val(total2);
        	$("#Invoice_grandtotal").val(grandtotal);
        })

        $("#kirim-invoice").click(function(event){
        	event.preventDefault();
  			event.stopImmediatePropagation();
        	if ($("#Invoice_biayaBeacukai").val()==0 || $("#Invoice_biayaBeacukai").val()==''){
	        	if (!confirm('Biaya beacukai = NOL, Apakah anda yakin untuk melanjutkan?')){
	        		return false;
	        	}else{
	        		$("#barang-form").submit();
	        	}
        	}else{
	        		$("#barang-form").submit();
        	}
        })
    })
    </script>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'barang-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'no',array('value'=>$joborder->id.'/TSA/'.$joborder->tglMulai ,'class'=>'input-lg','maxlength'=>50,'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'tgl',array('class'=>'input-lg','value'=>Date('Y-m-d'),'readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'jasaImpor',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'jasaPPJK',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'biayaDok',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'biayaLain',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'total1',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'ppnTotal1',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'pdri',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'beaMasuk',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'biayaBeacukai',array('class'=>'input-lg' , 'prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'total2',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'grandtotal',array('class'=>'input-lg' , 'prepend'=>'Rp','readonly'=>'readonly')); ?>

	<div class="form-actions">
		<button class="btn btn-primary" type="submit" id="kirim-invoice">Kirim</button>
	</div>

<?php $this->endWidget(); ?>
