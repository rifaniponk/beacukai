<?php
$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	$model->no=>array('view','id'=>$model->no),
	'Update',
);

$this->menu=array(
	array('label'=>'List Invoice','url'=>array('index')),
	array('label'=>'Create Invoice','url'=>array('create')),
	array('label'=>'View Invoice','url'=>array('view','id'=>$model->no)),
	array('label'=>'Manage Invoice','url'=>array('admin')),
);
?>

<h1>Update Invoice <?php echo $model->no; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>