<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	$model->no,
);
?>

<h1>Invoice #<?php echo $model->no; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array('name'=>'no'),
		array('name'=>'tgl', 'value'=>dateHelper::tgl_indo($model->tgl)),
		array('name'=>'jasaImpor', 'value'=>sysHelper::rp($model->jasaImpor)),
		array('name'=>'jasaPPJK', 'value'=>sysHelper::rp($model->jasaPPJK)),
		array('name'=>'biayaDok', 'value'=>sysHelper::rp($model->biayaDok)),
		array('name'=>'biayaLain', 'value'=>sysHelper::rp($model->biayaLain)),
		array('name'=>'total1', 'value'=>sysHelper::rp($model->total1)),
		array('name'=>'ppnTotal1', 'value'=>sysHelper::rp($model->ppnTotal1)),
		array('name'=>'pdri', 'value'=>sysHelper::rp($model->pdri)),
		array('name'=>'beaMasuk', 'value'=>sysHelper::rp($model->beaMasuk)),
		array('name'=>'biayaBeacukai', 'value'=>sysHelper::rp($model->biayaBeacukai)),
		array('name'=>'total2', 'value'=>sysHelper::rp($model->total2)),
		array('name'=>'grandtotal', 'value'=>sysHelper::rp($model->grandtotal)),
		array('name'=>'ctime'),
	),
)); ?>

<center>
	<a href='<?php echo Yii::app()->createUrl("Invoice/print", array("no"=>$joborder->invoice_no)) ?>' class="btn purple" target="_blank">Cetak Invoice</a>
	<a href='<?php echo Yii::app()->createUrl("pembayaranInvoice/create", array("joborder_id"=>$joborder->id)) ?>' class="btn green">Bayar</a>
</center>
