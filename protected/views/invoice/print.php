<div align="center">
	<h1>INVOICE</h1>
</div>

<div class="row-fluid">
	<div class="span6">
		Kepada Yth:
		<table width="250px" border="1" cellpadding="5px">
			<tr>
				<td>
					<?php echo $joborder->pelangganUser->nama ?><br>
					<?php echo $joborder->pelangganUser->telp ?><br>
					<?php echo $joborder->pelangganUser->alamat ?><br>
				</td>
			</tr>
		</table>
	</div>
	<div class="span5 offset1">
		<br>
		<table width="100%" border="1" cellpadding="5px">
			<tr>
				<td width="100px">NO INVOICE</td>
				<td><b><?php echo $model->no ?></b></td>
			</tr>
			<tr>
				<td>TGL</td>
				<td><b><?php echo $model->tgl ?></b></td>
			</tr>
			<tr>
				<td>COMODITY</td>
				<td><?php echo $joborder->barang->nama ?></td>
			</tr>
			<tr>
				<td>TERM</td>
				<td>CASH</td>
			</tr>
		</table>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<br><br>
		<table width="100%" border="1" cellpadding="5px">
			<thead>
				<tr>
					<th colspan="3" bgcolor="#F0F0F0">BIAYA PPJK</th>
				</tr>
				<tr>
					<th width="10%">No.</th>
					<th width="70%">Uraian</th>
					<th width="20%">Jumlah (Rp)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td>Jasa Impor</td>
					<td align="right"><?php echo sysHelper::rp($model->jasaImpor) ?></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Jasa PPJK</td>
					<td align="right"><?php echo sysHelper::rp($model->jasaPPJK) ?></td>
				</tr>
				<tr>
					<td>3.</td>
					<td>Biaya Dokumen</td>
					<td align="right"><?php echo sysHelper::rp($model->biayaDok) ?></td>
				</tr>
				<tr>
					<td>4.</td>
					<td>Biaya Lain-lain</td>
					<td align="right"><?php echo sysHelper::rp($model->biayaLain) ?></td>
				</tr>
				<tr>
					<td colspan="2" align="center">Sub Total (Rp)</td>
					<td align="right"><b><?php echo sysHelper::rp($model->total1) ?></b></td>
				</tr>
				<tr>
					<td colspan="2" align="center">PPN 10%</td>
					<td align="right"><?php echo sysHelper::rp($model->ppnTotal1) ?></td>
				</tr>

			</tbody>
		</table>
		<br>
		<table width="100%" border="1" cellpadding="5px">
			<thead>
				<tr>
					<th colspan="3" bgcolor="#F0F0F0">BIAYA BEACUKAI</th>
				</tr>
				<tr>
					<th width="10%">No.</th>
					<th width="70%">Uraian</th>
					<th width="20%">Jumlah (Rp)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1. </td>
					<td>Pajak dalam Rangka Impor</td>
					<td align="right"><?php echo sysHelper::rp($model->pdri) ?></td>
				</tr>
				<tr>
					<td>2. </td>
					<td>Bea Masuk</td>
					<td align="right"><?php echo sysHelper::rp($model->beaMasuk) ?></td>
				</tr>
				<tr>
					<td>3. </td>
					<td>Biaya Beacukai</td>
					<td align="right"><?php echo sysHelper::rp($model->biayaBeacukai) ?></td>
				</tr>
				<tr>
					<td colspan="2" align="center">Sub Total (Rp)</td>
					<td align="right"><b><?php echo sysHelper::rp($model->total2) ?></b></td>
				</tr>

			</tbody>
		</table>
		<br>
		<table width="100%" border="1" cellpadding="5px">
			<thead>
				<tr>
					<th colspan="3" bgcolor="#F0F0F0">GRAND TOTAL</th>
				</tr>
				<tr>
					<th width="10%">No.</th>
					<th width="70%">Uraian</th>
					<th width="20%">Jumlah (Rp)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1.</td>
					<td>Sub Total Biaya PPJK (+ppn)</td>
					<td align="right"><?php echo sysHelper::rp($model->total1 + $model->ppnTotal1) ?></td>
				</tr>
				<tr>
					<td>2.</td>
					<td>Sub Total Biaya Beacukai</td>
					<td align="right"><?php echo sysHelper::rp($model->total2) ?></td>
				</tr>
				<tr>
					<td colspan="2" align="center">TOTAL TAGIHAN (Rp)</td>
					<td align="right"><b><?php echo sysHelper::rp($model->grandtotal) ?></b></td>
				</tr>

			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid">
	<div class="span5 offset7" align="center">
		<br><br><br><br>
		Banda Aceh, <?php echo dateHelper::tgl_indo($model->tgl) ?> <br><br><br><br><br>
		<b><?php echo $model->mk->nama?></b><br>
		(Manager Keuangan)
	</div>
</div>