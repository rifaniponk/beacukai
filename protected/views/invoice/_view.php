<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('no')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->no),array('view','id'=>$data->no)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl')); ?>:</b>
	<?php echo CHtml::encode($data->tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jasaImpor')); ?>:</b>
	<?php echo CHtml::encode($data->jasaImpor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jasaPPJK')); ?>:</b>
	<?php echo CHtml::encode($data->jasaPPJK); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayaDok')); ?>:</b>
	<?php echo CHtml::encode($data->biayaDok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayaLain')); ?>:</b>
	<?php echo CHtml::encode($data->biayaLain); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total1')); ?>:</b>
	<?php echo CHtml::encode($data->total1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pdri')); ?>:</b>
	<?php echo CHtml::encode($data->pdri); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beaMasuk')); ?>:</b>
	<?php echo CHtml::encode($data->beaMasuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayaBeacukai')); ?>:</b>
	<?php echo CHtml::encode($data->biayaBeacukai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total2')); ?>:</b>
	<?php echo CHtml::encode($data->total2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grandtotal')); ?>:</b>
	<?php echo CHtml::encode($data->grandtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />

	*/ ?>

</div>