<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'no',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'tgl',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jasaImpor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jasaPPJK',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'biayaDok',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'biayaLain',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'total1',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pdri',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'beaMasuk',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'biayaBeacukai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'total2',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'grandtotal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ctime',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
