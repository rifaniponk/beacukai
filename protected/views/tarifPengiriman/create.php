<?php
$this->breadcrumbs=array(
	'Tarif Pengirimen'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TarifPengiriman','url'=>array('index')),
	array('label'=>'Manage TarifPengiriman','url'=>array('admin')),
);
?>

<h1>Create TarifPengiriman</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>