<?php
$this->breadcrumbs=array(
	'Tarif Pengirimen'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TarifPengiriman','url'=>array('index')),
	array('label'=>'Create TarifPengiriman','url'=>array('create')),
	array('label'=>'View TarifPengiriman','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TarifPengiriman','url'=>array('admin')),
);
?>

<h1>Update TarifPengiriman <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>