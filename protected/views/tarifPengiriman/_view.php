<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pelabuhan_muat_id')); ?>:</b>
	<?php echo CHtml::encode($data->pelabuhan_muat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pelabuhanBongkar_id')); ?>:</b>
	<?php echo CHtml::encode($data->pelabuhanBongkar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biaya')); ?>:</b>
	<?php echo CHtml::encode($data->biaya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ukContainer')); ?>:</b>
	<?php echo CHtml::encode($data->ukContainer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipeContainer')); ?>:</b>
	<?php echo CHtml::encode($data->tipeContainer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />


</div>