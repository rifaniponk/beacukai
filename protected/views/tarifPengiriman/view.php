<?php
$this->breadcrumbs=array(
	'Tarif Pengirimen'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TarifPengiriman','url'=>array('index')),
	array('label'=>'Create TarifPengiriman','url'=>array('create')),
	array('label'=>'Update TarifPengiriman','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete TarifPengiriman','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TarifPengiriman','url'=>array('admin')),
);
?>

<h1>View TarifPengiriman #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pelabuhan_muat_id',
		'pelabuhanBongkar_id',
		'biaya',
		'ukContainer',
		'tipeContainer',
		'ctime',
	),
)); ?>
