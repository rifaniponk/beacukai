<?php
$this->breadcrumbs=array(
	'Karyawan',
);
?>

<h1>Karyawan</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'karyawan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'user_id',
		'nama',
		'alamat',
		'telp',
		'email',
		// 'ctime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
