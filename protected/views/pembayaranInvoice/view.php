<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Pembayaran Biaya Kepabeanan',
);

foreach ($invoice->joborders as $in) {
	$joborder=$in;
}

$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>".fancy",
    'config'=>array(
    	'transitionIn'	=>	'elastic',
		'transitionOut'	=>	'elastic',
		'speedIn'		=>	600, 
		'speedOut'		=>	200, 
		'overlayShow'	=>	true
    	),
    )
); 
?>

<h1></h1>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		// 'id',
		array('name'=>'jml', 'value'=>sysHelper::rp($model->jml)),
		array('name'=>'tglBayar', 'value'=>dateHelper::tgl_indo($model->tglBayar)),
		'ket',
		array('name'=>'atc','value'=>CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),

		// 'ctime',
	),
)); ?>
<center>
	<a href="<?php echo Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$joborder->id,"status"=>"bjtb1"))?>" class="btn green"><i class="icon-ok"></i> Terima Pembayaran</a>
</center>
