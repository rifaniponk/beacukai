<?php
$this->breadcrumbs=array(
	'Pembayaran Invoices',
);

$this->menu=array(
	array('label'=>'Create PembayaranInvoice','url'=>array('create')),
	array('label'=>'Manage PembayaranInvoice','url'=>array('admin')),
);
?>

<h1>Pembayaran Invoices</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
