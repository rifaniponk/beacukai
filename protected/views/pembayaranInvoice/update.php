<?php
$this->breadcrumbs=array(
	'Pembayaran Invoices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PembayaranInvoice','url'=>array('index')),
	array('label'=>'Create PembayaranInvoice','url'=>array('create')),
	array('label'=>'View PembayaranInvoice','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PembayaranInvoice','url'=>array('admin')),
);
?>

<h1>Update PembayaranInvoice <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>