<script type="text/javascript">
    $(document).ready(function(){
        $("#PembayaranInvoice_tglBayar").datepicker({
            dateFormat:"yy-mm-dd",
            changeMonth: true,
            // numberOfMonths: 2,
            changeYear:true,
        })
    })
    </script>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'barang-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'jml',array('class'=>'input-lg','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'tglBayar',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'ket',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->fileFieldRow($model,'atc',array('class'=>'span5','maxlength'=>245)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Submit' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
