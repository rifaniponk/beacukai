<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml')); ?>:</b>
	<?php echo CHtml::encode($data->jml); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglBayar')); ?>:</b>
	<?php echo CHtml::encode($data->tglBayar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ket')); ?>:</b>
	<?php echo CHtml::encode($data->ket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('atc')); ?>:</b>
	<?php echo CHtml::encode($data->atc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />


</div>