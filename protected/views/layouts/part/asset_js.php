<!-- BEGIN JAVASCRIPTS -->
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/breakpoints/breakpoints.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/js/jquery.blockui.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/js/jquery.cookie.js"></script>
  
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/uniform/jquery.uniform.min.js"></script> 
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/js/jquery.blockui.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/js/app.js"></script>
  <script src="<?php echo Yii::app()->request->baseUrl?>/asset/js/own_js.js" type="text/javascript"></script>
  <script>
    jQuery(document).ready(function() {   
      App.init();
      //menerapkan style yg telah dipilih user
      $('#style_color').attr("href", base_url+"asset/metronic/assets/css/style_<?php echo rifan::Loadthemes()?>.css");
    });
  </script>
  <!-- END JAVASCRIPTS -->