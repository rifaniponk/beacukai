<?php 
$url = Yii::app()->request->requestUri;
$tokens = explode('/', $url);
$path = $tokens[sizeof($tokens)-1];
$path2 = $tokens[sizeof($tokens)-2];
$path3 = $tokens[sizeof($tokens)-2];
$nava='';$nava_='';$nava1='';$nava2='';$nava3='';$nava4='';
$navb='';$navb_='';$navb1='';$navb2='';$navb3='';$navb4='';
$navc='';$navc_='';$navc1='';$navc2='';$navc3='';$navc4='';$navc5='';$navc6='';$navc7='';$navc8='';$navc9='';
$navd='';$navd_='';$navd1='';$navd2='';$navd3='';$navd4='';$navd5='';
$nave='';$nave_='';$nave1='';$nave2='';$nave3='';$nave4='';$nave5='';
$navf='';$navf_='';$navf1='';$navf2='';$navf3='';$navf4='';
$navg='';$navg_='';$navg1='';$navg2='';$navg3='';$navg4='';$navg5='';
$navh='';$navh_='';$navh1='';$navh2='';$navh3='';$navh4='';
$navi='';$navi_='';$navi1='';$navi2='';$navi3='';$navi4='';
// echo $path;die();
if ($path=='dashboard'){
   $nava="active";
   $nava_='<span class="selected"></span>';
}elseif($path=='users' || $path2=='users' || $path3=='users'){
   $nave="active";
   $nave_='<span class="selected"></span>';
   $nave4="class='active'";
}
         // echo $path;die();
?>

<ul>
   <li>
      <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
      <div class="sidebar-toggler hidden-phone"></div>
      <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
   </li>
   <li class="start <?php echo $nava?>">
      <a href="<?php echo Yii::app()->createUrl('joborder/admin')?>">
      <i class="icon-road"></i> 
      <span class="title">On Going Job Order</span>
      <?php echo $nava_ ?>
      <span class="selected"></span>
      </a>
   </li>
   <li class="<?php echo $navb?>">
      <a href="<?php echo Yii::app()->createUrl('joborder/history')?>">
      <i class="icon-road"></i> 
      <span class="title">Job Order Completed</span>
      <?php echo $navb_ ?>
      <span class="selected"></span>
      </a>
   </li>
   <li class="<?php echo $navc?>">
      <a href="<?php echo Yii::app()->createUrl('laporan/ArusKas')?>">
      <i class="icon-arrow-down"></i> 
      <span class="title">Laporan Pemasukan & Pengeluaran</span>
      <?php echo $navc_ ?>
      <span class="selected"></span>
      </a>
   </li>
   <li class="<?php echo $navd?>">
      <a href="<?php echo Yii::app()->createUrl('laporan/Joborder')?>">
      <i class="icon-arrow-down"></i> 
      <span class="title">Laporan Job Order</span>
      <?php echo $navd_ ?>
      <span class="selected"></span>
      </a>
   </li>
<!--    <li class="<?php echo $navd?>">
      <a href="<?php echo Yii::app()->createUrl('laporan/pengeluaran')?>">
      <i class="icon-arrow-up"></i> 
      <span class="title">Laporan Pengeluaran</span>
      <?php echo $navd_ ?>
      <span class="selected"></span>
      </a>
   </li> -->
   <li class="<?php echo $nave?>">
      <a href="<?php echo Yii::app()->createUrl('barang/admin')?>">
      <i class="icon-th-large"></i> 
      <span class="title">Barang</span>
      <?php echo $nave_ ?>
      <span class="selected"></span>
      </a>
   </li>
   <li class="<?php echo $navf?>">
      <a href="<?php echo Yii::app()->createUrl('karyawan/admin')?>">
      <i class="icon-user"></i> 
      <span class="title">User</span>
      <?php echo $navf_ ?>
      <span class="selected"></span>
      </a>
   </li>

</ul>