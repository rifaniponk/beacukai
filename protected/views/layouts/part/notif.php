<?php
$c=new CDbCriteria;
$c->condition="(roleKe='".Yii::app()->user->getRoleCode()."' OR ke=".Yii::app()->user->id.") AND isRead=0";
$countNotif=Notif::model()->count($c);
$c->condition="roleKe='".Yii::app()->user->getRoleCode()."' OR ke=".Yii::app()->user->id;
$c->order='ctime DESC';
$c->limit='15';
$notif=Notif::model()->findAll($c);
?>
<script type="text/javascript">
$(document).ready(function(){
   $("#header_notification_bar").click(function(){
      $.ajax({
         url:base_url+"main/ReadAllNotif",
         success:function(data){
            $(".badge_notif").fadeOut(200);
         }
      })
   })
})
</script>
<li class="dropdown" id="header_notification_bar">
   <a href="#" class="dropdown-toggle bnotif" data-toggle="dropdown">
   <i class="icon-warning-sign"></i>
   <span class="badge badge_notif"><?php echo $countNotif==0 ? "" : $countNotif ?></span>
   </a>
   <ul class="dropdown-menu extended notification">
      <?php foreach ($notif as $r) {?>
         <li>
            <a href="<?php echo Yii::app()->createUrl('joborder/admin')?>" style="<?php echo $r->isRead==0 ? 'background:#FFFFED' : ''?>">
               <?php 
               $string = "<b>[".$r->tableDesc->desc." #".$r->row_id."]</b> ";
               $string .= $r->dariUser->nama." (".$r->roleDari.") ";
               $string .= sysHelper::getStringNotif($r->JOStatus);

               echo $string;
               ?>
               <br><span class="time"><?php echo dateHelper::tgl_indo($r->ctime,false,true,true)?></span>
            </a>
         </li>
      <?php }?>
      <!-- <li class="external">
         <a href="#">Lihat Semua <i class="m-icon-swapright"></i></a>
      </li>  -->
   </ul>
</li>