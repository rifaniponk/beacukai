<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>$id)); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal"></a>
    <h4><?php echo $title?></h4>
</div>
 
<div class="modal-body">
    <?php $this->renderPartial($content)?>
</div>
 
<!-- <div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'Save changes',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Close',
        'url'=>'#',
        'htmlOptions'=>array('data-dismiss'=>'modal'),
    )); ?>
</div> -->
 
<?php $this->endWidget(); ?>