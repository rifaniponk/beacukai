<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <script type="text/javascript">
         var base_url = "<?php echo $this->base_url()?>";
         var options={direction:"right"};
      </script>
      <?php Yii::app()->bootstrap->register(); ?>

        <link href="<?php $this->base_url()?>asset/metronic/assets/css/metro.css" rel="stylesheet" />
        <link href="<?php $this->base_url()?>asset/metronic/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php $this->base_url()?>asset/metronic/assets/css/style.css" rel="stylesheet" />
        <link href="<?php $this->base_url()?>asset/metronic/assets/css/style_responsive.css" rel="stylesheet" />
        <link href="<?php $this->base_url()?>asset/metronic/assets/css/style_default.css" rel="stylesheet" id="style_color" />
        <link rel="stylesheet" type="text/css" href="<?php $this->base_url()?>asset/metronic/assets/uniform/css/uniform.default.css" />

      <link rel="stylesheet" type="text/css" href="<?php echo $this->base_url()?>asset/css/own_style.css">
</head>

<body>
<?php $this->renderPartial("//layouts/part/loading") ?>
<div class="container" id="page">

   <div class="row-fluid">
      <?php echo $content; ?>
   </div>


</div><!-- page -->
<?php $this->renderPartial("//layouts/part/asset_js") ?>
</body>
</html>

