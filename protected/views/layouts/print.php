<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<?php //Yii::app()->bootstrap->register(); ?>
	<link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/bootstrap/css/bootstrap.css" rel="stylesheet" media="print, screen, projection" />

	<!-- blueprint CSS framework -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" /> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/grid.css" media="print, screen, projection" /> -->
	<link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style.css" rel="stylesheet" />
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print, screen, projection" /> -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<script type="text/javascript">
         var base_url = "<?php echo $this->base_url()?>";
         var options={direction:"right"};
         window.print();
      </script>
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" /> -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" /> -->
	<style type="text/css">
	<?php if ($this->printLandscape==1){
	 	echo "@media print{@page {size: landscape}}";
	}?>
		@page{ 
		    size: auto;   /* auto is the initial value */ 

		    /* this affects the margin in the printer settings */ 
		    margin: 15mm 20mm 15mm 20mm;  
		} 

		body  
		{ 
		    /* this affects the margin on the content before sending to printer */ 
		    margin: 0px;  
		    font-size: 16px;
		} 
	</style>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<div class="container">
 		<?php echo $content ?>
    </div>

</body>
</html>
