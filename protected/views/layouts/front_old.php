<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <script type="text/javascript">
         var base_url = "<?php echo $this->base_url()?>";
         var options={direction:"right"};
      </script>
      <?php Yii::app()->bootstrap->register(); ?>
      <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>
      <?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/custom/bootstrap/jquery-ui-1.10.0.custom.css'); ?> 
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/metro.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style_responsive.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style_<?php echo rifan::Loadthemes()?>.css" rel="stylesheet" id="style_color" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/uniform/css/uniform.default.css" />
        <?php $this->renderPartial("//layouts/part/asset_js") ?>
      <link rel="stylesheet" type="text/css" href="<?php echo $this->base_url()?>asset/css/own_style.css">
</head>

<!-- BEGIN BODY -->
<body class="fixed-top">
<?php $this->renderPartial("//layouts/part/loading") ?>

  <!-- BEGIN CONTAINER -->
  <div class="page-container row-fluid">
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12" style="background:white;padding:20px 20px 20px 20px">
            <?php echo $content ?>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->      
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <div class="footer">
    2013 &copy;  Aplikasi sederhana untuk tes PHP Programmer (by rifan).
    <div class="span pull-right">
      <span class="go-top"><i class="icon-angle-up"></i></span>
    </div>
  </div>
  <!-- END FOOTER -->

<!-- END BODY -->
</body>
</html>

