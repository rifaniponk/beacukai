<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title><?php echo $this->pageTitle ?></title>
<script type="text/javascript">
         var base_url = "<?php echo $this->base_url()?>";
         var options={direction:"right"};
      </script>
      <?php Yii::app()->bootstrap->register(); ?>
      <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>
      <?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/custom/bootstrap/jquery-ui-1.10.0.custom.css'); ?> 


<script type="text/javascript" src="<?php echo $this->base_url()?>asset/js/own_js.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->base_url()?>asset/css/own_style.css">
<!-- Le Styles -->

<link href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/css/skeleton.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/css/layout.css" rel="stylesheet">

<!-- Le Fonts -->
<link href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/css/fontgoogleapis.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/css/fonts/glyphicons/style.css" rel="stylesheet">

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<?php $this->renderPartial('//layouts/part/loading')?>
	<!-- Color Customizer, remove if not needed -->
	<!-- <div id="joiee-customizer"></div> -->
	<div id="main-wrapper" class="wide">

		<!-- Main Header -->
		<header id="main-header">

			<!-- Top Header -->
			<div id="header-top">
				<div class="container">
					<div class="sixteen columns">

						<!-- Logo Container -->
						<div id="logo-container">
							<div id="logo-center">
								<a href="#">
									<img src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/img/logo.png" alt="Joiee">
								</a>
							</div>
						</div>
						<!-- END Logo Container -->

						<div class="tagline">Aplikasi Impor</div>

						<!-- Main Navigation -->
						<nav id="main-nav">
							<ul>
								<li><a href="<?php echo Yii::app()->createUrl('main/index')?>">Home</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('main/tentang')?>">About us</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('main/kontak')?>">Contact us</a></li>
								<li><button class="btn btn-success" onclick="toglogin()">Login</button></li>
							</ul>
						</nav>
						<!-- END Main Navigation -->
					</div>
				</div>
			</div>
			<div align="right">
				<div class="alert alert-danger logingagal" style="display:none">
			      Username/Password <b>salah!</b>
			    </div>
			    <div class="alert alert-success loginberhasil" style="display:none">
			      Login <b>berhasil!</b>...
			    </div>
				<?php 
				$model=new LoginForm;
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
				    'id'=>'LoginForm',
				    'type'=>'inline',
				    'htmlOptions'=>array('class'=>'well loginbox'),
				)); ?>
				 
				<?php echo $form->textFieldRow($model, 'username', array('class'=>'span3 small')); ?>
				<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3 small')); ?>
				<button type="submit" class="btn btn-success">Log in</button>
				<a href="<?php echo Yii::app()->createUrl('main/register')?>" class="btn btn">Register</a>
				<?php $this->endWidget(); ?>
			</div>
		</header>
		<!-- END Main Header -->

		<!-- Main Content -->
		<section id="main-content">
			<div class="container">
				<div class="row-fluid">
					<div class="span12"><?php echo $content ?></div>
				</div>
			</div>
		</section>

		<!-- Footer -->
		<footer id="footer">

			<!-- Bottom Footer -->
			<div id="footer-bottom">
				<div class="container">

					<div class="eight columns">
						Copyright Aplikasi Impor 2012. All Rights Reserved.
					</div>
					<div class="eight columns">
						<ul class="footer-nav">
							<li><a href="#">Home</a></li>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</div>

				</div>
			</div>
			<!-- END Bottom Footer -->

		</footer>
		<!-- END Footer -->

	</div>

	<!-- Required Scripts -->
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/js/jquery.plugins.min.js"></script>

	<!-- Plugins -->
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/selectnav/selectnav.min.js"></script>
	<!--<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/jflickrfeed/jflickrfeed.min.js"></script>-->
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/fitvids/jquery.fitvids.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/isotope/jquery.isotope.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/sharrre/jquery.sharrre-1.3.4.min.js"></script>

	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/flexslider/jquery.flexslider-min.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/flexslider/flexslider.css">

	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/nivo/jquery.nivo.slider.pack.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/nivo/nivo-slider.css">


	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/revolutionslider/js/jquery.themepunch.revolution.min.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/revolutionslider/css/settings.css">

	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/caroufredsel/jquery.carouFredSel-6.1.0-packed.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/plugins/caroufredsel/jquery.touchSwipe.min.js"></script>

	<!-- Template Script -->
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/js/template.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl?>/asset/joiee/assets/js/customizer.js"></script>

</body>

</html>