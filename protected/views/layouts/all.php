<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app="myApp">
<head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <?php Yii::app()->bootstrap->register(); ?>
      <?php Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' ); ?>
      <?php Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/custom/bootstrap/jquery-ui-1.10.0.custom.css'); ?> 

      <script type="text/javascript">
         var base_url = "<?php echo $this->base_url()?>";
         var options={direction:"right"};
         jQuery.browser = {};
          (function () {
              jQuery.browser.msie = false;
              jQuery.browser.version = 0;
              if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                  jQuery.browser.msie = true;
                  jQuery.browser.version = RegExp.$1;
              }
          })();
      </script>

      <!-- angular -->
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angularlib/angular.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angularlib/angular-route.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angular/js/app.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angular/js/services.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angular/js/controllers.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angular/js/filters.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl?>/asset/angular/js/directives.js"></script>
      <!-- end of angular -->


        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/metro.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style_responsive.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/css/style_<?php echo rifan::Loadthemes()?>.css" rel="stylesheet" id="style_color" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/uniform/css/uniform.default.css" />
        <?php $this->renderPartial("//layouts/part/asset_js") ?>
      <link rel="stylesheet" type="text/css" href="<?php echo $this->base_url()?>asset/css/own_style.css">
</head>

<!-- BEGIN BODY -->
<body class="fixed-top">
<?php $this->renderPartial("//layouts/part/loading") ?>
  <!-- BEGIN HEADER -->
  <div class="header navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
      <div class="container-fluid">
        <!-- BEGIN LOGO -->
        <a class="brand" href="<?php echo Yii::app()->request->baseUrl?>">
        Aplikasi Impor
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
        <img src="<?php echo Yii::app()->request->baseUrl?>/asset/metronic/assets/img/menu-toggler.png" alt="" />
        </a>          
        <!-- END RESPONSIVE MENU TOGGLER --> 
        <!-- BEGIN TOP NAVIGATION MENU -->          
        <ul class="nav pull-right">
          <li class="dropdown">
            <a href="<?php echo Yii::app()->createUrl('main/InfoStatus')?>">INFO STATUS</a>
          </li>
          <?php $this->renderPartial('//layouts/part/notif')?>
          <li class="dropdown user">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="username"><?php echo Yii::app()->user->name ?></span>
            <i class="icon-angle-down"></i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo Yii::app()->createUrl('main/logout') ?>"><i class="icon-key"></i> Log Out</a></li>
            </ul>
          </li>
          <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->  
      </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
  </div>
  <!-- END HEADER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar nav-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->         
      <?php 
      if (Yii::app()->user->isDIR()){
        $this->renderPartial('//layouts/part/navDIR');
      }elseif (Yii::app()->user->isMA()){
        $this->renderPartial('//layouts/part/navMA');
      }elseif (Yii::app()->user->isMK()){
        $this->renderPartial('//layouts/part/navMK');
      }elseif (Yii::app()->user->isMO()){
        $this->renderPartial('//layouts/part/navMO');
      }elseif (Yii::app()->user->isOP()){
        $this->renderPartial('//layouts/part/navOP');
      }elseif (Yii::app()->user->isPEL()){
        $this->renderPartial('//layouts/part/navPEL');
      }elseif (Yii::app()->user->isPLY()){
        $this->renderPartial('//layouts/part/navPLY');
      }
      ?>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">

      <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <div id="portlet-config" class="modal hide">
        <div class="modal-header">
          <button data-dismiss="modal" class="close" type="button"></button>
          <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
          <p>Here will be a configuration form</p>
        </div>
      </div>
      <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
      <!-- BEGIN PAGE CONTAINER-->
      <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
          <div class="span12">
            <div class="color-panel hidden-phone">
              <div class="color-mode-icons icon-color"></div>
              <div class="color-mode-icons icon-color-close"></div>
              <div class="color-mode">
                <p>THEME COLOR</p>
                <ul class="inline">
                  <li class="color-black current color-default" data-style="default"></li>
                  <li class="color-blue" data-style="blue"></li>
                  <li class="color-brown" data-style="brown"></li>
                  <li class="color-purple" data-style="purple"></li>
                  <li class="color-white color-light" data-style="light"></li>
                </ul>
                <label class="hidden-phone">
                <input type="checkbox" class="header" checked value="" />
                <span class="color-mode-label">Fixed Header</span>
                </label>              
              </div>
            </div>
            <!-- END BEGIN STYLE CUSTOMIZER -->    
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->     
            <h3 class="page-title">
              <?php echo $this->pageTitle?>
            </h3>
            <div class="row-fluid">
              <div class="span9">
                <?php if(isset($this->breadcrumbs)):?>
                    <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                        'links'=>$this->breadcrumbs,
                        // 'homeLink'=>array($this->base_url().'lsk',
                    )); ?>
                  <?php endif?>
              </div>
              <script type="text/javascript">
              function reloadcurrenttime(){
                $(document).ready(function(){
                  $.get(base_url+"main/reloadcurrenttime",function(data){
                    $("#bcurrenttime").html(data);
                  })
                })
              }
              setInterval(reloadcurrenttime,30000);
              </script>
              <div class="span3" align="right">
                <p style="margin-bottom:-4px">waktu saat ini</p>
                <b id="bcurrenttime"><?php echo dateHelper::tgl_indo(date('Y-m-d'),true).' / '.date('H:i');  ?></b><hr>
              </div>
            </div>
            <!-- END PAGE TITLE & BREADCRUMB-->
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

      <?php echo $content ?>
        <!-- END PAGE CONTENT-->
      </div>
      <!-- END PAGE CONTAINER-->      
    </div>
    <!-- END PAGE -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <div class="footer">
    2013 &copy;  Aplikasi Import Beacukai
    <div class="span pull-right">
      <span class="go-top"><i class="icon-angle-up"></i></span>
    </div>
  </div>
  <!-- END FOOTER -->

<!-- END BODY -->
</body>
</html>

