<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Dokumen'=>Yii::app()->createUrl('Dokumen/admin',array('joborder_id'=>$joborder_id)),
	'Update Dokumen',
);

?>

<h1>Update Dokumen</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>