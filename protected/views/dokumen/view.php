<?php
$this->breadcrumbs=array(
	'Dokumens'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Dokumen','url'=>array('index')),
	array('label'=>'Create Dokumen','url'=>array('create')),
	array('label'=>'Update Dokumen','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Dokumen','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Dokumen','url'=>array('admin')),
);
?>

<h1>View Dokumen #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jenis',
		'judul',
		'atc',
		'JobOrder_id',
		'ctime',
	),
)); ?>
