<?php
$this->breadcrumbs=array(
	'Dokumens',
);

$this->menu=array(
	array('label'=>'Create Dokumen','url'=>array('create')),
	array('label'=>'Manage Dokumen','url'=>array('admin')),
);
?>

<h1>Dokumens</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
