<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Dokumen'=>Yii::app()->createUrl('Dokumen/admin',array('joborder_id'=>$joborder_id)),
	'Upload Dokumen',
);
?>

<h1>Upload Dokumen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>