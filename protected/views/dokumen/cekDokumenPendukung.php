<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder'),
	'Cek Dokumen Pendukung',
);


?>

<?php
if ($joborder->status=="jhn2" || $joborder->status=="jhn33"){
	$status_accept="jhn31";
	$status_reject="jhn32";
} elseif($joborder->status=="jm5" || $joborder->status=="jm63"){
	$status_accept="jm61";
	$status_reject="jm62";
}
echo CHtml::link("<i class='icon-thumbs-up'></i> Dokumen pendukung telah sesuai", Yii::app()->createUrl('joborder/updateStatus?joborder_id='.$joborder->id.'&status='.$status_accept), array('class'=>'btn btn-lg green'));
echo CHtml::link("<i class='icon-thumbs-down'></i> Dokumen pendukung perlu direvisi", Yii::app()->createUrl('joborder/updateStatus?joborder_id='.$joborder->id.'&status='.$status_reject), array('class'=>'btn btn-lg red')); 
?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'dokumen-grid',
	'dataProvider'=>$model->search(),
	// 'filter'=>$model,
	'columns'=>array(
		array('name'=>'id', 'sortable'=>false),
		array('name'=>'jenis', 'sortable'=>false),
		array('name'=>'judul', 'sortable'=>false),
		array('name'=>'atc', 'value'=>'CHtml::link($data->atc,Yii::app()->createUrl($data->atc))','type'=>'raw', 'sortable'=>false),
		// 'JobOrder_id',
		// 'ctime',
	),
)); ?>
