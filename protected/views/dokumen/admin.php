<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Upload Dokumen Pendukung',
);

?>

<h1>Dokumen Job Order #<?php echo $joborder_id?>
	<small>
		<?php echo CHtml::link('[UPLOAD BARU]',Yii::app()->createUrl('dokumen/create?joborder_id='.$joborder_id)) ?>
		<?php echo CHtml::link('[SELESAI]',Yii::app()->createUrl('dokumen/uploadDone?joborder_id='.$joborder_id)) ?>
	</small>
</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'dokumen-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'jenis',
		'judul',
		array('name'=>'atc','value'=>'CHtml::link($data->atc,Yii::app()->createUrl($data->atc))','type'=>'raw'),
		// 'JobOrder_id',
		'ctime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
)); ?>
