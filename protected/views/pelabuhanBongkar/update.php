<?php
$this->breadcrumbs=array(
	'Pelabuhan Bongkars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PelabuhanBongkar','url'=>array('index')),
	array('label'=>'Create PelabuhanBongkar','url'=>array('create')),
	array('label'=>'View PelabuhanBongkar','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PelabuhanBongkar','url'=>array('admin')),
);
?>

<h1>Update PelabuhanBongkar <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>