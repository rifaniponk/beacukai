<?php
$this->breadcrumbs=array(
	'Pelabuhan Bongkars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PelabuhanBongkar','url'=>array('index')),
	array('label'=>'Create PelabuhanBongkar','url'=>array('create')),
	array('label'=>'Update PelabuhanBongkar','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PelabuhanBongkar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PelabuhanBongkar','url'=>array('admin')),
);
?>

<h1>View PelabuhanBongkar #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'namaPOD',
		'ctime',
	),
)); ?>
