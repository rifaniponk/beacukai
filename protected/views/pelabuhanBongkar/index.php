<?php
$this->breadcrumbs=array(
	'Pelabuhan Bongkars',
);

$this->menu=array(
	array('label'=>'Create PelabuhanBongkar','url'=>array('create')),
	array('label'=>'Manage PelabuhanBongkar','url'=>array('admin')),
);
?>

<h1>Pelabuhan Bongkars</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
