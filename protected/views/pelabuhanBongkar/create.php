<?php
$this->breadcrumbs=array(
	'Pelabuhan Bongkars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PelabuhanBongkar','url'=>array('index')),
	array('label'=>'Manage PelabuhanBongkar','url'=>array('admin')),
);
?>

<h1>Create PelabuhanBongkar</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>