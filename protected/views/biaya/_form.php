<script type="text/javascript">
$(function(){
	$(".hitung").keyup(function(){
		var cif=Math.floor($("#Biaya_cif").val());
		var fob=Math.floor($("#Biaya_fob").val());
		var asuransi=Math.floor($("#Biaya_asuransi").val());
		var bm=Math.floor($("#Biaya_bm").val())/100;
		var cukai=Math.floor($("#Biaya_cukai").val())/100;
		var ppn=Math.floor($("#Biaya_ppn").val())/100;
		var ppnbm=Math.floor($("#Biaya_ppnbm").val())/100;
		var pph=Math.floor($("#Biaya_pph").val())/100;
		var kurs=Math.floor($("#Biaya_kurs").val());
		var freight=Math.floor($("#Biaya_freight").val());
		if (cif==0){
			var pabean= (fob+asuransi+freight) * kurs;
		}else{
			var pabean= cif * kurs;
		}
		var bm_rp=pabean*bm;
		var cukai_rp=cukai*pabean;
		var nilaiImpor= pabean + bm_rp + cukai_rp;
		var pdri=ppn*(pabean+bm_rp+cukai_rp) + pph * (pabean+bm_rp+cukai_rp);
		$("#Biaya_nilaiPabean").val(pabean);
		$("#Biaya_bm_rp").val(bm_rp);
		$("#Biaya_cukai_rp").val(cukai_rp);
		$("#Biaya_nilaiImpor").val(nilaiImpor);
		$("#Biaya_pdri").val(pdri);

	});
	$(".datepicker").datepicker({
		// minDate:0,
		dateFormat:'yy-mm-dd',
	})
})
</script>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'biaya-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'cif',array('class'=>'input-lg hitung','prepend'=>'$')); ?>

	<?php echo $form->textFieldRow($model,'fob',array('class'=>'input-lg hitung','prepend'=>'$')); ?>

	<?php echo $form->textFieldRow($model,'asuransi',array('class'=>'input-lg hitung','prepend'=>'$')); ?>

	<?php echo $form->textFieldRow($model,'bm',array('class'=>'input-lg hitung', 'append'=>'%')); ?>
 
	<?php echo $form->textFieldRow($model,'cukai',array('class'=>'input-lg hitung', 'append'=>'%')); ?>

	<?php echo $form->textFieldRow($model,'ppn',array('class'=>'input-lg hitung', 'append'=>'%', 'value'=>'10','readonly'=>'readonly')); ?>

	<?php echo $form->textFieldRow($model,'ppnbm',array('class'=>'input-lg hitung', 'append'=>'%')); ?>

	<?php echo $form->textFieldRow($model,'pph',array('class'=>'input-lg hitung', 'append'=>'%')); ?>

	<?php echo $form->textFieldRow($model,'kurs',array('class'=>'input-lg hitung','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'freight',array('class'=>'input-lg hitung','prepend'=>'$')); ?>

	<?php echo $form->textFieldRow($model,'nilaiPabean',array('class'=>'input-lg','readonly'=>'readonly','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'bm_rp',array('class'=>'input-lg', 'readonly'=>'readonly','prepend'=>'Rp')); ?>

    <?php echo $form->textFieldRow($model,'cukai_rp',array('class'=>'input-lg', 'readonly'=>'readonly','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'nilaiImpor',array('class'=>'input-lg','readonly'=>'readonly','prepend'=>'Rp')); ?>

    <?php echo $form->textFieldRow($model,'pdri',array('class'=>'input-lg', 'readonly'=>'readonly','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'tglTiba',array('class'=>'input-lg datepicker')); ?>

	<?php echo $form->fileFieldRow($model,'docAtc',array('class'=>'input-lg','maxlength'=>245)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
