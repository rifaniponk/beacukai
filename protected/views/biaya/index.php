<?php
$this->breadcrumbs=array(
	'Biayas',
);

$this->menu=array(
	array('label'=>'Create Biaya','url'=>array('create')),
	array('label'=>'Manage Biaya','url'=>array('admin')),
);
?>

<h1>Biayas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
