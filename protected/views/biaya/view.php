<?php
$this->breadcrumbs=array(
	'Biayas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Biaya','url'=>array('index')),
	array('label'=>'Create Biaya','url'=>array('create')),
	array('label'=>'Update Biaya','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Biaya','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Biaya','url'=>array('admin')),
);
?>

<h1>View Biaya #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cif',
		'fob',
		'asuransi',
		'bm',
		'cukai',
		'ppn',
		'ppnbm',
		'pph',
		'kurs',
		'freight',
		'nilaiPabean',
		'nilaiImpor',
		'tglTiba',
		'docAtc',
	),
)); ?>
