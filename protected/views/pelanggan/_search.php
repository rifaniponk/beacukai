<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ktp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>145)); ?>

	<?php echo $form->textFieldRow($model,'role',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'npwp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'noapi',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'npik',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'telp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'web',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'siupAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'npwpAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'ktpAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'isVerified',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ctime',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
