<?php
if (!isset($model)){
	$model=new Pelangganv;
}
if ($save_success==1){
	?>
	<script type="text/javascript">
	alert('User berhasil disimpan!');
	</script>
	<?php
	$model=new Pelangganv;
}elseif($save_success==0){
	?>
	<script type="text/javascript">
	alert('User gagal disimpan!');
	</script>
	<?php
}
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pelangganv-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
    'htmlOptions'=>array(
    	'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php 
	// echo $form->textFieldRow($model,'rolePelanggan',array('class'=>'span5')); 
	$modeldrop[]=array('id'=>1,'nama'=>'Importir Perusahaan');
	$modeldrop[]=array('id'=>2,'nama'=>'Importir Perorangan');
	$list = CHtml::listData($modeldrop, 'id', 'nama');
	echo $form->dropDownListRow($model,"rolePelanggan",$list,array('class'=>'span5','style'=>'padding:3px;'));
	?>
	<?php echo $form->textFieldRow($model,'username',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'namauser',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->hiddenField($model,'roleUser',array('class'=>'span5','maxlength'=>5,'value'=>'PEL')); ?>

	<!-- <?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?> -->

	<?php echo $form->textFieldRow($model,'ktp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'telp',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'npwp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'noapi',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'npik',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'web',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->fileFieldRow($model,'siupAtc',array('class'=>'span5 fper','maxlength'=>245)); ?>

	<?php echo $form->fileFieldRow($model,'npwpAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->fileFieldRow($model,'ktpAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->hiddenField($model,'isVerified',array('class'=>'span5','value'=>0)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
