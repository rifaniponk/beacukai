<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->user_id),array('view','id'=>$data->user_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ktp')); ?>:</b>
	<?php echo CHtml::encode($data->ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npwp')); ?>:</b>
	<?php echo CHtml::encode($data->npwp); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('noapi')); ?>:</b>
	<?php echo CHtml::encode($data->noapi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npik')); ?>:</b>
	<?php echo CHtml::encode($data->npik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telp')); ?>:</b>
	<?php echo CHtml::encode($data->telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('web')); ?>:</b>
	<?php echo CHtml::encode($data->web); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('siupAtc')); ?>:</b>
	<?php echo CHtml::encode($data->siupAtc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('npwpAtc')); ?>:</b>
	<?php echo CHtml::encode($data->npwpAtc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ktpAtc')); ?>:</b>
	<?php echo CHtml::encode($data->ktpAtc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isVerified')); ?>:</b>
	<?php echo CHtml::encode($data->isVerified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />

	*/ ?>

</div>