<?php
$this->breadcrumbs=array(
	'Pelanggans'=>array('index'),
	$model->user_id,
);

$this->menu=array(
	array('label'=>'List Pelanggan','url'=>array('index')),
	array('label'=>'Create Pelanggan','url'=>array('create')),
	array('label'=>'Update Pelanggan','url'=>array('update','id'=>$model->user_id)),
	array('label'=>'Delete Pelanggan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pelanggan','url'=>array('admin')),
);
?>

<h1>View Pelanggan #<?php echo $model->user_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'ktp',
		'nama',
		'role',
		'email',
		'alamat',
		'npwp',
		'noapi',
		'npik',
		'telp',
		'web',
		'siupAtc',
		'npwpAtc',
		'ktpAtc',
		'isVerified',
		'ctime',
	),
)); ?>
