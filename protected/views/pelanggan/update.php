<?php
$this->breadcrumbs=array(
	'Pelanggans'=>array('index'),
	$model->user_id=>array('view','id'=>$model->user_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pelanggan','url'=>array('index')),
	array('label'=>'Create Pelanggan','url'=>array('create')),
	array('label'=>'View Pelanggan','url'=>array('view','id'=>$model->user_id)),
	array('label'=>'Manage Pelanggan','url'=>array('admin')),
);
?>

<h1>Update Pelanggan <?php echo $model->user_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>