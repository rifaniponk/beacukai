<?php
$this->breadcrumbs=array(
	'Pelanggans'=>array('index'),
	'Manage',
);

?>

<h1>Manage Pelanggan</h1>


<ul class="nav nav-tabs" id="myTab">
  <li class="active"><a href="#home">Belum Di Approve</a></li>
  <li><a href="#profile">Sudah di Approve</a></li>
</ul>
<div class="tab-content" ng-controller='updateIsVerified'>
  <div class="tab-pane active" id="home" >
  	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pelanggan-grid-ua',
		'dataProvider'=>$modelua->search(),
		// 'filter'=>$modelua,
		'columns'=>array(
			array('name'=>'nama', 'sortable'=>false),
			array('name'=>'email', 'sortable'=>false),
			array('name'=>'alamat', 'sortable'=>false),
			array('header'=>'Dokumen', 'value'=>'sysHelper::generateLinkDoc($data->user_id)', 'type'=>'raw'),
			array('name'=>'isVerified', 'sortable'=>false, 'value'=>'sysHelper::VerifiedControl($data->isVerified,$data->user_id)'),
			/*
			array('name'=>'user_id', 'sortable'=>false),
			array('name'=>'ktp', 'sortable'=>false),
			'role',
			'npwp',
			'noapi',
			'npik',
			'telp',
			'web',
			'siupAtc',
			'npwpAtc',
			'ktpAtc',
			'ctime',
			*/
			array(
	            'class'=>'bootstrap.widgets.TbButtonColumn',
	            'header'=>'Action',
	            'template'=>'{view}{delete}',
	        ),
		),
	)); ?>
  </div>
  <div class="tab-pane" id="profile">
  	<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'pelanggan-grid-a',
		'dataProvider'=>$modela->search(),
		// 'filter'=>$modela,
		'columns'=>array(
			array('name'=>'nama', 'sortable'=>false),
			array('name'=>'email', 'sortable'=>false),
			array('name'=>'alamat', 'sortable'=>false),
			array('header'=>'Dokumen', 'value'=>'sysHelper::generateLinkDoc($data->user_id)', 'type'=>'raw'),
			array('name'=>'isVerified', 'sortable'=>false, 'value'=>'sysHelper::VerifiedControl($data->isVerified,$data->user_id)'),
			/*
			array('name'=>'user_id',  'sortable'=>false),
			array('name'=>'ktp', 'sortable'=>false),
			'role',
			'npwp',
			'noapi',
			'npik',
			'telp',
			'web',
			'siupAtc',
			'npwpAtc',
			'ktpAtc',
			'ctime',
			*/
			array(
	            'class'=>'bootstrap.widgets.TbButtonColumn',
	            'header'=>'Action',
	            'template'=>'{view}{delete}',
	        ),
		),
	)); ?>
  </div>
</div>
 
<script>
  $(function () {
    $('#myTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	})
  })
</script>


