<?php
$this->breadcrumbs=array(
	'Biayappjks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Biayappjk','url'=>array('index')),
	array('label'=>'Create Biayappjk','url'=>array('create')),
	array('label'=>'Update Biayappjk','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Biayappjk','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Biayappjk','url'=>array('admin')),
);
?>

<h1>View Biayappjk #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jenisImpor',
		'nilaiInvoice',
		'jmlKontainer',
		'jalur',
		'jmlDok',
		'jmlLampiranPIB',
		'ctime',
	),
)); ?>
