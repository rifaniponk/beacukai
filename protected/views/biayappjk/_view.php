<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenisImpor')); ?>:</b>
	<?php echo CHtml::encode($data->jenisImpor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilaiInvoice')); ?>:</b>
	<?php echo CHtml::encode($data->nilaiInvoice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlKontainer')); ?>:</b>
	<?php echo CHtml::encode($data->jmlKontainer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jalur')); ?>:</b>
	<?php echo CHtml::encode($data->jalur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlDok')); ?>:</b>
	<?php echo CHtml::encode($data->jmlDok); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlLampiranPIB')); ?>:</b>
	<?php echo CHtml::encode($data->jmlLampiranPIB); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />

	*/ ?>

</div>