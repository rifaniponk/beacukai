<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'barang-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'jenisImpor',array('1'=>'QQ','2'=>'FS'), array('class'=>'input-lg')); ?>

	<?php echo $form->textFieldRow($model,'nilaiInvoice',array('class'=>'input-lg','prepend'=>'Rp')); ?>

	<?php echo $form->textFieldRow($model,'jmlKontainer',array('class'=>'input-lg','append'=>'buah')); ?>

	<?php 
	if($joborder->jalur=="Hijau"){
		$model->jalur=1;
	}elseif ($joborder->jalur=="Hijau dengan notul") {
		$model->jalur=2;
	}elseif ($joborder->jalur=="Merah") {
		$model->jalur=3;
	}
	echo $form->hiddenField($model,'jalur');
	// echo $form->dropDownListRow($model,'jalur',array('1'=>'Jalur Hijau','2'=>'Jalur Hijau dengan Notul','3'=>'Jalur Merah'), array('class'=>'input-lg','readonly'=>'readonly')); 
	?>

	 <div class="control-group">
	    <label class="control-label" for="jalur">Jalur</label>
	    <div class="controls">
	      <input type="text" id="jalur" value="<?php echo $joborder->jalur?>" disabled>
	    </div>
	  </div>
	<?php echo $form->textFieldRow($model,'jmlDok',array('class'=>'input-lg','append'=>'lbr')); ?>

	<?php echo $form->textFieldRow($model,'jmlLampiranPIB',array('class'=>'input-lg','append'=>'lbr')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Submit' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
