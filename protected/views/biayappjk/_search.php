<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenisImpor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilaiInvoice',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlKontainer',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jalur',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlDok',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlLampiranPIB',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ctime',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
