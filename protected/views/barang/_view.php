<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('des')); ?>:</b>
	<?php echo CHtml::encode($data->des); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jml')); ?>:</b>
	<?php echo CHtml::encode($data->jml); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bruto')); ?>:</b>
	<?php echo CHtml::encode($data->bruto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('netto')); ?>:</b>
	<?php echo CHtml::encode($data->netto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('volume')); ?>:</b>
	<?php echo CHtml::encode($data->volume); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ctime')); ?>:</b>
	<?php echo CHtml::encode($data->ctime); ?>
	<br />

	*/ ?>

</div>