<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'barang-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>145)); ?>

	<?php echo $form->textAreaRow($model,'des',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'jml',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bruto',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'netto',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'volume',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ctime',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
