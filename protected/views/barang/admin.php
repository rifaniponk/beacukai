<?php
$this->breadcrumbs=array(
	'Barang',
);
?>

<h1>Barang</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'barang-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nama',
		'des',
		'jml',
		'bruto',
		'netto',
		/*
		'volume',
		'ctime',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
