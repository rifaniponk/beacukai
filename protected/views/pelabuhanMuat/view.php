<?php
$this->breadcrumbs=array(
	'Pelabuhan Muats'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PelabuhanMuat','url'=>array('index')),
	array('label'=>'Create PelabuhanMuat','url'=>array('create')),
	array('label'=>'Update PelabuhanMuat','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PelabuhanMuat','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PelabuhanMuat','url'=>array('admin')),
);
?>

<h1>View PelabuhanMuat #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'namaPOL',
		'ctime',
	),
)); ?>
