<?php
$this->breadcrumbs=array(
	'Pelabuhan Muats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PelabuhanMuat','url'=>array('index')),
	array('label'=>'Manage PelabuhanMuat','url'=>array('admin')),
);
?>

<h1>Create PelabuhanMuat</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>