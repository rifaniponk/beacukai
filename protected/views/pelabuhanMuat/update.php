<?php
$this->breadcrumbs=array(
	'Pelabuhan Muats'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PelabuhanMuat','url'=>array('index')),
	array('label'=>'Create PelabuhanMuat','url'=>array('create')),
	array('label'=>'View PelabuhanMuat','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PelabuhanMuat','url'=>array('admin')),
);
?>

<h1>Update PelabuhanMuat <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>