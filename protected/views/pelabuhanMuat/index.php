<?php
$this->breadcrumbs=array(
	'Pelabuhan Muats',
);

$this->menu=array(
	array('label'=>'Create PelabuhanMuat','url'=>array('create')),
	array('label'=>'Manage PelabuhanMuat','url'=>array('admin')),
);
?>

<h1>Pelabuhan Muats</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
