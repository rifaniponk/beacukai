<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="row-fluid">
					<div class="span12">
						<div class="row-fluid page-404">
							<div class="span5 number">
								<?php echo $code; ?>
							</div>
							<div class="span7 details">
								<h3>Opps, You're lost.</h3>
								<p><?php echo CHtml::encode($message); ?>
								</p>
								<form action="#">
									<div class="input-append">                      
										<input class="m-wrap" size="16" type="text" placeholder="keyword..." />
										<button class="btn blue">Search</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
