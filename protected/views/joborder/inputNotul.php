<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Input Notul',
);
?>
<!-- <h1>Input Notul</h1> -->
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'joborder-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->textFieldRow($model,'notulAngka',array('class'=>'span5')); ?>

	<?php echo $form->fileFieldRow($model,'notulAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
