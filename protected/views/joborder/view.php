<?php
$this->breadcrumbs=array(
	'Job Order'=>array('admin'),
	$model->id,
);
?>

<h1>View Joborder #<?php echo $model->id; ?></h1>
<?php
// null setter
// print_r($model->pib);die();
if(isset($model->pib->noPengajuan)){
	$val_pib=$model->pib->noPengajuan."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>", Yii::app()->createUrl($model->pib->atc), array("title"=>"Attachment","class"=>"fancy"))."&nbsp;&nbsp;".
				CHtml::link("<i class='icon-print'></i>",Yii::app()->createUrl("joborder/printPIB", array('pib_id'=>$model->pib_id)), array("title"=>"Print", "target"=>"_blank"));
}else{
	$val_pib="<span class='null'>Tidak diset</span>";
}

$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>".fancy",
    'config'=>array(
    	'transitionIn'	=>	'elastic',
		'transitionOut'	=>	'elastic',
		'speedIn'		=>	600, 
		'speedOut'		=>	200, 
		'overlayShow'	=>	true
    	),
    )
); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('name'=>'tglMulai','value'=>dateHelper::tgl_indo($model->tglMulai,true,false)),
		array('label'=>'Pelanggan','value'=>sysHelper::getUsername($model->pelanggan_user_id)),
		array('label'=>'Barang','value'=>$model->barang->nama, "type"=>'raw'),
		array('label'=>'Layanan','value'=>$model->layanan->nama, "type"=>'raw'),
		array('name'=>'status','value'=>"<span class='label label-info'>".$model->joborderstatus->status."</span>", "type"=>'raw'),
		array('label'=>'No Bill of Lading','value'=>$model->bl->nomor."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->bl->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Commercial Invoice','value'=>$model->ci->noCI."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->ci->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Shipping Instruction','value'=>$model->si->noSI."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->si->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Packling List', 'name'=>'pl','value'=>$model->pl."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->plAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Surat Jalan', 'name'=>'sj','value'=>$model->sj."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->sjAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('name'=>'No surat Kuasa','value'=>$model->sk."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->skAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No PIB','value'=>$val_pib,'type'=>'raw'),
		array('label'=>'SPPB','value'=>
				CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->sppbAtc), array("title"=>"Attachment","class"=>"fancy"))
				."&nbsp;&nbsp;".
				CHtml::link("<i class='icon-print'></i>",Yii::app()->createUrl("joborder/printSPPB", array('joborder_id'=>$model->id)), array("title"=>"Print", "target"=>"_blank")),
			'type'=>'raw'),
		
		// array('name'=>'pelayaran_user_id','value'=>$model->pelayaran_user_id),
		array('name'=>'tglPelayaran','value'=>dateHelper::tgl_indo($model->tglPelayaran),true,false),
		array('label'=>'Nilai Impor','value'=>isset($model->biaya->nilaiImpor) ? sysHelper::rp($model->biaya->nilaiImpor) : ''),
		array('label'=>'Nilai Pabean','value'=>isset($model->biaya->nilaiPabean) ? sysHelper::rp($model->biaya->nilaiPabean) : ''),
		array('label'=>'Nilai Bea Masuk','value'=>isset($model->biaya->bm_rp) ? sysHelper::rp($model->biaya->bm_rp) : ''),
		array('label'=>'Pajak dalam rangka impor','value'=>isset($model->biaya->pdri) ? sysHelper::rp($model->biaya->pdri) : ''),

		// array('name'=>'manifes','value'=>sysHelper::isManifesDiambil($model->manifes)),
		// array('name'=>'jalur','value'=>$model->jalur),
		// array('name'=>'sp2','value'=>sysHelper::isSP2Otorisasi($model->sp2)),
		array('name'=>'notulAngka','value'=>$model->notulAngka."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->notulAtc), array("title"=>"Attachment","class"=>"fancy")), 'type'=>'raw'),
		// array('name'=>'notulAtc','value'=>$model->notulAtc),
		array('name'=>'tglSelesai','value'=>dateHelper::tgl_indo($model->tglSelesai),true,false),
		array('name'=>'docAtc','value'=>$model->docAtc),
		// array('name'=>'ctime','value'=>$model->ctime),
	),
)); ?>
