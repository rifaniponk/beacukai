
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array( 
    'id'=>'createjoborder-form', 
    'enableAjaxValidation'=>false, 
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>
<script type="text/javascript">
<?php if ($transactionSucceed==0){ 
    echo "alert('Proses submit gagal! Silakan perbaiki kesalahan anda lalu submit ulang!')";
}elseif ($transactionSucceed==1) {
    echo "alert('Pesanan berhasil disubmit!')";
    $this->redirect(Yii::app()->createUrl('joborder/admin'));
} ?>
</script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".datepicker").datepicker({
            dateFormat:"yy-mm-dd",
            changeMonth: true,
            // numberOfMonths: 2,
            changeYear:true,
        })
    })
    </script>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p> 

    <?php //echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model,'layanan_id',array('class'=>'span5','value'=>'1')); ?>
    
    <!-- Barang -->
    <?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>145)); ?>
    <?php echo $form->textAreaRow($model,'des',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
    <?php echo $form->textFieldRow($model,'jml',array('class'=>'input-lg', 'append'=>'buah')); ?>
    <?php echo $form->textFieldRow($model,'bruto',array('class'=>'input-lg', 'append'=>'kg' )); ?>
    <?php echo $form->textFieldRow($model,'netto',array('class'=>'input-lg', 'append'=>'kg')); ?>
    <?php echo $form->textFieldRow($model,'volume',array('class'=>'input-lg', 'append'=>'m3')); ?>

    <!-- SI -->
    <?php echo $form->textFieldRow($model,'noSI',array('class'=>'span5','maxlength'=>30)); ?>
    <?php echo $form->textFieldRow($model,'tglTiba',array('class'=>'span5 datepicker')); ?>
    <?php echo $form->textFieldRow($model,'tglBrgkt',array('class'=>'span5 datepicker')); ?>
    <?php echo $form->fileFieldRow($model,'si_atc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- BL -->
    <?php echo $form->textFieldRow($model,'nomor',array('class'=>'span5','maxlength'=>30)); ?>
    <?php echo $form->textFieldRow($model,'bl_tgl',array('class'=>'span5 datepicker')); ?>
    <?php echo $form->fileFieldRow($model,'atc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- PL -->
    <?php echo $form->textFieldRow($model,'pl',array('class'=>'span5','maxlength'=>45)); ?>
    <?php echo $form->fileFieldRow($model,'plAtc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- SJ -->
    <?php echo $form->textFieldRow($model,'sj',array('class'=>'span5','maxlength'=>45)); ?>
    <?php echo $form->fileFieldRow($model,'sjAtc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- SK -->
    <?php echo $form->textFieldRow($model,'sk',array('class'=>'span5','maxlength'=>45)); ?>
    <?php echo $form->fileFieldRow($model,'skAtc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- CI -->
    <?php echo $form->textFieldRow($model,'noCI',array('class'=>'span5','maxlength'=>45)); ?>
    <?php echo $form->textFieldRow($model,'ci_tgl',array('class'=>'span5 datepicker')); ?>
    <?php echo $form->fileFieldRow($model,'ci_atc',array('class'=>'span5','maxlength'=>245)); ?>

    <!-- FK, hidden field -->
    <?php echo $form->hiddenField($model,'barang_id',array('class'=>'span5')); ?>
    <?php echo $form->hiddenField($model,'bl_id',array('class'=>'span5')); ?>
    <?php echo $form->hiddenField($model,'ci_id',array('class'=>'span5')); ?>
    <?php echo $form->hiddenField($model,'si_id',array('class'=>'span5')); ?>


    <div class="form-actions"> 
        <button class="btn blue" type="submit">Submit</button>
    </div> 

<?php $this->endWidget(); ?>
