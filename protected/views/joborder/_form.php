<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'joborder-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'tglMulai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pelanggan_user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'barang_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'layanan_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bl_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ci_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'si_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pl',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'plAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'sj',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'sjAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'sk',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'skAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>5)); ?>

	<?php echo $form->textFieldRow($model,'pelayaran_user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglPelayaran',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'biaya_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'manifes',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jalur',array('class'=>'span5','maxlength'=>5)); ?>

	<?php echo $form->textFieldRow($model,'sp2',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'notulAngka',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'notulAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'tglSelesai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'invoice_kode',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'docAtc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->textFieldRow($model,'ctime',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
