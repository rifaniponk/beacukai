<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Upload SPPB',
);

?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array( 
    'id'=>'createjoborder-form', 
    'enableAjaxValidation'=>false, 
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p> 
    
	<?php echo $form->fileFieldRow($model,'sppbAtc',array('class'=>'span5','maxlength'=>245)); ?>

    <div class="form-actions"> 
        <button class="btn blue" type="submit">Submit</button>
    </div> 

<?php $this->endWidget(); ?>
