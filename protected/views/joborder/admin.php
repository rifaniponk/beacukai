<?php
$this->breadcrumbs=array(
	'Job Order',
);
?>

<h1>Job Order</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'joborder-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array('header'=>'Pelanggan','value'=>'$data->pelangganUser->nama', 'visible'=>!Yii::app()->user->isPEL()),
		'tglMulai',
		array('name'=>'barang_id', 'value'=>'$data->barang->nama','header'=>'Nama Barang'),
		array('name'=>'bl_id', 'value'=>'$data->bl->nomor','header'=>'No BL'),
		array('name'=>'ci_id', 'value'=>'$data->ci->noCI','header'=>'No CI'),
		array('name'=>'si_id', 'value'=>'$data->si->noSI','header'=>'No SI'),
		array('name'=>'pl', 'value'=>'$data->pl'),
        array('name'=>'status','sortable'=>false, 'filter'=>false, 'value'=>'$data->joborderstatus->status'),
		
    //action for input tgl by PLY
    array( 
            'class' => 'editable.EditableColumn',
            'name' => 'tglPelayaran',
            'editable' => array(
                'type'          => 'date',
                'viewformat'    => 'dd.mm.yyyy',
                'url'       => $this->createUrl('Joborder/updateAjax'),
                'placement' => 'left',
            ),
            'visible'=>Yii::app()->user->isPLY(),
          ), 

    // action for cdpi by OP
		// array( 
		// 	'visible'=>Yii::app()->user->isOP() || Yii::app()->user->isMO(),
  //           'class' => 'editable.EditableColumn',
  //           'name' => 'status',
  //           'headerHtmlOptions' => array('style' => 'width: 100px'),
  //           'editable' => array(
  //               'type'     => 'select',
  //               'url'      => $this->createUrl('Joborder/updateAjax '),
  //               'source'   => $this->createUrl('Joborder/GetStatus'),
  //               // 'options'  => array(    //custom display 
  //               //      'display' => 'js: function(value, sourceData) {
  //               //           var selected = $.grep(sourceData, function(o){ return value == o.value; }),
  //               //               colors = {"tPLY": "gray", "cdpi1": "green", "cdpi0": "red"};
  //               //           $(this).text(selected[0].text).css("color", colors[value]);    
  //               //       }'
  //               // ),
  //               //onsave event handler 
  //               'onSave' => 'js:function(e, params) {
  //                     console && console.log("saved value: "+params.newValue);
  //               }',
  //           )
  //       ),


        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'header'=>'Action',
            'template'=>'{viewjb}{Revisi}{hitungBiaya}{uploadBuktiBayar}{isiPIB}{uploadBuktiBayarNotul}{uploadDokPen}
                            {hitungUlangBiaya}{inputNotul}{uploadSppb}{cekDok}{manAmbil}{otoMan}{inputJalur}{cekDokPen}
                            {otoSP2}{isiBiayaPPJK}{kirimInvoice}{cetakInvoice}{bayarPPJK}{terimaBayar}{joc}{otoJoc}{delete}',
            'buttons'=>array(
                // 'view'
                'viewjb'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/view", array("id"=>$data->id))',
                    'label'=>'View',
                    'btnClass'=>'btn mini default',
                    ),

                // Revisi by PEL
                'Revisi'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/update", array("id"=>$data->id))',
                    'label'=>'Revisi Dokumen',
                    'btnClass'=>'btn mini green',
                    'visible'=>'Yii::app()->user->isPEL() && ($data->status=="init" || $data->status=="cdpi0")',
                    ),

                //Cek dokumen by OP
                'cekDok'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/cekDokumen", array("joborder_id"=>$data->id))',
                    'label'=>'Cek Dokumen',
                    'btnClass'=>'btn mini green',
                    'visible'=>'Yii::app()->user->isOP() && ($data->status=="init" || $data->status=="cdpi3")',
                    ),
                
                //hitung biaya by OP
                'hitungBiaya'=>array(
                    'url'=>'Yii::app()->createUrl("biaya/create", array("joborder_id"=>$data->id))',
                    'label'=>'Hitung Biaya',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isOP() && $data->status=="tPLY"',
                    ),
                
                //manifes telah diambil by OP
                'manAmbil'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$data->id,"status"=>"man"))',
                    'label'=>'Manifes telah diambil',
                    'btnClass'=>'btn mini yellow',
                    'visible'=>'Yii::app()->user->isOP() && $data->status=="byop"',
                    ),
                
                //Otorisasi PIB by MO
                'otoMan'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$data->id,"status"=>"pibot"))',
                    'label'=>'Otorisasi',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isMO() && $data->status=="pibis"',
                    ),

                // Input jalur by OP
                'inputJalur'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/inputJalur", array("joborder_id"=>$data->id))',
                    'label'=>'Input Jalur',
                    'btnClass'=>'btn mini green',
                    'visible'=>'Yii::app()->user->isOP() && $data->status=="pibot"',
                    ),

                //upload bukti bayar PIB by MK
                'uploadBuktiBayar'=>array(
                    'url'=>'Yii::app()->createUrl("buktibayar/create", array("joborder_id"=>$data->id,"jenis"=>"PIB"))',
                    'label'=>'Upload Bukti Bayar',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="man"',
                    ),

                //isi PIB by OP
                'isiPIB'=>array(
                    'url'=>'Yii::app()->createUrl("pib/create", array("joborder_id"=>$data->id))',
                    'label'=>'Isi PIB',
                    'btnClass'=>'btn mini yellow',
                    'visible'=>'Yii::app()->user->isOP() && $data->status=="bukba"',
                    ),

                //upload bukti bayar notul by PEL
                'uploadBuktiBayarNotul'=>array(
                    'url'=>'Yii::app()->createUrl("buktibayar/create", array("joborder_id"=>$data->id,"jenis"=>"Notul"))',
                    'label'=>'Upload Bukti Bayar Notul',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isPEL() && ($data->status=="jhp" || $data->status=="jm3")',
                    ),

                //upload dokumen pendukung by PEL
                'uploadDokPen'=>array(
                    'url'=>'Yii::app()->createUrl("dokumen/admin", array("joborder_id"=>$data->id))',
                    'label'=>'Upload Dokumen Pendukung',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isPEL() && ($data->status=="jhn1" || $data->status=="jhn32" || $data->status=="jm4" || $data->status=="jm62")',
                    ),

                //Hitung ulang biaya by OP
                'hitungUlangBiaya'=>array(
                    'url'=>'Yii::app()->createUrl("biaya/update", array("id"=>$data->biaya_id,"joborder_id"=>$data->id))',
                    'label'=>'Hitung Ulang Biaya',
                    'btnClass'=>'btn mini green',
                    'visible'=>'Yii::app()->user->isOP() && ($data->status=="jm" || $data->status=="jm22")',
                    ),

                //Input notul by OP
                'inputNotul'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/inputNotul", array("id"=>$data->id))',
                    'label'=>'Input Notul',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isOP() && ($data->status=="jm21" || $data->status=="jhp")',
                    ),

                //cek dokumen by OP
                'cekDokPen'=>array(
                    'url'=>'Yii::app()->createUrl("dokumen/cekDokumenPendukung", array("joborder_id"=>$data->id))',
                    'label'=>'Cek Dokumen Pendukung',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isOP() && ($data->status=="jhn2" || $data->status=="jhn33" || $data->status=="jm5" || $data->status=="jm63")',
                    ),

                //Upload SPPB by OP 
                'uploadSppb'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/uploadSppb", array("joborder_id"=>$data->id))',
                    'label'=>'Upload SPPB',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isOP() && ($data->status=="jm61" || $data->status=="jhn31" || $data->status=="jh1" )',
                    ),

                //Otorisasi SP2 by OP
                'otoSP2'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$data->id,"status"=>"sp2"))',
                    'label'=>'Otorisasi SP2',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isOP() && $data->status=="sppb"',
                    ),

                //Isi biaya PPJK
                'isiBiayaPPJK'=>array(
                    'url'=>'Yii::app()->createUrl("biayappjk/create", array("joborder_id"=>$data->id,"biaya_id"=>$data->biaya_id))',
                    'label'=>'Isi Biaya PPJK',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="sp2"',
                    ),

                //Isi biaya PPJK
                'kirimInvoice'=>array(
                    'url'=>'Yii::app()->createUrl("invoice/create", array("joborder_id"=>$data->id))',
                    'label'=>'Kirim Invoice',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="hti"',
                    ),

                //cetak invoice
                'cetakInvoice'=>array(
                    'url'=>'Yii::app()->createUrl("invoice/print", array("no"=>$data->invoice_no))',
                    'label'=>'Cetak Invoice',
                    'btnClass'=>'btn mini green',
                    'options'=>array('target'=>'_blank'),
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="inv"',
                    ),

                //Bayar biaya PPJK
                // 'bayarPPJK'=>array(
                //     'url'=>'Yii::app()->createUrl("pembayaranInvoice/create", array("joborder_id"=>$data->id))',
                //     'label'=>'Bayar Invoice',
                //     'btnClass'=>'btn mini purple',
                //     'visible'=>'Yii::app()->user->isPEL() && $data->status=="inv"',
                //     ),
 
                //Bayar biaya PPJK by PEL
                'bayarPPJK'=>array(
                    'url'=>'Yii::app()->createUrl("Invoice/view", array("no"=>$data->invoice_no,"joborder_id"=>$data->id))',
                    'label'=>'Bayar Invoice',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isPEL() && $data->status=="inv"',
                    ),

                //Terima pembayaran by MK
                'terimaBayar'=>array(
                    'url'=>'Yii::app()->createUrl("pembayaranInvoice/view", array("invoice_no"=>$data->invoice_no))',
                    'label'=>'Terima Pembayaran',
                    'btnClass'=>'btn mini blue',
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="bjtb"',
                    ),

                //Job Order Complete by MK
                'joc'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$data->id,"status"=>"ljoc"))',
                    'label'=>'Job Order Complete!',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isMK() && $data->status=="bjtb1"',
                    ),

                //Otorisasi Job Order Complete by DIR
                'otoJoc'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/updateStatus", array("joborder_id"=>$data->id,"status"=>"ljoc1"))',
                    'label'=>'Otorisasi Job Order Complete!',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isDIR() && $data->status=="ljoc"',
                    ),

                //Otorisasi Job Order Complete by DIR
                'delete'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/delete", array("id"=>$data->id))',
                    'label'=>'Delete',
                    'btnClass'=>'btn mini red',
                    'visible'=>'Yii::app()->user->isMO()',
                    ),

                ),
        ),



		/*
		array('name'=>'layanan_id', 'value'=>'$data->layanan->nama','header'=>'Layanan'),
		array('name'=>'pelanggan_user_id', 'value'=>'$data->pelanggan->nama','header'=>'Nama Pelanggan'),
		'plAtc',
		'sj',
		'sjAtc',
		'sk',
		'skAtc',
		'status',
		array('name'=>'pelayaran_user_id', 'value'=>'$data->pelayaran->nama','header'=>'Pelayaran'),
		'tglPelayaran',
		array('name'=>'biaya_id', 'value'=>'$data->biaya->nilaiImpor','header'=>'Nilai Impor'),
		'manifes',
		'jalur',
		'sp2',
		'notulAngka',
		'notulAtc',
		'tglSelesai',
		'invoice_kode',
		'docAtc',
		'ctime',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
		*/
	),
)); ?>
