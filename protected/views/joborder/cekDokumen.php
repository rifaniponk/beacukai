<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Cek Dokumen',
);

?>
<?php
$this->widget('application.extensions.fancybox.EFancyBox', array(
    'target'=>".fancy",
    'config'=>array(
    	'transitionIn'	=>	'elastic',
		'transitionOut'	=>	'elastic',
		'speedIn'		=>	600, 
		'speedOut'		=>	200, 
		'overlayShow'	=>	true
    	),
    )
); ?>
<?php echo CHtml::link("<i class='icon-thumbs-up'></i> Dokumen telah sesuai",Yii::app()->createUrl('joborder/updateStatus?joborder_id='.$model->id.'&status=cdpi1'), array('class'=>'btn btn-lg green')); ?>
<?php echo CHtml::link("<i class='icon-thumbs-down'></i> Dokumen perlu direvisi",Yii::app()->createUrl('joborder/updateStatus?joborder_id='.$model->id.'&status=cdpi0'), array('class'=>'btn btn-lg red')); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>'model',
	'attributes'=>array(
		// 'id',
		array('name'=>'tglMulai','value'=>dateHelper::tgl_indo($model->tglMulai,true,false)),
		array('label'=>'Pelanggan','value'=>sysHelper::getUsername($model->pelanggan_user_id)),
		array('label'=>'Barang','value'=>$model->barang->nama, "type"=>'raw'),
		array('label'=>'Layanan','value'=>$model->layanan->nama, "type"=>'raw'),
		array('name'=>'status','value'=>"<span class='label label-info'>".$model->joborderstatus->status."</span>", "type"=>'raw'),
		array('label'=>'No Bill of Lading','value'=>$model->bl->nomor."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->bl->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Commercial Invoice','value'=>$model->ci->noCI."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->ci->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('label'=>'No Shipping Instruction','value'=>$model->si->noSI."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->si->atc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('name'=>'pl', 'label'=>'No Packling List', 'value'=>$model->pl."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->plAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('name'=>'sj', 'label'=>'No Surat Jalan', 'value'=>$model->sj."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->sjAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		array('name'=>'sk','label'=>'No Surat Kuasa' ,'value'=>$model->sk."&nbsp;&nbsp;".CHtml::link("<i class='icon-file'></i>",Yii::app()->createUrl($model->skAtc), array("title"=>"Attachment","class"=>"fancy")),'type'=>'raw'),
		// array('name'=>'pelayaran_user_id','value'=>$model->pelayaran_user_id),
		array('name'=>'tglPelayaran','value'=>$model->tglPelayaran),
	),
)); ?>