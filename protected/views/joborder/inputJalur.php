<?php
$this->breadcrumbs=array(
	'Job Order'=>Yii::app()->createUrl('joborder/admin'),
	'Input Jalur',
);

?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array( 
    'id'=>'createjoborder-form', 
    'enableAjaxValidation'=>false, 
    'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p> 
    
	<?php echo $form->fileFieldRow($model,'jalurAtc',array('class'=>'span5','maxlength'=>245)); ?>
	<div class="control-group ">
		<label class="control-label" for="Joborder_jalur">Pilih Jalur</label>
		<div class="controls">
		    <?php 
		    	// $modeldrop[0]=array('id'=>'0','nama'=>'-- Pilih Jalur --');
		    	$modeldrop[1]=array('id'=>'1','nama'=>'Jalur Hijau');
		    	$modeldrop[2]=array('id'=>'2','nama'=>'Jalur Hijau dengan notul');
		    	$modeldrop[3]=array('id'=>'3','nama'=>'Jalur Merah');

			    $list = CHtml::listData($modeldrop, 'id', 'nama');
				echo CHtml::dropDownList("Joborder[jalur]",0, $list);
			?>
		</div>
	</div>

    <div class="form-actions"> 
        <button class="btn blue" type="submit">Submit</button>
    </div> 

<?php $this->endWidget(); ?>
