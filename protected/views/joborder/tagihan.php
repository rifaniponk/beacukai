<?php
$this->breadcrumbs=array(
    'Tagihan',
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array( 
    'id'=>'joborder-grid', 
    'dataProvider'=>$model->search(), 
    // 'filter'=>$model, 
    'columns'=>array( 
        array('name'=>'id','sortable'=>false),
        array('name'=>'tglMulai','value'=>'dateHelper::tgl_indo($data->tglMulai)','sortable'=>false),
        array('header'=>'Barang', 'value'=>'$data->barang->nama'),
        array('header'=>'Total Tagihan','value'=>'isset($data->invoice->grandtotal) ? sysHelper::rp($data->invoice->grandtotal) : "Belum Ada"'),
        /*
        'layanan_id',
        'bl_id',
        'pelanggan_user_id',
        'ci_id',
        'si_id',
        'pl',
        'plAtc',
        'sj',
        'sjAtc',
        'sk',
        'skAtc',
        'status',
        'pelayaran_user_id',
        'tglPelayaran',
        'biaya_id',
        'manifes',
        'pib_id',
        'jalur',
        'jalurAtc',
        'sp2',
        'notulAngka',
        'notulAtc',
        'sppbAtc',
        'tglSelesai',
        'docAtc',
        'biayaPPJK_id',
        'invoice_no',
        'ctime',
        */
        array( 
            'class'=>'bootstrap.widgets.TbButtonColumn', 
            'header'=>'Action',
            'template'=>'{view}{bayarPPJK}',
            'buttons'=>array(
                'view'=>array(
                    'url'=>'Yii::app()->createUrl("joborder/view", array("id"=>$data->id))',
                    ),
                //Bayar biaya PPJK
                'bayarPPJK'=>array(
                    'url'=>'Yii::app()->createUrl("Invoice/view", array("no"=>$data->invoice_no,"joborder_id"=>$data->id))',
                    'label'=>'Bayar Invoice',
                    'btnClass'=>'btn mini purple',
                    'visible'=>'Yii::app()->user->isPEL() && $data->status=="inv"',
                    ),
                ),
        ), 
    ), 
)); ?> 