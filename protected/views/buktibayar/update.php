<?php
$this->breadcrumbs=array(
	'Buktibayars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Buktibayar','url'=>array('index')),
	array('label'=>'Create Buktibayar','url'=>array('create')),
	array('label'=>'View Buktibayar','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Buktibayar','url'=>array('admin')),
);
?>

<h1>Update Buktibayar <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>