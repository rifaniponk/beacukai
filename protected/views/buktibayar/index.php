<?php
$this->breadcrumbs=array(
	'Buktibayars',
);

$this->menu=array(
	array('label'=>'Create Buktibayar','url'=>array('create')),
	array('label'=>'Manage Buktibayar','url'=>array('admin')),
);
?>

<h1>Buktibayars</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
