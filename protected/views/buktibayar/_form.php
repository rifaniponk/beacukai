<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'buktibayar-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'judul',array('class'=>'span5','maxlength'=>145)); ?>

	<?php echo $form->fileFieldRow($model,'atc',array('class'=>'span5','maxlength'=>245)); ?>

	<?php echo $form->hiddenField($model,'JobOrder_id',array('class'=>'span5')); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
