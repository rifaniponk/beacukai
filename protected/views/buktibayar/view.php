<?php
$this->breadcrumbs=array(
	'Buktibayars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Buktibayar','url'=>array('index')),
	array('label'=>'Create Buktibayar','url'=>array('create')),
	array('label'=>'Update Buktibayar','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Buktibayar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Buktibayar','url'=>array('admin')),
);
?>

<h1>View Buktibayar #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'judul',
		'atc',
		'JobOrder_id',
		'ctime',
	),
)); ?>
