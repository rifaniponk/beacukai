<?php
$this->breadcrumbs=array(
	'Pibs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pib','url'=>array('index')),
	array('label'=>'Create Pib','url'=>array('create')),
	array('label'=>'View Pib','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pib','url'=>array('admin')),
);
?>

<h1>Update Pib <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>