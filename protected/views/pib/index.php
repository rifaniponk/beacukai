<?php
$this->breadcrumbs=array(
	'Pibs',
);

$this->menu=array(
	array('label'=>'Create Pib','url'=>array('create')),
	array('label'=>'Manage Pib','url'=>array('admin')),
);
?>

<h1>Pibs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
