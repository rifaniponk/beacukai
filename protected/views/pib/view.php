<?php
$this->breadcrumbs=array(
	'Pibs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Pib','url'=>array('index')),
	array('label'=>'Create Pib','url'=>array('create')),
	array('label'=>'Update Pib','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Pib','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pib','url'=>array('admin')),
);
?>

<h1>View Pib #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'noPengajuan',
		'ctime',
	),
)); ?>
