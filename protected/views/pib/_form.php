<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pib-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
     ),
	 'htmlOptions'=>array(
        'enctype'=>'multipart/form-data'),
)); ?>
	<script type="text/javascript">
    $(document).ready(function(){
        $(".datepicker").datepicker({
            dateFormat:"yy-mm-dd",
            changeMonth: true,
            // numberOfMonths: 2,
            changeYear:true,
        })
    })
    </script>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'noPengajuan',array('class'=>'span5','maxlength'=>45)); ?>
	<?php echo $form->textFieldRow($model,'tglPengajuan',array('class'=>'span5 datepicker')); ?>
	<?php echo $form->fileFieldRow($model,'atc',array('class'=>'span5','maxlength'=>245)); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Submit' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
