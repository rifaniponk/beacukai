<!-- Slider Wrapper -->
<div class="slider-wrap">
   <div class="container">
      <div class="sixteen columns">
         <div class="slider-box">
            <div class="nivoslider">
               <img src="<?php echo Yii::app()->request->baseUrl?>/asset/img/1.jpg" alt="" title="">
               <img src="<?php echo Yii::app()->request->baseUrl?>/asset/img/2.png" alt="" title="">
               <img src="<?php echo Yii::app()->request->baseUrl?>/asset/img/3.jpg" alt="" title="">
               <img src="<?php echo Yii::app()->request->baseUrl?>/asset/img/4.jpg" alt="" title="">
               <img src="<?php echo Yii::app()->request->baseUrl?>/asset/img/5.jpg" alt="" title="">
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Slider Wrapper -->
<div class="container">

   <!-- Clients Widget -->
   <div class="sixteen columns">
      <div class="underline-heading">
         <h2>Our Clients</h2>
      </div>
   </div>
   <div class="twelve columns">
      <ul>
         <li>PT.SEMEN ANDALAS INDONESIA</li>
         <li>PT. PUPUK ISKANDAR MUDA (Persero)</li>
         <li>PT. PELITA NUSA PERKASA</li>
      </ul>
   </div>

   <!-- END Clients Widget -->
</div>