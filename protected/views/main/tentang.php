<br><br><br>
<h3>About Us</h3>
	<p>PT. Tumbuh Selaras Alam adalah perusahaan yang bergerak di bidang pengurusan jasa kepabeanan yang juga sudah mulai berkembang menjadi freight forwarding, yaitu dalam hal kepengurusan ekspedisi barang untuk kegiatan ekspor dan impor barang melalui laut.
	PT. Tumbuh Selaras Alam berlokasi di Jalan Karang Tengah Laksana No. 47 Karang Tengah RT01/RW08, Lebak Bulus, Cilandak, Jakarta Selatan dan kantor cabang Jalan Tgk. Imuem. Luengbata No. 202 Banda Aceh Nanggroe Aceh Darussalam, dengan nomor SIUJPT : P2TSP.552.1/2461/JPT/2008.</p>
<h3>Visi dan Misi Perusahaan</h3>
	<p>Visi perusahaan adalah menjadi salah satu perusahaan jasa pengiriman yang memberikan jasa pelayanan bagi masyarakat secara penuh totalitas.</p>
	<p>Misi perusahaan adalah memberikan layanan yang berkualitas tinggi terhadap pelanggan. Memberikan pelayanan terbaik terhadap pelanggan dengan mengirimkan barang ke pemilik atau pelanggan sampai ke tujuan dengan kondisi yang diharapkan dan tepat waktu, dan mengatasi tantangan industri dengan siap menghadapi setiap perkembangan dan perubahan yang ada di dalam pasar.</p>
<h3>Client List</h3>
<ul>
	<li>PT.SEMEN ANDALAS INDONESIA</li>
	<li>PT. PUPUK ISKANDAR MUDA (Persero)</li>
	<li>PT. PELITA NUSA PERKASA</li>
</ul>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
