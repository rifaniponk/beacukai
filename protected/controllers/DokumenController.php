<?php

class DokumenController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','admin','delete' ,'uploadDone','CekDokumenPendukung'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionCekDokumenPendukung($joborder_id)
	{
		$jo=Joborder::model()->findByPk($joborder_id);
		$model=new Dokumen('search');
		$model->JobOrder_id=$joborder_id;
		$this->render('cekDokumenPendukung',array(
			'model'=>$model,
			'joborder'=>$jo,
			'joborder_id'=>$model->JobOrder_id,
		));
	}
	public function actionUploadDone($joborder_id)
	{
		$jo=Joborder::model()->findByPk($joborder_id);

		//insert ke notif
		$n=new Notif;
		$n->dari=Yii::app()->user->id;
		$n->roleDari=Yii::app()->user->getRoleCode();
		$n->roleKe='OP';
		$n->row_id=$jo->id;
		$n->tableDesc_id=9;
		$n->field='status';
		$n->isSentToRole=1;


		if ($jo->status=='jhn32'){
			$jo->status='jhn33';
		}elseif ($jo->status=='jhn1') {
			$jo->status='jhn2';
		}elseif ($jo->status=='jm4') {
			$jo->status='jm5';
		}elseif ($jo->status=='jm62') {
			$jo->status='jm63';
		}

		$n->JOStatus=$jo->status;

		if($jo->save()){
			$n->save();
		}
		$this->redirect(Yii::app()->createUrl('Joborder'));
	}
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate($joborder_id)
	{
		$model=new Dokumen;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Dokumen']))
		{
			$model->attributes=$_POST['Dokumen'];
			$model->JobOrder_id=$joborder_id;

			$model->atc=CUploadedFile::getInstance($model,'atc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->atc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->atc->saveAs($dir.$pelengkap.$model->atc);
	           $model->atc=$dir.$pelengkap.$model->atc;
            }else{
            	unset($model->atc);
            }

			if($model->save())
				$this->redirect(array('admin','joborder_id'=>$joborder_id));
		}

		$this->render('create',array(
			'model'=>$model,
			'joborder_id'=>$joborder_id,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Dokumen']))
		{
			$model->attributes=$_POST['Dokumen'];
			$model->JobOrder_id=$joborder_id;

			$model->atc=CUploadedFile::getInstance($model,'atc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->atc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->atc->saveAs($dir.$pelengkap.$model->atc);
	           $model->atc=$dir.$pelengkap.$model->atc;
            }else{
            	unset($model->atc);
            }

			if($model->save())
				$this->redirect(array('admin','joborder_id'=>$joborder_id));
		}

		$this->render('update',array(
			'model'=>$model,
			'joborder_id'=>$model->JobOrder_id,

		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex($joborder_id)
	{
		$this->redirect(Yii::app()->createUrl('Dokumen/admin',array('joborder_id'=>$joborder_id)));
	}

	public function actionAdmin($joborder_id)
	{
		$model=new Dokumen('search');
		$model->unsetAttributes();  // clear any default values
		$model->JobOrder_id=$joborder_id;
		if(isset($_GET['Dokumen']))
			$model->attributes=$_GET['Dokumen'];

		$this->render('admin',array(
			'model'=>$model,
			'joborder_id'=>$joborder_id,
		));
	}

	public function loadModel($id)
	{
		$model=Dokumen::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='dokumen-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
