<?php

class JoborderController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create','update','admin','index','history', 'printSPPB','printPIB','tagihan'),
				'expression'=>'Yii::app()->user->isPEL()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('getStatus','hitungBiaya','inputNotul','cekDokumen','updateStatus','inputJalur','uploadSppb','printSPPB','printPIB','history'),
				'expression'=>'Yii::app()->user->isOP()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('getStatus','updateStatus', 'printSPPB','printPIB','delete'),
				'expression'=>'Yii::app()->user->isMO()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('updateStatus','printSPPB','printPIB'),
				'expression'=>'Yii::app()->user->isMK()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('updateStatus','printSPPB','printPIB','history'),
				'expression'=>'Yii::app()->user->isDIR()',
			),
			array('allow',
				'actions'=>array('admin','updateAjax','view', 'printSPPB','printPIB','history'),
				'expression'=>'!Yii::app()->user->isGuest',
				),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionTagihan($id=null)
	{	
		if($id===null){
			$this->pageTitle=Yii::app()->name." - Tagihan";
			$model=new Joborder('search');
			$model->unsetAttributes();  // clear any default values
			if (Yii::app()->user->isPEL()){
				$model->pelanggan_user_id=Yii::app()->user->id;
			}
			if(isset($_GET['Joborder']))
				$model->attributes=$_GET['Joborder'];

			$this->render('tagihan',array(
				'model'=>$model,
			));
		}else{
			$model=Invoice::model()->findByAttributes(array('no'=>$id));
			$this->render('//invoice/view',array(
				'model'=>$model,
			));
		}
	}

	public function actionPrintSPPB($joborder_id)
	{
		$this->layout='print';
		$this->render('printSPPB',array(
			'joborder'=>Joborder::model()->findByPk($joborder_id),
		));
	}

	public function actionPrintPIB($pib_id)
	{
		$this->layout='print';
		$this->render('printPIB',array(
			'pib'=>Pib::model()->findByPk($pib_id),
		));
	}
	
	public function actionUploadSppb($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Upload SPPB";
		$model=$this->loadModel($joborder_id);
		if(isset($_POST['Joborder'])){
			$model->attributes=$_POST['Joborder'];
			$model->sppbAtc=CUploadedFile::getInstance($model,'sppbAtc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->sppbAtc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->sppbAtc->saveAs($dir.$pelengkap.$model->sppbAtc);
	           $model->sppbAtc=$dir.$pelengkap.$model->sppbAtc;
	            
	            $model->status='sppb';
	            if ($model->save()){
	            	$this->redirect(Yii::app()->createUrl('joborder/admin'));
	            }
            }else{
            	unset($model->sppbAtc);
            }
		}
		$this->render('uploadSppb',array(
			'model'=>$model,
			));
	}
	public function actionInputJalur($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Input Jalur";
		$model=$this->loadModel($joborder_id);
		if(isset($_POST['Joborder'])){
			$data=$_POST['Joborder'];
			$model->attributes=$_POST['Joborder'];
			$model->jalurAtc=CUploadedFile::getInstance($model,'jalurAtc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->jalurAtc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->jalurAtc->saveAs($dir.$pelengkap.$model->jalurAtc);
	           $model->jalurAtc=$dir.$pelengkap.$model->jalurAtc;
            }else{
            	unset($model->jalurAtc);
            }
            // print_r($data);echo "<br>";
            // print_r($model->jalur);
            // die();
			
            if($data['jalur']==1){
            	$model->status="jh1";
            	$model->jalur=1;
            }elseif($data['jalur']==2){
            	$model->status="jhp";
            	$model->jalur=2;
            }elseif($data['jalur']==3){
            	$model->status="jm";
            	$model->jalur=3;

            }


            if ($model->save()){
	        	$n=new Notif;
				$n->dari=Yii::app()->user->id;
				$n->roleDari=Yii::app()->user->getRoleCode();
				$n->row_id=$model->id;
				$n->tableDesc_id=9;
				$n->field='status';
				$n->isSentToRole=0;
				$n->ke=$model->pelanggan_user_id;
				$n->JOStatus=$model->status;
				$n->save();

            	$this->redirect(Yii::app()->createUrl('joborder/admin'));
            }
		}
		$this->render('inputJalur',array(
			'model'=>$model,
			));
	}
	public function actionCekDokumen($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Cek Dokumen";
		$model=$this->loadModel($joborder_id);
		$this->render('cekDokumen',array(
			'model'=>$model,
			));
	}
	public function actionUpdateStatus($joborder_id,$status)
	{
		$model=$this->loadModel($joborder_id);
		if($status=="jm61" || $status=="jm62"){
			if($model->status=="jhn2"){
				if($status=="jm61"){
					$model->status="jhn31";
				}else{
					$model->status="jhn32";
				}
			}else{
				$model->status=$status;
			}
		}else{
			$model->status=$status;
		}
		if($model->save()){
			if($status=="pibot" || $status=="ljoc"){
				//notif ke OP
				$n=new Notif;
				$n->dari=Yii::app()->user->id;
				$n->roleDari=Yii::app()->user->getRoleCode();
				if($status=="pibot"){
					$n->roleKe='OP';
				}elseif ($status=="ljoc") {
					$n->roleKe='DIR';
				}
				$n->ke=null;
				$n->row_id=$model->id;
				$n->tableDesc_id=9;
				$n->field='status';
				$n->isSentToRole=1;
				$n->JOStatus=$model->status;
				$n->save();

			}elseif($status=="cdpi0" || $status=="cdpi1" || $status=="jm61" || $status=="jm62" || $status=="ljoc1"){
				//notif ke pelanggan
				$n=new Notif;
				$n->dari=Yii::app()->user->id;
				$n->roleDari=Yii::app()->user->getRoleCode();
				$n->roleKe=null;
				$n->ke=$model->pelanggan_user_id;
				$n->row_id=$model->id;
				$n->tableDesc_id=9;
				$n->field='status';
				$n->isSentToRole=0;
				$n->JOStatus=$model->status;
				$n->save();

				//notif ke pelayaran
				if($status=='cdpi1'){
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->roleKe='PLY';
					$n->ke=null;
					$n->row_id=$model->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=0;
					$n->JOStatus=$model->status;
					$n->save();
				}

				//set tgl selesai
				if($status=="ljoc1"){
					$model->tglSelesai=Date('Y-m-d');
					$model->save();
				}

			}elseif($status=="bjtb1"){
				//update invoice jadi lunas
				$invoice=Invoice::model()->findByAttributes(array('no'=>$model->invoice_no));
				$invoice->isLunas=1;
				if($invoice->save()){

				}
			}elseif($status=="sp2"){
				//update SP2 telah di otorisasi
				$model->sp2=1;
				if($model->save()){
					//notif ke MK
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->roleKe='MK';
					$n->row_id=$model->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus=$model->status;
					$n->save();
				}
			}elseif($status=="man"){
				//update manifes telah diambil
				$model->manifes=1;
				if($model->save()){
					//notif
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->roleKe='MK';
					$n->ke=null;
					$n->row_id=$model->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus=$model->status;
					$n->save();
				}
			}
		}
		$this->redirect(Yii::app()->createUrl('joborder/admin'));
	}
	public function actionGetStatus(){
		$c=new CDbCriteria;
		$c->order='urutan ASC';
		// $c->condition="ric='".Yii::app()->user->getRoleCode()."'";
		$js=Joborderstatus::model()->findAll($c);
        $rows = array();$i=0;
        $ro=array();
        foreach($js as $r){
        	$ro[$i]['value']=$r->kode;
        	$ro[$i]['text']=$r->status;
        	$i++;
        }
        echo CJSON::encode($ro);
		// echo '[{"value":"tPLY","text":"Belum diverifikasi"},{"value":"cdpi1","text":"Terverifikasi"},{"value":"cdpi0","text":"Ditolak"}]';
	}

	public function actionHitungBiaya($joborder_id){
		$this->pageTitle=Yii::app()->name." - Hitung Biaya";
		$joborder=Joborder::model()->findByPk($joborder_id);
		$model=new Biaya;
		$this->render('hitungBiaya',array(
			'joborder'=>$joborder,
			'model'=>$model,
			));
	}
	
	public function actionView($id)
	{
		$this->pageTitle=Yii::app()->name." - View Job Order #".$id;
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate()
	{
		$this->pageTitle=Yii::app()->name." - Buat Pesanan Baru";
		$model=new Createjoborder;
		$transactionSucceed=-1;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Createjoborder']))
		{
			$model->attributes=$_POST['Createjoborder'];
			$transaction=$model->dbConnection->beginTransaction();
			$transactionSucceed=0;

			try{
				$cjo=$model;

				// barang
				$brg=new Barang;
				$brg->nama=$cjo['nama'];
				$brg->des=$cjo['des'];
				$brg->jml=$cjo['jml'];
				$brg->bruto=$cjo['bruto'];
				$brg->netto=$cjo['netto'];
				$brg->volume=$cjo['volume'];
				$brg->save();

				// SI
				$si=new Si;
				$si->noSI=$cjo['noSI'];
				$si->tglTiba=$cjo['tglTiba'];
				$si->tglBrgkt=$cjo['tglBrgkt'];
				// $si->atc=$cjo['si_atc'];
				$si->atc=CUploadedFile::getInstance($cjo,'si_atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($si->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $si->atc->saveAs($dir.$pelengkap.$si->atc);
		           $si->atc=$dir.$pelengkap.$si->atc;
	            }else{
	            	unset($si->atc);
	            }
	            $si->save();

				// BL
				$bl=new Bl;
				$bl->nomor=$cjo['nomor'];
				$bl->tgl=$cjo['bl_tgl'];
				// $bl->atc=$cjo['atc'];
				$bl->atc=CUploadedFile::getInstance($cjo,'atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($bl->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $bl->atc->saveAs($dir.$pelengkap.$bl->atc);
		           $bl->atc=$dir.$pelengkap.$bl->atc;
	            }else{
	            	unset($bl->atc);
	            }
	            $bl->save();

				// CI
				$ci=new Ci;
				$ci->noCI=$cjo['noCI'];
				$ci->tgl=$cjo['ci_tgl'];
				// $ci->atc=$cjo['ci_atc'];
				$ci->atc=CUploadedFile::getInstance($cjo,'ci_atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($ci->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $ci->atc->saveAs($dir.$pelengkap.$ci->atc);
		           $ci->atc=$dir.$pelengkap.$ci->atc;
	            }else{
	            	unset($ci->atc);
	            }
	            $ci->save();

				//ALL JOB ORDER
				$jo=new Joborder;
				$jo->tglMulai=date('Y-m-d');
				$jo->pelanggan_user_id=Yii::app()->user->id;
				$jo->barang_id=$brg->id;
				$jo->layanan_id=$cjo['layanan_id'];
				$jo->bl_id=$bl->id;
				$jo->ci_id=$ci->id;
				$jo->si_id=$si->id;
				$jo->pl=$cjo['pl'];
				// $jo->plAtc=$cjo['plAtc'];
				$jo->plAtc=CUploadedFile::getInstance($cjo,'plAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->plAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->plAtc->saveAs($dir.$pelengkap.$jo->plAtc);
		           $jo->plAtc=$dir.$pelengkap.$jo->plAtc;
	            }else{
	            	unset($jo->plAtc);
	            }

				$jo->sj=$cjo['sj'];
				// $jo->sjAtc=$cjo['sjAtc'];
				$jo->sjAtc=CUploadedFile::getInstance($cjo,'sjAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->sjAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->sjAtc->saveAs($dir.$pelengkap.$jo->sjAtc);
		           $jo->sjAtc=$dir.$pelengkap.$jo->sjAtc;
	            }else{
	            	unset($jo->sjAtc);
	            }

				$jo->sk=$cjo['sk'];
				// $jo->skAtc=$cjo['skAtc'];
				$jo->skAtc=CUploadedFile::getInstance($cjo,'skAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->skAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->skAtc->saveAs($dir.$pelengkap.$jo->skAtc);
		           $jo->skAtc=$dir.$pelengkap.$jo->skAtc;
	            }else{
	            	unset($jo->skAtc);
	            }

				$jo->status='init';

				if ($jo->save()){
					//insert ke notif
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari='PEL';
					$n->roleKe='OP';
					$n->row_id=$jo->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus='init';
					$n->save();

					$transaction->commit();
					$transactionSucceed=1;
				}else{
					$transaction->rollback();
				}
			}catch(Exception $e){
				$transaction->rollback();
    			throw $e;
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'transactionSucceed'=>$transactionSucceed,
		));
	}

	
	public function actionUpdate($id)
	{
		$this->pageTitle=Yii::app()->name." - ";
		$model=Createjoborder::model()->findByPk($id);
		$transactionSucceed=-1;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Createjoborder']))
		{
			$model->attributes=$_POST['Createjoborder'];
			$transaction=$model->dbConnection->beginTransaction();
			$transactionSucceed=0;

			try{
				$cjo=$model;

				// barang
				$brg=Barang::model()->findByPk($model->barang_id);
				$brg->nama=$cjo['nama'];
				$brg->des=$cjo['des'];
				$brg->jml=$cjo['jml'];
				$brg->bruto=$cjo['bruto'];
				$brg->netto=$cjo['netto'];
				$brg->volume=$cjo['volume'];
				$brg->save();

				// SI
				$si=Si::model()->findByPk($model->si_id);
				$si->noSI=$cjo['noSI'];
				$si->tglTiba=$cjo['tglTiba'];
				$si->tglBrgkt=$cjo['tglBrgkt'];
				// $si->atc=$cjo['si_atc'];
				$si->atc=CUploadedFile::getInstance($cjo,'si_atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($si->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $si->atc->saveAs($dir.$pelengkap.$si->atc);
		           $si->atc=$dir.$pelengkap.$si->atc;
	            }else{
	            	unset($si->atc);
	            }
	            $si->save();

				// BL
				$bl=Bl::model()->findByPk($model->bl_id);
				$bl->nomor=$cjo['nomor'];
				$bl->tgl=$cjo['bl_tgl'];
				// $bl->atc=$cjo['atc'];
				$bl->atc=CUploadedFile::getInstance($cjo,'atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($bl->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $bl->atc->saveAs($dir.$pelengkap.$bl->atc);
		           $bl->atc=$dir.$pelengkap.$bl->atc;
	            }else{
	            	unset($bl->atc);
	            }
	            $bl->save();

				// CI
				$ci=Ci::model()->findByPk($model->ci_id);
				$ci->noCI=$cjo['noCI'];
				$ci->tgl=$cjo['ci_tgl'];
				// $ci->atc=$cjo['ci_atc'];
				$ci->atc=CUploadedFile::getInstance($cjo,'ci_atc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($ci->atc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $ci->atc->saveAs($dir.$pelengkap.$ci->atc);
		           $ci->atc=$dir.$pelengkap.$ci->atc;
	            }else{
	            	unset($ci->atc);
	            }
	            $ci->save();

				//ALL JOB ORDER
				$jo=Joborder::model()->findByPk($model->id);
				$jo->tglMulai=date('Y-m-d');
				$jo->pelanggan_user_id=Yii::app()->user->id;
				$jo->barang_id=$brg->id;
				$jo->layanan_id=$cjo['layanan_id'];
				$jo->bl_id=$bl->id;
				$jo->ci_id=$ci->id;
				$jo->si_id=$si->id;
				$jo->pl=$cjo['pl'];
				// $jo->plAtc=$cjo['plAtc'];
				$jo->plAtc=CUploadedFile::getInstance($cjo,'plAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->plAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->plAtc->saveAs($dir.$pelengkap.$jo->plAtc);
		           $jo->plAtc=$dir.$pelengkap.$jo->plAtc;
	            }else{
	            	unset($jo->plAtc);
	            }

				$jo->sj=$cjo['sj'];
				// $jo->sjAtc=$cjo['sjAtc'];
				$jo->sjAtc=CUploadedFile::getInstance($cjo,'sjAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->sjAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->sjAtc->saveAs($dir.$pelengkap.$jo->sjAtc);
		           $jo->sjAtc=$dir.$pelengkap.$jo->sjAtc;
	            }else{
	            	unset($jo->sjAtc);
	            }

				$jo->sk=$cjo['sk'];
				// $jo->skAtc=$cjo['skAtc'];
				$jo->skAtc=CUploadedFile::getInstance($cjo,'skAtc');
		        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
		        $pelengkap=date('Y-m-d H.i.s').'-';
				if (strlen($jo->skAtc)>0){
		            if (!file_exists($dir)){
		            	@mkdir($dir);
		            }
		           $jo->skAtc->saveAs($dir.$pelengkap.$jo->skAtc);
		           $jo->skAtc=$dir.$pelengkap.$jo->skAtc;
	            }else{
	            	unset($jo->skAtc);
	            }

				$jo->status='cdpi3';

				if ($jo->save()){
					//insert ke notif
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->roleKe='OP';
					$n->row_id=$jo->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus='cdpi3';
					$n->save();

					$transaction->commit();
					$transactionSucceed=1;
				}else{
					$transaction->rollback();
				}
			}catch(Exception $e){
				$transaction->rollback();
    			throw $e;
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'transactionSucceed'=>$transactionSucceed,
		));
	}

	public function actionInputNotul($id)
	{
		$this->pageTitle=Yii::app()->name." - Input Notul";
		$model=$this->loadModel($id);
		if (isset($_POST['Joborder'])){
			$model->attributes=$_POST['Joborder'];	
			$model->notulAtc=CUploadedFile::getInstance($model,'notulAtc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->notulAtc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->notulAtc->saveAs($dir.$pelengkap.$model->notulAtc);
	           $model->notulAtc=$dir.$pelengkap.$model->notulAtc;
            }else{
            	unset($model->notulAtc);
            }
            $model->status='jm3';
            if($model->save()){
            	$n=new Notif;
				$n->dari=Yii::app()->user->id;
				$n->roleDari=Yii::app()->user->getRoleCode();
				$n->ke=$model->pelanggan_user_id;
				$n->row_id=$model->id;
				$n->tableDesc_id=9;
				$n->field='status';
				$n->isSentToRole=0;
				$n->JOStatus=$model->status;
				$n->save();

            	$this->redirect(Yii::app()->createUrl('joborder/admin'));
            }
		}
		$this->render('inputNotul',array(
			'model'=>$model,
			));
	}

	public function actionUpdateAjax()
	{
	    //get job offer
	    $jo=Joborder::model()->findByPk($_POST['pk']);
	    if ($_POST['name']=='tglPelayaran'){
	    	if ($jo->status=='cdpi1'){
	    		$jo->status='tPLY';
	    		if($jo->save()){
	    			//insert ke notif ke pelanggan
					// $n=new Notif;
					// $n->dari=Yii::app()->user->id;
					// $n->roleDari=Yii::app()->user->getRoleCode();
					// $n->ke=$jo->pelanggan_user_id;
					// $n->row_id=$jo->id;
					// $n->tableDesc_id=9;
					// $n->field='status';
					// $n->isSentToRole=0;
					// $n->JOStatus='tPLY';
					// $n->save();

					//insert ke notif ke operator
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->roleKe='OP';
					$n->row_id=$jo->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus='tPLY';
					$n->save();
	    		}
	    	}	
	    }
	    $es = new EditableSaver('Joborder'); 
	    $es->update();
	}


	
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	
	public function actionIndex()
	{
		$this->pageTitle=Yii::app()->name." - ";
		$model=new Joborder('search');
		$model->unsetAttributes();  // clear any default values
		if (Yii::app()->user->isPEL()){
			$model->pelanggan_user_id=Yii::app()->user->id;
		}
		if(isset($_GET['Joborder']))
			$model->attributes=$_GET['Joborder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	
	public function actionAdmin()
	{
		$this->pageTitle=Yii::app()->name." - On Going Job Order";
		$model=new Joborder('search');
		$model->unsetAttributes();  // clear any default values
		$model->status="notCompleted";
		if (Yii::app()->user->isPEL()){
			$model->pelanggan_user_id=Yii::app()->user->id;
		}
		if(isset($_GET['Joborder']))
			$model->attributes=$_GET['Joborder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionHistory()
	{
		$this->pageTitle=Yii::app()->name." - History Job Order";
		$model=new Joborder('search');
		$model->unsetAttributes();  // clear any default values
		$model->status="ljoc1";
		if (Yii::app()->user->isPEL()){
			$model->pelanggan_user_id=Yii::app()->user->id;
		}
		if(isset($_GET['Joborder']))
			$model->attributes=$_GET['Joborder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	
	public function loadModel($id)
	{
		$model=Joborder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='joborder-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
