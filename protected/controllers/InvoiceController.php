<?php

class InvoiceController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','admin','delete','print'),
				'expression'=>'Yii::app()->user->isMK()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','print'),
				'expression'=>'Yii::app()->user->isPEL()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionPrint($no)
	{
		$this->layout='print';
		$this->render('print',array(
			'model'=>$this->loadModel($no),
			'joborder'=>Joborder::model()->findByAttributes(array("invoice_no"=>$no)),
		));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($no,$joborder_id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($no),
			'joborder'=>Joborder::model()->findByPk($joborder_id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Buat Invoice";

		$model=new Invoice;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			$model->mk_id=Yii::app()->user->id;
			$jo=Joborder::model()->findByPk($joborder_id);
			$jo->invoice_no=null;
			$jo->save();
			
			Invoice::model()->deleteAllByAttributes(array('no'=>$model->no));

			if($model->save()){
				$jo->status='inv';
				$jo->invoice_no=$model->no;
				if($jo->save()){
					//notif ke pelanggan
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->ke=$jo->pelanggan_user_id;
					$n->row_id=$jo->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=0;
					$n->JOStatus=$jo->status;
					$n->save();
				}
				$this->redirect(Yii::app()->createUrl('joborder/admin'));
			}
		}

		$jo=Joborder::model()->findByPk($joborder_id);
		$bp=$jo->biayaPPJK;
		//hitung biaya PPJK utk invoice
		if($bp->jenisImpor=="1"){
			if ($bp->nilaiInvoice < 66666666 ){
				$model->jasaImpor=1000000;	
			}else {
				$model->jasaImpor=0.015*$bp->nilaiInvoice;	
			}
		}else{
			if ($bp->nilaiInvoice < 100000000 ){
				$model->jasaImpor=2000000;	
			}else {
				$model->jasaImpor=0.02*$bp->nilaiInvoice;	
			}
		}

		$model->jasaPPJK=(($bp->jmlKontainer-1)*200000)+600000;

		if ($bp->jalur=="1" || $bp->jalur=="2"){
			$model->biayaDok=600000+(($bp->jmlDok-1)*1500000);
		}else{
			$model->biayaDok=2000000+(($bp->jmlDok-1)*1500000);
		}
		$biaya_edi=175000*$bp->jmlDok;
		$biaya_lampiran=50000*$bp->jmlLampiranPIB;
		$model->biayaLain=$biaya_edi+$biaya_lampiran;

		$model->total1=$model->jasaImpor + $model->jasaPPJK + $model->biayaDok + $model->biayaLain;
		$model->ppnTotal1=ceil($model->total1/10);

		$model->pdri=$jo->biaya->pdri;
		$model->beaMasuk=$jo->biaya->bm_rp;
		$model->total2=$model->pdri+$model->beaMasuk;

		$model->grandtotal=$model->total1+$model->total2;
		$this->render('create',array(
			'model'=>$model,
			'joborder'=>$jo,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->no));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Invoice');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invoice('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invoice']))
			$model->attributes=$_GET['Invoice'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($no)
	{
		$model=Invoice::model()->findByAttributes(array('no'=>$no));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invoice-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
