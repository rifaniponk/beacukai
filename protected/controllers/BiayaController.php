<?php

class BiayaController extends Controller
{
	
	public $layout='//layouts/all';

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'expression'=>'Yii::app()->user->isOP()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


	public function actionCreate($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Input Biaya";
		$model=new Biaya;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Biaya']))
		{
			$model->attributes=$_POST['Biaya'];
			$model->docAtc=CUploadedFile::getInstance($model,'docAtc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->docAtc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->docAtc->saveAs($dir.$pelengkap.$model->docAtc);
	           $model->docAtc=$dir.$pelengkap.$model->docAtc;
            }else{
            	unset($model->docAtc);
            }

			if($model->save()){
				$jo=Joborder::model()->findByPk($joborder_id);
				$jo->biaya_id=$model->id;
				$jo->status='byop';
				$jo->save();
				$this->redirect(Yii::app()->createUrl('joborder/admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id,$joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - ";
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Biaya']))
		{
			$model->attributes=$_POST['Biaya'];
			$model->docAtc=CUploadedFile::getInstance($model,'docAtc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->docAtc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->docAtc->saveAs($dir.$pelengkap.$model->docAtc);
	           $model->docAtc=$dir.$pelengkap.$model->docAtc;
            }else{
            	unset($model->docAtc);
            }

			if($model->save()){
				$jo=Joborder::model()->findByPk($joborder_id);
				$jo->biaya_id=$model->id;
				// if ($jo->status=='jm'){
				// 	$jo->status='jm1';
				// }elseif ($jo->status=='jm22'){
				// 	$jo->status='jm23';
				// }
				$jo->status='jm21';
				$jo->save();
				$this->redirect(Yii::app()->createUrl('joborder/admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex()
	{
		$this->pageTitle=Yii::app()->name." - ";

		$dataProvider=new CActiveDataProvider('Biaya');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionAdmin()
	{
		$this->pageTitle=Yii::app()->name." - ";
		
		$model=new Biaya('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Biaya']))
			$model->attributes=$_GET['Biaya'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function loadModel($id)
	{
		$model=Biaya::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='biaya-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
