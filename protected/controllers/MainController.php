<?php

class MainController extends Controller
{
	public function init(){
		$this->layout='all';
	}
	public function actionIndex(){
		$model=array();
		$this->layout='front';
		$this->render('index',array('model'=>$model));
	}
	public function actionSavethemes($color){
		$File = "themes.txt";
		unlink($File);
		$dataString = $color;
		$fWrite = fopen($File,"a");
		$wrote = fwrite($fWrite, $dataString);
		fclose($fWrite);
	}
	public function actionLoadthemes(){
		$filename="themes.txt";
		$file = fopen($filename, "r");
		$f=fread($file,filesize($filename));
		fclose($file);
		echo $f;
	}
	public function actionSavecollapse($id){
		$File = "collapse.txt";
		unlink($File);
		$dataString = $id;
		$fWrite = fopen($File,"a");
		$wrote = fwrite($fWrite, $dataString);
		fclose($fWrite);
	}
	public function actionLoadcollapse(){
		$filename="collapse.txt";
		$file = fopen($filename, "r");
		$f=fread($file,filesize($filename));
		fclose($file);
		echo $f;
	}
	public function actionTentang()
	{
		$this->layout='front';
		$this->render('tentang');
	}
	public function actionKontak(){
		$this->layout='front';
		$this->render('kontak');
	}
	public function actionLogin(){
		$model=new LoginForm;
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				echo Yii::app()->session['roles'];
			}else{
				echo "gagal";
			}
		}else{
			if (Yii::app()->user->isGuest){
				$this->redirect('../../');
			}else{
				$this->redirect(Yii::app()->createUrl('main/index'));
			}
			$this->layout='blank';
			$this->render('login');
		}
	}
	public function actionReadnotif(){
		$c=new CDbCriteria;
		Approval::model()->updateAll(array('is_read'=>1));
	}
	public function actionReloadcurrenttime(){
		echo dateHelper::tgl_indo(date('Y-m-d'),true).' / '.date('H:i');
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
		// $this->redirect(Yii::app()->homeUrl);
		Yii::app()->session->clear();
		Yii::app()->session->destroy();
		$this->redirect($this->base_url());
	}
	public function actionRegister(){
		$model=new Pelangganv;
		$save_success=-1;

		if (isset($_POST['Pelangganv'])){
			$save_success=0;
			$model->attributes=$_POST['Pelangganv'];
			if ($model->validate()){
				//save ke user
				$user=new User;
				$user->username=strtolower($model->username);
				$user->password=Yii::app()->hash->pass->make($model->password);
				$user->nama=$model->namauser;
				$user->role=$model->roleUser;
				if ($user->save()){
					//save ke pelanggan
					$pel=new Pelanggan;
					$pel->user_id=$user->id;
					$pel->ktp=$model->ktp;
					$pel->nama=$user->nama;
					$pel->role=$model->rolePelanggan;
					$pel->email=$model->email;
					$pel->alamat=$model->alamat;
					$pel->npwp=$model->npwp;
					$pel->noapi=$model->noapi;
					$pel->npik=$model->npik;
					$pel->telp=$model->telp;
					$pel->web=$model->web;
					$pel->isVerified=$model->isVerified;
					$pel->siupAtc=CUploadedFile::getInstance($model,'siupAtc');
					$pel->npwpAtc=CUploadedFile::getInstance($model,'npwpAtc');
					$pel->ktpAtc=CUploadedFile::getInstance($model,'ktpAtc');

			        $dir='uploaded/'.$user->username."/";
			        $pelengkap=date('Y-m-d H.i.s').'-';
					if (strlen($pel->siupAtc)>0){
			            if (!file_exists($dir)){
			            	@mkdir($dir);
			            }
			            $pel->siupAtc->saveAs($dir.$pelengkap.$pel->siupAtc);
			            $pel->siupAtc=$dir.$pelengkap.$pel->siupAtc;
		            }else{
		            	unset($pel->$pel->siupAtc);
		            }
		            if (strlen($pel->npwpAtc)>0){
			            if (!file_exists($dir)){
			            	@mkdir($dir);
			            }
			            $pel->npwpAtc->saveAs($dir.$pelengkap.$pel->npwpAtc);
			            $pel->npwpAtc=$dir.$pelengkap.$pel->npwpAtc;
		            }else{
		            	unset($pel->$pel->npwpAtc);
		            }
		            if (strlen($pel->ktpAtc)>0){
			            if (!file_exists($dir)){
			            	@mkdir($dir);
			            }
			            $pel->ktpAtc->saveAs($dir.$pelengkap.$pel->ktpAtc);
			            $pel->ktpAtc=$dir.$pelengkap.$pel->ktpAtc;
		            }else{
		            	unset($pel->$pel->siupAtc);
		            }
					if ($pel->save()){
						$save_success=1;
					}else{
						$user->delete();
						print_r($pel->getErrors());die();
					}
				}
			}
		}
		$this->layout='front';
		$this->render('register',array('model'=>$model,'save_success'=>$save_success));
	}

	public function actionReadAllNotif()
	{
		$c=new CDbCriteria;
		$c->condition="roleKe='".Yii::app()->user->getRoleCode()."' OR ke=".Yii::app()->user->id;

		$n=Notif::model()->updateAll(array('isRead'=>1),$c);
	}

	public function actionInfoStatus(){
		$this->layout='all';
		$this->render('InfoStatus');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}