<?php

class PembayaranInvoiceController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','admin','delete'),
				'expression'=>'Yii::app()->user->isPEL()',
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'expression'=>'Yii::app()->user->isMK()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($invoice_no)
	{
		$invoice=Invoice::model()->findByAttributes(array('no'=>$invoice_no));
		$this->render('view',array(
			'model'=>$invoice->pembayaranInvoice,
			'invoice'=>$invoice
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($joborder_id)
	{
		$this->pageTitle=Yii::app()->name." - Pembayaran Invoice";

		$model=new PembayaranInvoice;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PembayaranInvoice']))
		{
			$model->attributes=$_POST['PembayaranInvoice'];
			$model->atc=CUploadedFile::getInstance($model,'atc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->atc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->atc->saveAs($dir.$pelengkap.$model->atc);
	           $model->atc=$dir.$pelengkap.$model->atc;
            }else{
            	unset($model->atc);
            }
			if($model->save()){
				$jo=Joborder::model()->findByPk($joborder_id);
				
				$in=Invoice::model()->findByAttributes(array('no'=>$jo->invoice_no));
				$in->pembayaranInvoice_id=$model->id;
				if ($in->save()){
					$jo->status='bjtb';
					if($jo->save()){
						//notif ke MK
						$n=new Notif;
						$n->dari=Yii::app()->user->id;
						$n->roleDari=Yii::app()->user->getRoleCode();
						$n->roleKe='MK';
						$n->row_id=$jo->id;
						$n->tableDesc_id=9;
						$n->field='status';
						$n->isSentToRole=1;
						$n->JOStatus=$jo->status;
						$n->save();
					}
				}
				$this->redirect(Yii::app()->createUrl('joborder/admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PembayaranInvoice']))
		{
			$model->attributes=$_POST['PembayaranInvoice'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PembayaranInvoice');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PembayaranInvoice('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PembayaranInvoice']))
			$model->attributes=$_GET['PembayaranInvoice'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PembayaranInvoice::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pembayaran-invoice-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
