<?php

class BuktibayarController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	public function accessRules()
	{
		return array(

			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionView($id)
	{
		$this->pageTitle=Yii::app()->name." - ";
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate($joborder_id,$jenis)
	{
		$this->pageTitle=Yii::app()->name." - Upload Bukti Bayar";
		$model=new Buktibayar;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buktibayar']))
		{
			$model->attributes=$_POST['Buktibayar'];
			$model->jenis=$jenis;
			$model->atc=CUploadedFile::getInstance($model,'atc');
	        $dir='uploaded/'.sysHelper::getUsername(Yii::app()->user->id)."/";
	        $pelengkap=date('Y-m-d H.i.s').'-';
			if (strlen($model->atc)>0){
	            if (!file_exists($dir)){
	            	@mkdir($dir);
	            }
	           $model->atc->saveAs($dir.$pelengkap.$model->atc);
	           $model->atc=$dir.$pelengkap.$model->atc;
            }else{
            	unset($model->atc);
            }
			if($model->save()){
				$jo=Joborder::model()->findByPk($joborder_id);
				if ($jenis=="PIB"){
					$jo->status='bukba';
				}elseif ($jenis=="Notul") {
					if ($jo->status=="pibot"){
						$jo->status='jhn1';
					}elseif ($jo->status=="jm3"){
						$jo->status='jm4';
					}
				}
				if($jo->save()){
					$n=new Notif;
					$n->dari=Yii::app()->user->id;
					$n->roleDari=Yii::app()->user->getRoleCode();
					$n->row_id=$model->id;
					$n->tableDesc_id=9;
					$n->field='status';
					$n->isSentToRole=1;
					$n->JOStatus=$jo->status;
					$n->roleKe='OP';
					$n->save();
				}
				$this->redirect(Yii::app()->createUrl('joborder/admin'));
			}
		}

		$model->JobOrder_id=$joborder_id;
		$this->render('create',array(
			'model'=>$model,
			'joborder_id'=>$joborder_id
		));
	}

	public function actionUpdate($id)
	{
		$this->pageTitle=Yii::app()->name." - ";
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buktibayar']))
		{
			$model->attributes=$_POST['Buktibayar'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}


	public function actionIndex()
	{
		$this->pageTitle=Yii::app()->name." - ";
		$dataProvider=new CActiveDataProvider('Buktibayar');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}


	public function actionAdmin()
	{
		$this->pageTitle=Yii::app()->name." - ";
		$model=new Buktibayar('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Buktibayar']))
			$model->attributes=$_GET['Buktibayar'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	public function loadModel($id)
	{

		$model=Buktibayar::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='buktibayar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
