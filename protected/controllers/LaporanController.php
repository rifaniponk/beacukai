<?php

class LaporanController extends Controller
{
	public function init()
	{
		if(!Yii::app()->user->isDIR() && !Yii::app()->user->isMK()){
			$this->redirect(Yii::app()->createUrl('joborder/admin'));
		}
	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionArusKas()
	{
		$this->render('arusKas');
	}
	public function actionPrintArusKas()
	{
		$this->layout='print';
		$this->render('print');
	}
	public function actionJoborder($month1=null,$month2=null,$year1=null,$year2=null,$print=false)
	{
		$year1==null ? $year1=date('Y') : $year1=$year1; 
		$year2==null ? $year2=date('Y') : $year2=$year2;
		if($month1==null){
			$month1=date('m');
		}
		if($month2==null){
			$month2=date('m');
		}
		if(strtotime($month1)>strtotime($month2)){
			$a=$month1;
			$month1=$month2;
			$month2=$a;
		}
		$month1=dateHelper::convertMonthSeq($month1);
		$month2=dateHelper::convertMonthSeq($month2);
		$model=new LaporanJO('search'); 
		$model->setDate1($year1.'-'.$month1.'-01');
		$model->setDate2($year2.'-'.$month2.'-31');

		if($print){
			$this->layout='print';
			$this->printLandscape=1;
			$this->render('joborder',array(
				'printMode'=>true,
				'model'=>$model,
				'month1'=>$month1,
				'month2'=>$month2,
				'year1'=>$year1,
				'year2'=>$year2
				));
		}else{
			$this->render('joborder',array(
				'printMode'=>false,
				'model'=>$model,
				'month1'=>$month1,
				'month2'=>$month2,
				'year1'=>$year1,
				'year2'=>$year2
				));
		}
	}


}