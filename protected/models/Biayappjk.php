<?php

/**
 * This is the model class for table "biayappjk".
 *
 * The followings are the available columns in table 'biayappjk':
 * @property integer $id
 * @property integer $jenisImpor
 * @property double $nilaiInvoice
 * @property integer $jmlKontainer
 * @property integer $jalur
 * @property integer $jmlDok
 * @property integer $jmlLampiranPIB
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 */
class Biayappjk extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Biayappjk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'biayappjk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jenisImpor, nilaiInvoice, jmlKontainer, jalur, jmlDok, jmlLampiranPIB', 'required'),
			array('jenisImpor, jmlKontainer, jalur, jmlDok, jmlLampiranPIB', 'numerical', 'integerOnly'=>true),
			array('nilaiInvoice', 'numerical'),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jenisImpor, nilaiInvoice, jmlKontainer, jalur, jmlDok, jmlLampiranPIB, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'biayaPPJK_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenisImpor' => 'Jenis Impor',
			'nilaiInvoice' => 'Nilai Invoice',
			'jmlKontainer' => 'Jml Kontainer',
			'jalur' => 'Jalur',
			'jmlDok' => 'Jml Dok',
			'jmlLampiranPIB' => 'Jml Lampiran Pib',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenisImpor',$this->jenisImpor);
		$criteria->compare('nilaiInvoice',$this->nilaiInvoice);
		$criteria->compare('jmlKontainer',$this->jmlKontainer);
		$criteria->compare('jalur',$this->jalur);
		$criteria->compare('jmlDok',$this->jmlDok);
		$criteria->compare('jmlLampiranPIB',$this->jmlLampiranPIB);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}