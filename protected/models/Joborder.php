<?php

/**
 * This is the model class for table "joborder".
 *
 * The followings are the available columns in table 'joborder':
 * @property integer $id
 * @property string $tglMulai
 * @property integer $pelanggan_user_id
 * @property integer $barang_id
 * @property integer $layanan_id
 * @property integer $bl_id
 * @property integer $ci_id
 * @property integer $si_id
 * @property string $pl
 * @property string $plAtc
 * @property string $sj
 * @property string $sjAtc
 * @property string $sk
 * @property string $skAtc
 * @property string $status
 * @property integer $pelayaran_user_id
 * @property string $tglPelayaran
 * @property integer $biaya_id
 * @property integer $manifes
 * @property integer $pib_id
 * @property string $jalurAtc
 * @property integer $sp2
 * @property integer $notulAngka
 * @property string $notulAtc
 * @property string $tglSelesai
 * @property string $invoice_kode
 * @property string $docAtc
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Buktibayar[] $buktibayars
 * @property Pib $pib
 * @property Joborderstatus $status0
 * @property Barang $barang
 * @property Biaya $biaya
 * @property Bl $bl
 * @property Ci $ci
 * @property Invoice $invoiceKode
 * @property Layanan $layanan
 * @property Pelanggan $pelangganUser
 * @property Pelayaran $pelayaranUser
 * @property Si $si
 */
class Joborder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Joborder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'joborder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pelanggan_user_id, barang_id, layanan_id, bl_id, ci_id, si_id, pl, plAtc, sj, sjAtc, sk, skAtc, status', 'required'),
			array('pelanggan_user_id, barang_id, layanan_id, bl_id, ci_id, si_id, pelayaran_user_id, biaya_id, manifes, pib_id, sp2, notulAngka', 'numerical', 'integerOnly'=>true),
			array('pl, sj, sk', 'length', 'max'=>45),
			array('plAtc, sjAtc, skAtc, notulAtc, docAtc, jalurAtc, sppbAtc', 'length', 'max'=>245),
			array('status', 'length', 'max'=>5),
			array('jalur', 'length', 'max'=>18),
			array('tglMulai, tglPelayaran, tglSelesai, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tglMulai, pelanggan_user_id, barang_id, layanan_id, bl_id, ci_id, si_id, pl, plAtc, sj, sjAtc, sk, skAtc, status, pelayaran_user_id, tglPelayaran, biaya_id, manifes, pib_id, jalurAtc, sp2, notulAngka, notulAtc, tglSelesai, invoice_kode, docAtc, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'buktibayars' => array(self::HAS_MANY, 'Buktibayar', 'JobOrder_id'),
			'pib' => array(self::BELONGS_TO, 'Pib', 'pib_id'),
			'joborderstatus' => array(self::BELONGS_TO, 'Joborderstatus', 'status'),
			'barang' => array(self::BELONGS_TO, 'Barang', 'barang_id'),
			'biaya' => array(self::BELONGS_TO, 'Biaya', 'biaya_id'),
			'bl' => array(self::BELONGS_TO, 'Bl', 'bl_id'),
			'ci' => array(self::BELONGS_TO, 'Ci', 'ci_id'),
			'layanan' => array(self::BELONGS_TO, 'Layanan', 'layanan_id'),
			'pelangganUser' => array(self::BELONGS_TO, 'Pelanggan', 'pelanggan_user_id'),
			'pelayaranUser' => array(self::BELONGS_TO, 'Pelayaran', 'pelayaran_user_id'),
			'si' => array(self::BELONGS_TO, 'Si', 'si_id'),
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_no'),
            'biayaPPJK' => array(self::BELONGS_TO, 'Biayappjk', 'biayaPPJK_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Pesanan ID',
			'tglMulai' => 'Tgl Mulai',
			'pelanggan_user_id' => 'Pelanggan User',
			'barang_id' => 'Barang',
			'layanan_id' => 'Layanan',
			'bl_id' => 'BL ID',
			'ci_id' => 'CI ID',
			'si_id' => 'SI ID',
			'pl' => 'No PL',
			'plAtc' => 'Lampiran PL',
			'sj' => 'No SJ',
			'sjAtc' => 'Lampiran SJ',
			'sk' => 'No SK',
			'skAtc' => 'Lampiran SK',
			'status' => 'Status',
			'pelayaran_user_id' => 'Pelayaran',
			'tglPelayaran' => 'Tgl Pelayaran',
			'biaya_id' => 'Biaya',
			'manifes' => 'Manifes',
			'pib_id' => 'PIB ID',
			'jalur' => 'Jalur',
			'jalurAtc' => 'Surat Pemberitahuan Jalur',
			'sp2' => 'Sp2',
			'notulAngka' => 'Angka Notul',
			'notulAtc' => 'Lampiran Notul',
			'sppbAtc' => 'Lampiran SPPB',
			'tglSelesai' => 'Tgl Selesai',
			'docAtc' => 'Doc Atc',
			'biayaPPJK_id' => 'Biaya Ppjk',
            'invoice_no' => 'Invoice No',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tglMulai',$this->tglMulai,true);
		$criteria->compare('pelanggan_user_id',$this->pelanggan_user_id);
		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('layanan_id',$this->layanan_id);
		$criteria->compare('bl_id',$this->bl_id);
		$criteria->compare('ci_id',$this->ci_id);
		$criteria->compare('si_id',$this->si_id);
		$criteria->compare('pl',$this->pl,true);
		$criteria->compare('plAtc',$this->plAtc,true);
		$criteria->compare('sj',$this->sj,true);
		$criteria->compare('sjAtc',$this->sjAtc,true);
		$criteria->compare('sk',$this->sk,true);
		$criteria->compare('skAtc',$this->skAtc,true);

		if($this->status=='notCompleted'){
			$criteria->addCondition("status <> 'ljoc1'");
		}else{
			$criteria->compare('status',$this->status,false);
		}
		$criteria->compare('pelayaran_user_id',$this->pelayaran_user_id);
		$criteria->compare('tglPelayaran',$this->tglPelayaran,true);
		$criteria->compare('biaya_id',$this->biaya_id);
		$criteria->compare('manifes',$this->manifes);
		$criteria->compare('pib_id',$this->pib_id);
		$criteria->compare('jalur',$this->jalur,true);
		$criteria->compare('jalurAtc',$this->jalurAtc,true);
		$criteria->compare('sp2',$this->sp2);
		$criteria->compare('notulAngka',$this->notulAngka);
		$criteria->compare('notulAtc',$this->notulAtc,true);
		$criteria->compare('tglSelesai',$this->tglSelesai,true);
		$criteria->compare('docAtc',$this->docAtc,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
		        'defaultOrder'=>'t.ctime DESC',
		    ),
			'pagination'=>array(
		                            'pageSize'=>20,
		                        ),
		));
	}
}