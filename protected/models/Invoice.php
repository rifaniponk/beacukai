<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property string $no
 * @property string $tgl
 * @property double $jasaImpor
 * @property double $jasaPPJK
 * @property double $biayaDok
 * @property double $biayaLain
 * @property double $total1
 * @property double $ppnTotal1
 * @property double $pdri
 * @property double $beaMasuk
 * @property double $biayaBeacukai
 * @property double $total2
 * @property double $grandtotal
 * @property integer $pembayaranInvoice_id
 * @property integer $mk_id
 * @property integer $isLunas
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Pembayaraninvoice $pembayaranInvoice
 * @property User $mk
 * @property Joborder[] $joborders
 */
class Invoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no, tgl, jasaImpor, jasaPPJK, biayaDok, total1, ppnTotal1, pdri, beaMasuk, total2, grandtotal, mk_id', 'required'),
			array('pembayaranInvoice_id,  mk_id, isLunas', 'numerical', 'integerOnly'=>true),
			array('jasaImpor, jasaPPJK, biayaDok, biayaLain, total1, ppnTotal1, pdri, beaMasuk, biayaBeacukai, total2, grandtotal', 'numerical'),
			array('no', 'length', 'max'=>50),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('no, tgl, jasaImpor, jasaPPJK, biayaDok, biayaLain, total1, ppnTotal1, pdri, beaMasuk, biayaBeacukai, total2, grandtotal, pembayaranInvoice_id, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mk' => array(self::BELONGS_TO, 'User', 'mk_id'),
			'pembayaranInvoice' => array(self::BELONGS_TO, 'Pembayaraninvoice', 'pembayaranInvoice_id'),
			'joborders' => array(self::HAS_MANY, 'Joborder', 'invoice_no'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no' => 'No',
			'tgl' => 'Tgl',
			'jasaImpor' => 'Jasa Impor',
			'jasaPPJK' => 'Jasa PPJK',
			'biayaDok' => 'Biaya Dokumen',
			'biayaLain' => 'Biaya Lain',
			'total1' => 'Total Biaya PPJK',
			'ppnTotal1' => 'PPN 10% (dari biaya PPJK)',
			'pdri' => 'Pajak dalam Rangka Impor',
			'beaMasuk' => 'Bea Masuk',
			'biayaBeacukai' => 'Biaya Beacukai',
			'total2' => 'Total Biaya Beacukai',
			'grandtotal' => 'GRANDTOTAL',
			'pembayaranInvoice_id' => 'Pembayaran Invoice',
			'mk_id'=>'Pembuat Invoice',
			'ctime' => 'Created Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('no',$this->no,true);
		$criteria->compare('tgl',$this->tgl,true);
		$criteria->compare('jasaImpor',$this->jasaImpor);
		$criteria->compare('jasaPPJK',$this->jasaPPJK);
		$criteria->compare('biayaDok',$this->biayaDok);
		$criteria->compare('biayaLain',$this->biayaLain);
		$criteria->compare('total1',$this->total1);
		$criteria->compare('ppnTotal1',$this->ppnTotal1);
		$criteria->compare('pdri',$this->pdri);
		$criteria->compare('beaMasuk',$this->beaMasuk);
		$criteria->compare('biayaBeacukai',$this->biayaBeacukai);
		$criteria->compare('total2',$this->total2);
		$criteria->compare('grandtotal',$this->grandtotal);
		$criteria->compare('pembayaranInvoice_id',$this->pembayaranInvoice_id);
		$criteria->compare('mk_id',$this->mk_id);
		$criteria->compare('isLunas',$this->isLunas);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}