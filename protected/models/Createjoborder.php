<?php

/**
 * This is the model class for table "createjoborder".
 *
 * The followings are the available columns in table 'createjoborder':
 * @property integer $barang_id
 * @property integer $layanan_id
 * @property integer $bl_id
 * @property integer $ci_id
 * @property integer $si_id
 * @property string $pl
 * @property string $plAtc
 * @property string $sj
 * @property string $sjAtc
 * @property string $sk
 * @property string $skAtc
 * @property string $noSI
 * @property string $tglTiba
 * @property string $tglBrgkt
 * @property string $si_atc
 * @property string $noCI
 * @property string $ci_tgl
 * @property string $ci_atc
 * @property string $nomor
 * @property string $bl_tgl
 * @property string $atc
 * @property string $nama
 * @property string $des
 * @property integer $jml
 * @property integer $bruto
 * @property integer $netto
 * @property integer $volume
 */
class Createjoborder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Createjoborder the static model class
	 */
	public function primaryKey(){
		return 'id';
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'createjoborder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barang_id, layanan_id, bl_id, ci_id, si_id, pl, plAtc, sj, sjAtc, sk, skAtc, noSI, tglTiba, tglBrgkt, si_atc, noCI, ci_tgl, ci_atc, nomor, bl_tgl, atc, nama, jml, bruto, netto', 'required'),
			array('barang_id, layanan_id, bl_id, ci_id, si_id, jml, bruto, netto, volume', 'numerical', 'integerOnly'=>true),
			array('pl, sj, sk, noCI', 'length', 'max'=>45),
			array('plAtc, sjAtc, skAtc, si_atc, ci_atc, atc', 'length', 'max'=>245),
			array('noSI, nomor', 'length', 'max'=>30),
			array('nama', 'length', 'max'=>145),
			array('des', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('barang_id, layanan_id, bl_id, ci_id, si_id, pl, plAtc, sj, sjAtc, sk, skAtc, noSI, tglTiba, tglBrgkt, si_atc, noCI, ci_tgl, ci_atc, nomor, bl_tgl, atc, nama, des, jml, bruto, netto, volume', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'barang_id' => 'Barang',
			'layanan_id' => 'Layanan',
			'bl_id' => 'ID BL',
			'ci_id' => 'ID CI',
			'si_id' => 'ID SI',
			'pl' => 'No Packing List',
			'plAtc' => 'Lampiran Packing List',
			'sj' => 'No Surat Jalan',
			'sjAtc' => 'Lampiran Surat Jalan',
			'sk' => 'No Surat Kuasa',
			'skAtc' => 'Lampiran Surat Kuasa',
			'noSI' => 'No SI',
			'tglTiba' => 'Tgl Tiba',
			'tglBrgkt' => 'Tgl Berangkat',
			'si_atc' => 'Lampiran SI',
			'noCI' => 'No CI',
			'ci_tgl' => 'Tgl CI',
			'ci_atc' => 'Lampiran CI',
			'nomor' => 'No BL',
			'bl_tgl' => 'Tgl BL',
			'atc' => 'Lampiran BL',
			'nama' => 'Nama Barang',
			'des' => 'Deskripsi brg',
			'jml' => 'Jml barang',
			'bruto' => 'Bruto brg',
			'netto' => 'Netto brg',
			'volume' => 'Volume brg',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('barang_id',$this->barang_id);
		$criteria->compare('layanan_id',$this->layanan_id);
		$criteria->compare('bl_id',$this->bl_id);
		$criteria->compare('ci_id',$this->ci_id);
		$criteria->compare('si_id',$this->si_id);
		$criteria->compare('pl',$this->pl,true);
		$criteria->compare('plAtc',$this->plAtc,true);
		$criteria->compare('sj',$this->sj,true);
		$criteria->compare('sjAtc',$this->sjAtc,true);
		$criteria->compare('sk',$this->sk,true);
		$criteria->compare('skAtc',$this->skAtc,true);
		$criteria->compare('noSI',$this->noSI,true);
		$criteria->compare('tglTiba',$this->tglTiba,true);
		$criteria->compare('tglBrgkt',$this->tglBrgkt,true);
		$criteria->compare('si_atc',$this->si_atc,true);
		$criteria->compare('noCI',$this->noCI,true);
		$criteria->compare('ci_tgl',$this->ci_tgl,true);
		$criteria->compare('ci_atc',$this->ci_atc,true);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('bl_tgl',$this->bl_tgl,true);
		$criteria->compare('atc',$this->atc,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('des',$this->des,true);
		$criteria->compare('jml',$this->jml);
		$criteria->compare('bruto',$this->bruto);
		$criteria->compare('netto',$this->netto);
		$criteria->compare('volume',$this->volume);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}