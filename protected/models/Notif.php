<?php

/**
 * This is the model class for table "notif".
 *
 * The followings are the available columns in table 'notif':
 * @property integer $id
 * @property integer $dari
 * @property string $roleDari
 * @property integer $ke
 * @property string $roleKe
 * @property string $row_id
 * @property integer $tableDesc_id
 * @property string $field
 * @property integer $isSentToRole
 * @property integer $isRead
 * @property string $JOStatus
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Tabledesc $tableDesc
 * @property Joborderstatus $jOStatus
 * @property User $dari0
 * @property User $ke0
 * @property Userrole $roleDari0
 * @property Userrole $roleKe0
 */
class Notif extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Notif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dari, roleDari, isSentToRole', 'required'),
			array('dari, ke, tableDesc_id, isSentToRole, isRead', 'numerical', 'integerOnly'=>true),
			array('roleDari, roleKe, JOStatus', 'length', 'max'=>5),
			array('row_id, field', 'length', 'max'=>45),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, dari, roleDari, ke, roleKe, row_id, tableDesc_id, field, isSentToRole, isRead, JOStatus, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tableDesc' => array(self::BELONGS_TO, 'Tabledesc', 'tableDesc_id'),
			'jOStatus' => array(self::BELONGS_TO, 'Joborderstatus', 'JOStatus'),
			'dariUser' => array(self::BELONGS_TO, 'User', 'dari'),
			'keUser' => array(self::BELONGS_TO, 'User', 'ke'),
			'roleDariRole' => array(self::BELONGS_TO, 'Userrole', 'roleDari'),
			'roleKeRole' => array(self::BELONGS_TO, 'Userrole', 'roleKe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dari' => 'Dari',
			'roleDari' => 'Role Dari',
			'ke' => 'Ke',
			'roleKe' => 'Role Ke',
			'row_id' => 'Row',
			'tableDesc_id' => 'Table Desc',
			'field' => 'Field',
			'isSentToRole' => 'Is Sent To Role',
			'isRead' => 'Is Read',
			'JOStatus' => 'Jostatus',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dari',$this->dari);
		$criteria->compare('roleDari',$this->roleDari,true);
		$criteria->compare('ke',$this->ke);
		$criteria->compare('roleKe',$this->roleKe,true);
		$criteria->compare('row_id',$this->row_id,true);
		$criteria->compare('tableDesc_id',$this->tableDesc_id);
		$criteria->compare('field',$this->field,true);
		$criteria->compare('isSentToRole',$this->isSentToRole);
		$criteria->compare('isRead',$this->isRead);
		$criteria->compare('JOStatus',$this->JOStatus,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}