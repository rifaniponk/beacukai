<?php

/**
 * This is the model class for table "pelanggan".
 *
 * The followings are the available columns in table 'pelanggan':
 * @property integer $user_id
 * @property string $ktp
 * @property string $nama
 * @property integer $role
 * @property string $email
 * @property string $alamat
 * @property string $npwp
 * @property string $noapi
 * @property string $npik
 * @property string $telp
 * @property string $web
 * @property string $siupAtc
 * @property string $npwpAtc
 * @property string $ktpAtc
 * @property integer $isVerified
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 * @property User $user
 */
class Pelanggan extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pelanggan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelanggan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, ktp, nama, email, alamat', 'required'),
			array('user_id, role, isVerified', 'numerical', 'integerOnly'=>true),
			array('ktp, email, npwp, noapi, npik, telp', 'length', 'max'=>45),
			array('nama', 'length', 'max'=>145),
			array('web', 'length', 'max'=>25),
			array('siupAtc, npwpAtc, ktpAtc', 'length', 'max'=>245),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, ktp, nama, role, email, alamat, npwp, noapi, npik, telp, web, siupAtc, npwpAtc, ktpAtc, isVerified, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'pelanggan_user_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'ktp' => 'Ktp',
			'nama' => 'Nama',
			'role' => 'Role',
			'email' => 'Email',
			'alamat' => 'Alamat',
			'npwp' => 'Npwp',
			'noapi' => 'Noapi',
			'npik' => 'Npik',
			'telp' => 'Telp',
			'web' => 'Web',
			'siupAtc' => 'Siup Atc',
			'npwpAtc' => 'Npwp Atc',
			'ktpAtc' => 'Ktp Atc',
			'isVerified' => 'Is Verified',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('ktp',$this->ktp,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('noapi',$this->noapi,true);
		$criteria->compare('npik',$this->npik,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('web',$this->web,true);
		$criteria->compare('siupAtc',$this->siupAtc,true);
		$criteria->compare('npwpAtc',$this->npwpAtc,true);
		$criteria->compare('ktpAtc',$this->ktpAtc,true);
		$criteria->compare('isVerified',$this->isVerified);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}