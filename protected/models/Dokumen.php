<?php

/**
 * This is the model class for table "dokumen".
 *
 * The followings are the available columns in table 'dokumen':
 * @property integer $id
 * @property string $jenis
 * @property string $judul
 * @property string $atc
 * @property integer $JobOrder_id
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder $jobOrder
 */
class Dokumen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Dokumen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dokumen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, atc, JobOrder_id', 'required'),
			array('JobOrder_id', 'numerical', 'integerOnly'=>true),
			array('jenis', 'length', 'max'=>15),
			array('judul', 'length', 'max'=>145),
			array('atc', 'length', 'max'=>245),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jenis, judul, atc, JobOrder_id, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jobOrder' => array(self::BELONGS_TO, 'Joborder', 'JobOrder_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jenis' => 'Jenis Dokumen',
			'judul' => 'Judul Dokumen',
			'atc' => 'Lampiran',
			'JobOrder_id' => 'Job Order ID',
			'ctime' => 'Waktu Upload',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jenis',$this->jenis,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('atc',$this->atc,true);
		$criteria->compare('JobOrder_id',$this->JobOrder_id);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}