<?php

/**
 * This is the model class for table "pemasukanv".
 *
 * The followings are the available columns in table 'pemasukanv':
 * @property integer $bulan
 * @property integer $tahun
 * @property double $jasaImpor
 * @property double $jasaPPJK
 * @property double $biayaDok
 * @property double $biayaLain
 * @property double $total1
 * @property double $ppnTotal1
 * @property double $pdri
 * @property double $beaMasuk
 * @property double $biayaBeacukai
 * @property double $total2
 * @property double $grandtotal
 */
class Pemasukanv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pemasukanv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pemasukanv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bulan, tahun', 'numerical', 'integerOnly'=>true),
			array('jasaImpor, jasaPPJK, biayaDok, biayaLain, total1, ppnTotal1, pdri, beaMasuk, biayaBeacukai, total2, grandtotal', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bulan, tahun, jasaImpor, jasaPPJK, biayaDok, biayaLain, total1, ppnTotal1, pdri, beaMasuk, biayaBeacukai, total2, grandtotal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bulan' => 'Bulan',
			'tahun' => 'Tahun',
			'jasaImpor' => 'Jasa Impor',
			'jasaPPJK' => 'Jasa Ppjk',
			'biayaDok' => 'Biaya Dok',
			'biayaLain' => 'Biaya Lain',
			'total1' => 'Total1',
			'ppnTotal1' => 'Ppn Total1',
			'pdri' => 'Pdri',
			'beaMasuk' => 'Bea Masuk',
			'biayaBeacukai' => 'Biaya Beacukai',
			'total2' => 'Total2',
			'grandtotal' => 'Grandtotal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bulan',$this->bulan);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('jasaImpor',$this->jasaImpor);
		$criteria->compare('jasaPPJK',$this->jasaPPJK);
		$criteria->compare('biayaDok',$this->biayaDok);
		$criteria->compare('biayaLain',$this->biayaLain);
		$criteria->compare('total1',$this->total1);
		$criteria->compare('ppnTotal1',$this->ppnTotal1);
		$criteria->compare('pdri',$this->pdri);
		$criteria->compare('beaMasuk',$this->beaMasuk);
		$criteria->compare('biayaBeacukai',$this->biayaBeacukai);
		$criteria->compare('total2',$this->total2);
		$criteria->compare('grandtotal',$this->grandtotal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}