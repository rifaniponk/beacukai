<?php

/**
 * This is the model class for table "si".
 *
 * The followings are the available columns in table 'si':
 * @property integer $id
 * @property string $noSI
 * @property string $tglTiba
 * @property string $tglBrgkt
 * @property string $atc
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 */
class Si extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Si the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'si';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('noSI, tglTiba, tglBrgkt, atc', 'required'),
			array('noSI', 'length', 'max'=>30),
			array('atc', 'length', 'max'=>245),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, noSI, tglTiba, tglBrgkt, atc, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'si_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'noSI' => 'No Si',
			'tglTiba' => 'Tgl Tiba',
			'tglBrgkt' => 'Tgl Brgkt',
			'atc' => 'Atc',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('noSI',$this->noSI,true);
		$criteria->compare('tglTiba',$this->tglTiba,true);
		$criteria->compare('tglBrgkt',$this->tglBrgkt,true);
		$criteria->compare('atc',$this->atc,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}