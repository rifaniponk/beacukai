<?php

/**
 * This is the model class for table "pelangganv".
 *
 * The followings are the available columns in table 'pelangganv':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $namauser
 * @property string $roleUser
 * @property string $last_login
 * @property integer $user_id
 * @property string $ktp
 * @property string $nama
 * @property integer $rolePelanggan
 * @property string $email
 * @property string $alamat
 * @property string $npwp
 * @property string $noapi
 * @property string $npik
 * @property string $telp
 * @property string $web
 * @property string $siupAtc
 * @property string $npwpAtc
 * @property string $ktpAtc
 * @property integer $isVerified
 */
class Pelangganv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pelangganv the static model class
	 */
	public function primaryKey(){
		return 'id';
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelangganv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, namauser, roleUser, rolePelanggan, ktp, email, alamat, telp', 'required'),
			array('id, user_id, rolePelanggan, isVerified', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>10),
			array('password, namauser, siupAtc, npwpAtc, ktpAtc', 'length', 'max'=>245),
			array('roleUser', 'length', 'max'=>5),
			array('ktp, email, npwp, noapi, npik, telp', 'length', 'max'=>45),
			array('nama', 'length', 'max'=>145),
			array('web', 'length', 'max'=>25),
			array('last_login, alamat', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, namauser, roleUser, last_login, user_id, ktp, nama, rolePelanggan, email, alamat, npwp, noapi, npik, telp, web, isVerified', 'safe', 'on'=>'search'),
			array('username,email','unique'),
			array('siupAtc, npwpAtc, ktpAtc','file','maxSize'=>'1500000'),
			array('email','email'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'namauser' => 'Nama Pelanggan',
			'roleUser' => 'Role',
			'last_login' => 'Last Login',
			'user_id' => 'User ID',
			'ktp' => 'No KTP',
			'nama' => 'Nama Pelanggan',
			'rolePelanggan' => 'Jenis Pelanggan',
			'email' => 'Email',
			'alamat' => 'Alamat',
			'npwp' => 'No NPWP',
			'noapi' => 'No API',
			'npik' => 'NPIK',
			'telp' => 'Telp',
			'web' => 'Web',
			'siupAtc' => 'Lampiran SIUP',
			'npwpAtc' => 'Lampiran NPWP',
			'ktpAtc' => 'Lampiran KTP',
			'isVerified' => 'Is Verified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('namauser',$this->namauser,true);
		$criteria->compare('roleUser',$this->roleUser,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('ktp',$this->ktp,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('rolePelanggan',$this->rolePelanggan);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('noapi',$this->noapi,true);
		$criteria->compare('npik',$this->npik,true);
		$criteria->compare('telp',$this->telp,true);
		$criteria->compare('web',$this->web,true);
		$criteria->compare('siupAtc',$this->siupAtc,true);
		$criteria->compare('npwpAtc',$this->npwpAtc,true);
		$criteria->compare('ktpAtc',$this->ktpAtc,true);
		$criteria->compare('isVerified',$this->isVerified);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}