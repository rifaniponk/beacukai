<?php

/**
 * This is the model class for table "laporanjo".
 *
 * The followings are the available columns in table 'laporanjo':
 * @property integer $Pesanan_ID
 * @property string $Nama_Pelanggan
 * @property string $Nama_Barang
 * @property string $Tanggal_Mulai
 * @property string $Tanggal_Selesai
 * @property string $Status
 * @property string $No_Pengajuan_PIB
 * @property double $Total_Biaya
 */
class LaporanJO extends CActiveRecord
{
	public $_date1;
	public function getDate1(){
		return $this->_date1;
	}
	public function setDate1($date1){
		$this->_date1 = $date1;
	}
	public $_date2;
	public function getDate2(){
		return $this->_date2;
	}
	public function setDate2($date2){
		$this->_date2 = $date2;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanJO the static model class
	 */
	public function primaryKey(){
		return 'Pesanan_ID';
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporanjo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nama_Pelanggan, Nama_Barang', 'required'),
			array('Pesanan_ID', 'numerical', 'integerOnly'=>true),
			array('Total_Biaya', 'numerical'),
			array('Nama_Pelanggan, Nama_Barang', 'length', 'max'=>145),
			array('Status', 'length', 'max'=>20),
			array('No_Pengajuan_PIB', 'length', 'max'=>45),
			array('Tanggal_Mulai, Tanggal_Selesai', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Pesanan_ID, Nama_Pelanggan, Nama_Barang, Tanggal_Mulai, Tanggal_Selesai, Status, No_Pengajuan_PIB, Total_Biaya', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Pesanan_ID' => 'Pesanan ID',
			'Nama_Pelanggan' => 'Nama Pelanggan',
			'Nama_Barang' => 'Nama Barang',
			'Tanggal_Mulai' => 'Tanggal Mulai',
			'Tanggal_Selesai' => 'Tanggal Selesai',
			'Status' => 'Status',
			'No_Pengajuan_PIB' => 'No Pengajuan PIB',
			'Total_Biaya' => 'Total Biaya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Pesanan_ID',$this->Pesanan_ID);
		$criteria->compare('Nama_Pelanggan',$this->Nama_Pelanggan,true);
		$criteria->compare('Nama_Barang',$this->Nama_Barang,true);
		
		// $criteria->compare('Tanggal_Mulai',$this->Tanggal_Mulai,true);
		$criteria->addCondition("Tanggal_Mulai BETWEEN '".$this->_date1."' AND '".$this->_date2."'");

		// $criteria->compare('Tanggal_Selesai',$this->Tanggal_Selesai,true);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('No_Pengajuan_PIB',$this->No_Pengajuan_PIB,true);
		$criteria->compare('Total_Biaya',$this->Total_Biaya);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}