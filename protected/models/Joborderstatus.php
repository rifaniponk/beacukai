<?php

/**
 * This is the model class for table "joborderstatus".
 *
 * The followings are the available columns in table 'joborderstatus':
 * @property string $kode
 * @property string $status
 * @property integer $urutan
 * @property string $ket
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 */
class Joborderstatus extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Joborderstatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'joborderstatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, status', 'required'),
			array('urutan', 'numerical', 'integerOnly'=>true),
			array('kode', 'length', 'max'=>5),
			array('status', 'length', 'max'=>245),
			array('ket', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('kode, status, urutan, ket', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kode' => 'Kode',
			'status' => 'Status',
			'urutan' => 'Urutan',
			'ket' => 'Ket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('urutan',$this->urutan);
		$criteria->compare('ket',$this->ket,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}