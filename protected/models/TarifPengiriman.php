<?php

/**
 * This is the model class for table "tarif_pengiriman".
 *
 * The followings are the available columns in table 'tarif_pengiriman':
 * @property integer $id
 * @property integer $pelabuhan_muat_id
 * @property integer $pelabuhanBongkar_id
 * @property integer $biaya
 * @property string $ukContainer
 * @property string $tipeContainer
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property PelabuhanBongkar $pelabuhanBongkar
 * @property PelabuhanMuat $pelabuhanMuat
 */
class TarifPengiriman extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TarifPengiriman the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tarif_pengiriman';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pelabuhan_muat_id, pelabuhanBongkar_id, biaya', 'required'),
			array('pelabuhan_muat_id, pelabuhanBongkar_id, biaya', 'numerical', 'integerOnly'=>true),
			array('ukContainer, tipeContainer', 'length', 'max'=>45),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pelabuhan_muat_id, pelabuhanBongkar_id, biaya, ukContainer, tipeContainer, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pelabuhanBongkar' => array(self::BELONGS_TO, 'PelabuhanBongkar', 'pelabuhanBongkar_id'),
			'pelabuhanMuat' => array(self::BELONGS_TO, 'PelabuhanMuat', 'pelabuhan_muat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pelabuhan_muat_id' => 'Pelabuhan Muat',
			'pelabuhanBongkar_id' => 'Pelabuhan Bongkar',
			'biaya' => 'Biaya',
			'ukContainer' => 'Uk Container',
			'tipeContainer' => 'Tipe Container',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pelabuhan_muat_id',$this->pelabuhan_muat_id);
		$criteria->compare('pelabuhanBongkar_id',$this->pelabuhanBongkar_id);
		$criteria->compare('biaya',$this->biaya);
		$criteria->compare('ukContainer',$this->ukContainer,true);
		$criteria->compare('tipeContainer',$this->tipeContainer,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}