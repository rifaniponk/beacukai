<?php

/**
 * This is the model class for table "pib".
 *
 * The followings are the available columns in table 'pib':
 * @property integer $id
 * @property string $noPengajuan
 * @property string $tglPengajuan
 * @property string $atc
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 */
class Pib extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pib the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pib';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('noPengajuan, tglPengajuan, atc', 'required'),
			array('noPengajuan', 'length', 'max'=>45),
			array('atc', 'length', 'max'=>245),
			array('ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, noPengajuan, tglPengajuan, atc, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'pib_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'noPengajuan' => 'No Pengajuan',
			'tglPengajuan' => 'Tgl Pengajuan',
			'atc' => 'Atc',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('noPengajuan',$this->noPengajuan,true);
		$criteria->compare('tglPengajuan',$this->tglPengajuan,true);
		$criteria->compare('atc',$this->atc,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}