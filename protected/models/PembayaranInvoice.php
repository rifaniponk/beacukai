<?php

/**
 * This is the model class for table "pembayaraninvoice".
 *
 * The followings are the available columns in table 'pembayaraninvoice':
 * @property integer $id
 * @property integer $jml
 * @property string $tglBayar
 * @property string $ket
 * @property string $atc
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Invoice[] $invoices
 */
class PembayaranInvoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PembayaranInvoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pembayaraninvoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jml, tglBayar, atc', 'required'),
			array('jml', 'numerical', 'integerOnly'=>true),
			array('atc', 'length', 'max'=>245),
			array('ket, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jml, tglBayar, ket, atc, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invoices' => array(self::HAS_MANY, 'Invoice', 'pembayaranInvoice_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jml' => 'Jml Bayar',
			'tglBayar' => 'Tgl Bayar',
			'ket' => 'Ket',
			'atc' => 'Lampiran',
			'ctime' => 'Ctime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jml',$this->jml);
		$criteria->compare('tglBayar',$this->tglBayar,true);
		$criteria->compare('ket',$this->ket,true);
		$criteria->compare('atc',$this->atc,true);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}