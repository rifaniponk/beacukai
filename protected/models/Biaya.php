<?php

/**
 * This is the model class for table "biaya".
 *
 * The followings are the available columns in table 'biaya':
 * @property integer $id
 * @property double $cif
 * @property double $fob
 * @property double $asuransi
 * @property double $bm
 * @property double $cukai
 * @property double $ppn
 * @property double $ppnbm
 * @property double $pph
 * @property double $kurs
 * @property double $freight
 * @property double $nilaiPabean
 * @property double $nilaiImpor
 * @property string $tglTiba
 * @property string $docAtc
 * @property double $bm_rp
 * @property double $cukai_rp
 * @property double $pdri
 * @property string $ctime
 *
 * The followings are the available model relations:
 * @property Joborder[] $joborders
 */
class Biaya extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Biaya the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'biaya';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cif, fob, asuransi, bm, cukai, ppn, ppnbm, pph, kurs, freight, nilaiPabean, nilaiImpor, bm_rp, cukai_rp, pdri', 'numerical'),
			array('docAtc', 'length', 'max'=>245),
			array('tglTiba, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cif, fob, asuransi, bm, cukai, ppn, ppnbm, pph, kurs, freight, nilaiPabean, nilaiImpor, tglTiba, docAtc, bm_rp, cukai_rp, pdri, ctime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'joborders' => array(self::HAS_MANY, 'Joborder', 'biaya_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cif' => 'CIF',
			'fob' => 'FOB',
			'asuransi' => 'Asuransi',
			'bm' => 'Bea Masuk',
			'cukai' => 'Cukai',
			'ppn' => 'PPN',
			'ppnbm' => 'PPNBM',
			'pph' => 'Pph',
			'kurs' => 'Kurs',
			'freight' => 'Freight',
			'nilaiPabean' => 'Nilai Pabean',
			'nilaiImpor' => 'Nilai Impor',
			'tglTiba' => 'Tgl Tiba',
			'docAtc' => 'Lampiran',
			'bm_rp' => 'Bea Masuk',
			'cukai_rp' => 'Cukai',
			'pdri' => 'Pajak Dalam Rangka Impor',
			'ctime' => 'Created time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cif',$this->cif);
		$criteria->compare('fob',$this->fob);
		$criteria->compare('asuransi',$this->asuransi);
		$criteria->compare('bm',$this->bm);
		$criteria->compare('cukai',$this->cukai);
		$criteria->compare('ppn',$this->ppn);
		$criteria->compare('ppnbm',$this->ppnbm);
		$criteria->compare('pph',$this->pph);
		$criteria->compare('kurs',$this->kurs);
		$criteria->compare('freight',$this->freight);
		$criteria->compare('nilaiPabean',$this->nilaiPabean);
		$criteria->compare('nilaiImpor',$this->nilaiImpor);
		$criteria->compare('tglTiba',$this->tglTiba,true);
		$criteria->compare('docAtc',$this->docAtc,true);
		$criteria->compare('bm_rp',$this->bm_rp);
		$criteria->compare('cukai_rp',$this->cukai_rp);
		$criteria->compare('pdri',$this->pdri);
		$criteria->compare('ctime',$this->ctime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}